# coding: utf-8
from django.utils.translation import ugettext as _
from django.contrib import admin
from django.conf import settings
from .models import Macros
from .forms import EditMacrosForm


class MacrosAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'identifier',
        'signal_name', 'signal_sender',
        'append_output')
    search_fields = ['name', 'identifier']
    form = EditMacrosForm
    fieldsets = [
        (None, {
            'fields': ['name', 'identifier']
        }),
        (_(u'Code'), {
            'fields': ['entry_point', 'code']
        }),
        (_(u'Signals'), {
            'fields': ['signal_name', 'signal_sender']
        }),
        (_(u'Output'), {
            'fields': ['append_output', 'output']
        }),
    ]
    change_form_template = 'admin/macroses/macros_change_form.html'

    actions = (
        'run_macros',
        'set_macros_as_job',
        'clear_output',
    )

    def run_macros(self, request, queryset):
        for q in queryset:
            q.run()
    run_macros.short_description = _(u'Run immideatly')

    def set_macros_as_job(self, request, queryset):
        from tasks.api import TaskQueueManager
        task_manager = TaskQueueManager()

        idents = [q.identifier for q in queryset]
        task_manager.schedule(
            'macroses.jobs.run_macros_job',
            args=idents,
            priority=getattr(settings, 'JOB_MACROS_PRIORITY', 1),
        )
    set_macros_as_job.short_description = _(u'Set as backround job')

    def clear_output(self, request, queryset):
        for q in queryset:
            q.output = ''
            q.save()
    clear_output.short_description = _(u'Clear output')

    def suit_row_attributes(self, obj):
        if 'Error' in obj.output:
            return {'class': 'error'}

admin.site.register(Macros, MacrosAdmin)
