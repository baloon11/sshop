# coding: utf-8

MACROS_TEMPLATE = '''
from macroses.default import BaseMacros

class MainMacros(BaseMacros):
    def run(self):
        print 'Hello!'

'''

ENTRY_POINT_CLASS = 'MainMacros'
