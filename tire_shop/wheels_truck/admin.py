from django.contrib import admin
from tire_shop.wheels_truck.models import TransportType, Vendor, Car, Modification

admin.site.register(TransportType)
admin.site.register(Vendor)
admin.site.register(Car)
admin.site.register(Modification)