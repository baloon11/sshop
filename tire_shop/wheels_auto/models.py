from django.db import models

class BoltPattern(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Car(models.Model):
    vendor = models.ForeignKey('Vendor')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Modification(models.Model):
    car = models.ForeignKey('Car')
    year = models.IntegerField()
    name = models.CharField(max_length=100)
    wheel_size1 = models.IntegerField()
    wheel_size2 = models.IntegerField()
    wheel_rim1 = models.FloatField()
    wheel_rim2 = models.FloatField()
    et1 = models.IntegerField()
    et2 = models.IntegerField()
    bolt_pattern = models.ForeignKey('BoltPattern')
    center_bore = models.FloatField()
    nut = models.ForeignKey('Nut')
    tire_size1 = models.IntegerField()
    tire_size2 = models.IntegerField()
    tire_width1 = models.IntegerField()
    tire_width2 = models.IntegerField()
    tire_ratio1 = models.IntegerField()
    tire_ratio2 = models.IntegerField()

    def __unicode__(self):
        return self.name

class Nut(models.Model):
    nut_type = models.ForeignKey('NutType')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class NutType(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class ParamTire(models.Model):
    modification = models.ForeignKey('Modification')
    param_type = models.ForeignKey('ParamType')
    size = models.IntegerField()
    width = models.IntegerField()
    ratio = models.IntegerField()

    def __unicode__(self):
        return '%s %s %s %s' % (self.modification, self.size, self.width, self.ratio)

class ParamType(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class ParamWheel(models.Model):
    modification = models.ForeignKey('Modification')
    param_type = models.ForeignKey('ParamType')
    size = models.IntegerField()
    rim = models.FloatField()
    et = models.IntegerField()

    def __unicode__(self):
        return '%s %s %s %s' % (self.modification, self.size, self.rim, self.et)


class Vendor(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name