# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BoltPattern'
        db.create_table('wheels_auto_boltpattern', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_auto', ['BoltPattern'])

        # Adding model 'Car'
        db.create_table('wheels_auto_car', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vendor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.Vendor'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_auto', ['Car'])

        # Adding model 'Modification'
        db.create_table('wheels_auto_modification', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.Car'])),
            ('year', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('wheel_size1', self.gf('django.db.models.fields.IntegerField')()),
            ('wheel_size2', self.gf('django.db.models.fields.IntegerField')()),
            ('wheel_rim1', self.gf('django.db.models.fields.FloatField')()),
            ('wheel_rim2', self.gf('django.db.models.fields.FloatField')()),
            ('et1', self.gf('django.db.models.fields.IntegerField')()),
            ('et2', self.gf('django.db.models.fields.IntegerField')()),
            ('bolt_pattern', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.BoltPattern'])),
            ('center_bore', self.gf('django.db.models.fields.FloatField')()),
            ('nut', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.Nut'])),
            ('tire_size1', self.gf('django.db.models.fields.IntegerField')()),
            ('tire_size2', self.gf('django.db.models.fields.IntegerField')()),
            ('tire_width1', self.gf('django.db.models.fields.IntegerField')()),
            ('tire_width2', self.gf('django.db.models.fields.IntegerField')()),
            ('tire_ratio1', self.gf('django.db.models.fields.IntegerField')()),
            ('tire_ratio2', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('wheels_auto', ['Modification'])

        # Adding model 'Nut'
        db.create_table('wheels_auto_nut', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nut_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.NutType'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_auto', ['Nut'])

        # Adding model 'NutType'
        db.create_table('wheels_auto_nuttype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_auto', ['NutType'])

        # Adding model 'ParamTire'
        db.create_table('wheels_auto_paramtire', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('modification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.Modification'])),
            ('param_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.ParamType'])),
            ('size', self.gf('django.db.models.fields.IntegerField')()),
            ('width', self.gf('django.db.models.fields.IntegerField')()),
            ('ratio', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('wheels_auto', ['ParamTire'])

        # Adding model 'ParamType'
        db.create_table('wheels_auto_paramtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_auto', ['ParamType'])

        # Adding model 'ParamWheel'
        db.create_table('wheels_auto_paramwheel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('modification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.Modification'])),
            ('param_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_auto.ParamType'])),
            ('size', self.gf('django.db.models.fields.IntegerField')()),
            ('rim', self.gf('django.db.models.fields.FloatField')()),
            ('et', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('wheels_auto', ['ParamWheel'])

        # Adding model 'Vendor'
        db.create_table('wheels_auto_vendor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_auto', ['Vendor'])


    def backwards(self, orm):
        # Deleting model 'BoltPattern'
        db.delete_table('wheels_auto_boltpattern')

        # Deleting model 'Car'
        db.delete_table('wheels_auto_car')

        # Deleting model 'Modification'
        db.delete_table('wheels_auto_modification')

        # Deleting model 'Nut'
        db.delete_table('wheels_auto_nut')

        # Deleting model 'NutType'
        db.delete_table('wheels_auto_nuttype')

        # Deleting model 'ParamTire'
        db.delete_table('wheels_auto_paramtire')

        # Deleting model 'ParamType'
        db.delete_table('wheels_auto_paramtype')

        # Deleting model 'ParamWheel'
        db.delete_table('wheels_auto_paramwheel')

        # Deleting model 'Vendor'
        db.delete_table('wheels_auto_vendor')


    models = {
        'wheels_auto.boltpattern': {
            'Meta': {'object_name': 'BoltPattern'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'wheels_auto.car': {
            'Meta': {'object_name': 'Car'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.Vendor']"})
        },
        'wheels_auto.modification': {
            'Meta': {'object_name': 'Modification'},
            'bolt_pattern': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.BoltPattern']"}),
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.Car']"}),
            'center_bore': ('django.db.models.fields.FloatField', [], {}),
            'et1': ('django.db.models.fields.IntegerField', [], {}),
            'et2': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nut': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.Nut']"}),
            'tire_ratio1': ('django.db.models.fields.IntegerField', [], {}),
            'tire_ratio2': ('django.db.models.fields.IntegerField', [], {}),
            'tire_size1': ('django.db.models.fields.IntegerField', [], {}),
            'tire_size2': ('django.db.models.fields.IntegerField', [], {}),
            'tire_width1': ('django.db.models.fields.IntegerField', [], {}),
            'tire_width2': ('django.db.models.fields.IntegerField', [], {}),
            'wheel_rim1': ('django.db.models.fields.FloatField', [], {}),
            'wheel_rim2': ('django.db.models.fields.FloatField', [], {}),
            'wheel_size1': ('django.db.models.fields.IntegerField', [], {}),
            'wheel_size2': ('django.db.models.fields.IntegerField', [], {}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'wheels_auto.nut': {
            'Meta': {'object_name': 'Nut'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nut_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.NutType']"})
        },
        'wheels_auto.nuttype': {
            'Meta': {'object_name': 'NutType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'wheels_auto.paramtire': {
            'Meta': {'object_name': 'ParamTire'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.Modification']"}),
            'param_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.ParamType']"}),
            'ratio': ('django.db.models.fields.IntegerField', [], {}),
            'size': ('django.db.models.fields.IntegerField', [], {}),
            'width': ('django.db.models.fields.IntegerField', [], {})
        },
        'wheels_auto.paramtype': {
            'Meta': {'object_name': 'ParamType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'wheels_auto.paramwheel': {
            'Meta': {'object_name': 'ParamWheel'},
            'et': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.Modification']"}),
            'param_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_auto.ParamType']"}),
            'rim': ('django.db.models.fields.FloatField', [], {}),
            'size': ('django.db.models.fields.IntegerField', [], {})
        },
        'wheels_auto.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['wheels_auto']