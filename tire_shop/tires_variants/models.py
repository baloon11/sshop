from django.db import models

class AutoMake(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class BoltPattern(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class AutoModel(models.Model):
    auto_make = models.ForeignKey('AutoMake')
    name = models.CharField(max_length=255)
    year = models.CharField(max_length=100)
    x1 = models.IntegerField()
    x2 = models.IntegerField()
    yy = models.IntegerField()
    inc = models.IntegerField()
    wst = models.IntegerField()
    wm = models.IntegerField()

    def __unicode__(self):
        return self.name

class AutoModClar(models.Model):
    auto_model = models.ForeignKey('AutoModel')
    name = models.CharField(max_length=255)
    wheel_size1 = models.IntegerField()
    wheel_size2 = models.IntegerField()
    offsets1 = models.IntegerField()
    offsets2 = models.IntegerField()
    bolt_pattern = models.ForeignKey('BoltPattern')
    tyres = models.TextField()

    def __unicode__(self):
        return self.name

