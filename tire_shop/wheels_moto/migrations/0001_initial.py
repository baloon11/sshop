# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Vendor'
        db.create_table('wheels_moto_vendor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_moto', ['Vendor'])

        # Adding model 'Modification'
        db.create_table('wheels_moto_modification', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vendor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_moto.Vendor'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_moto', ['Modification'])

        # Adding model 'Car'
        db.create_table('wheels_moto_car', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('modification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_moto.Modification'])),
            ('years', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_moto', ['Car'])

        # Adding model 'Param'
        db.create_table('wheels_moto_param', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_moto.Car'])),
            ('position', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('param_type', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('size', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('p1', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('p2', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('p3', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('wheels_moto', ['Param'])


    def backwards(self, orm):
        # Deleting model 'Vendor'
        db.delete_table('wheels_moto_vendor')

        # Deleting model 'Modification'
        db.delete_table('wheels_moto_modification')

        # Deleting model 'Car'
        db.delete_table('wheels_moto_car')

        # Deleting model 'Param'
        db.delete_table('wheels_moto_param')


    models = {
        'wheels_moto.car': {
            'Meta': {'object_name': 'Car'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_moto.Modification']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'years': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'wheels_moto.modification': {
            'Meta': {'object_name': 'Modification'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_moto.Vendor']"})
        },
        'wheels_moto.param': {
            'Meta': {'object_name': 'Param'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_moto.Car']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'p1': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'p2': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'p3': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'param_type': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'wheels_moto.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['wheels_moto']