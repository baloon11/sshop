$('#start-button').click(function() {
    LogTailer.startReading();
});

$('#stop-button').click(function() {
    LogTailer.stopReading();
});

$('#auto-scroll').click(function() {
    LogTailer.changeAutoScroll();
});	

$('#filter-select').change(function() {
	LogTailer.customFilter();
});		

$('#log-window').html("")
LogTailer.customFilter();

$("#save-logs").colorbox({width:"500px",
									 height:"300px",
									 inline:true,
									 href:"#clipboard-form-container",
									 onOpen: function(){
									 	$("#clipboard-logs").val(getSelectedText());
									 	$("#clipboard-name").val("Name");
									 	$("#clipboard-notes").val("Notes");
									 	$("#clipboard-error").html("");
									 }});

$('#clipboard-form').submit(function() {
  var error = false;
  if($("#clipboard-name").val().length<1){
  	$("#clipboard-error").html("Field name is mandatory");
  	error = true;
  }
  if($("#clipboard-logs").val().length<1){
  	$("#clipboard-error").html("No log lines selected");
  	error = true;
  }
  
  if(!error){
  	$.ajax({type: 'POST',
					   url: clipboard_url,
  	                   data: { 
  	                   	 name: $("#clipboard-name").val(),
  	                     notes: $("#clipboard-notes").val(),
  	                     logs: $("#clipboard-logs").val(),
  	                     file: $("#clipboard-file").val()
  	                   },
  	                   success: function(result){
  	                   	   alert(result);
  	                   	   $("#save-logs").colorbox.close();	
  	                   }, 
  	                   dataType: "text/html"});
  }
  
  return false;
});
