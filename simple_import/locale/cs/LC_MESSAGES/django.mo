��    ,      |  ;   �      �  :   �           %     ?     \     j     w     �     �     �     �     �     �     �     �     �            
   "     -     =  :   B     }     �     �     �  2   �     �  )   �          %     *     /     =     Q     c     o          �     �  
   �     �     �  �  �  Q   O  &   �     �  $   �     	     	     4	     J	  
   Y	     d	  #   �	  
   �	     �	     �	     �	     �	     
     
     
     %
     5
  M   ;
     �
     �
     �
     �
  ?   �
       8   '     `     f     m     u     �     �  
   �     �     �     �     �          "  	   1              &                %   	            #       $             *           )             "              '          !       
       +                                                  (   ,                Annoying way to order the columns to match the header rows Cannot undo this type of import! Clear field on blank cell Click here to run the import Column Header Column match Column matches Column name Content type Create New Records Create and Update Records Created Date Created Default Value Default value Download failed records Failed Field Field name Header position Home If cell is blank, clear out the field setting it to blank. Import Results Import data Import file Import settings Invalid file type. Must be xls, xlsx, ods, or csv. Match Columns Match Relations and Prepare to Run Import Model Name Next Null on empty Only Update Records Run actual import Sample Data Simulate import Submit This was only a simulation. Unique Mapping Update Key Updated User Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-23 17:11+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 Složitý zúůsob uspořádání sloupců tak, aby odpovidaly řádkům zahlaví Nelze vrátit zpět tento typ importu! Očistit pole v prazdné buňce Klikněte zde pro zrušení importu. Záhlaví sloupce přiřazení sloupce přiřazení sloupců Název sloupce Typ obsahu Vytvoření nového záznamu Vytvoření a aktualizace záznamů Vytvořeno Datum vytvoření Výchozí hodnota Výchozí hodnota Stahnout neúspěšné záznamy Neúspěšný Pole Název pole pozice zahlaví Úvod Je-li buňka prazdná, očistětě pole nastavením je na prazdnou hodnotu. . Výsledky importu Importovát data Importovat soubor Importovat nastavení Neplatný typ souboru. Musí být buď xsl, xslx, ods nebo csv. Přiřadit sloupce Přiřadit vztahy a připravte se ke spuštění importu Model Jméno Další Null na prazdné Pouze aktualizovat záznamy Spustit aktuální import Vzorek dat Simulovat import Potvrdit To byla jen simulace. Jedinečné mapování Aktualizace klíče Aktualizováno Uživatel 