# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SMS'
        db.create_table('sms_sms', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('exported', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('sms', ['SMS'])


    def backwards(self, orm):
        # Deleting model 'SMS'
        db.delete_table('sms_sms')


    models = {
        'sms.sms': {
            'Meta': {'object_name': 'SMS'},
            'exported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        }
    }

    complete_apps = ['sms']