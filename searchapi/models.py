# coding: utf-8
from django.db import models
from lfs.catalog.models import Product


class UniqueWord(models.Model):
    word = models.CharField(max_length=128, unique=True, primary_key=True)
    products = models.ManyToManyField(Product)
