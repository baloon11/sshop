��          �            x  "   y     �     �     �     �     �     �          
       5   )  9   _  
   �     �  r   �  �     F   �  (   ,  
   U  *   `  +   �  %   �  %   �       4        K  p   e  C   �          '  �   @     	                                             
                             Announce both for admins and users Announce for admins End time Full site down Maintenance Mode Maintenance message Maintenance messages Message Message for users Message type Service window is between %(start_time)s  and unknown Service window is between %(start_time)s and %(end_time)s Start time Stop API The application is currently undergoing scheduled maintenance. Please see the messages below for more information: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-26 17:50+0300
PO-Revision-Date: 2013-07-26 17:52+0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Сообщение для админов и пользователей Сообщение для админов Конец Полная остановка сайта Сервисное обслуживание Сервисное сообщение Сервисные сообщения Сообщение Сообщение для пользователей Тип сообщения Сервисные работы с %(start_time)s (время окончания пока не известно) Сервисные работы с %(start_time)s по %(end_time)s Начало Блокировка API Работа сайта временно приостановлена на период проведения сервисных работ. Обратите внимание на сообщение для дополнительной информации: 