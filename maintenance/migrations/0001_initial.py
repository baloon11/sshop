# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MaintenanceMessage'
        db.create_table('maintenance_maintenancemessage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 7, 26, 0, 0))),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('maintenance', ['MaintenanceMessage'])


    def backwards(self, orm):
        # Deleting model 'MaintenanceMessage'
        db.delete_table('maintenance_maintenancemessage')


    models = {
        'maintenance.maintenancemessage': {
            'Meta': {'object_name': 'MaintenanceMessage'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 7, 26, 0, 0)'})
        }
    }

    complete_apps = ['maintenance']