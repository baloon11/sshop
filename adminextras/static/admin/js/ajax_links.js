function update_ajax_links() {
    // Simple ajax link
    Suit.$(document).on("click", ".simple-ajax-link", function() {
        var url = Suit.$(this).attr("href");
        Suit.$(Suit.$(this).data("loaderselector")).loadOverStart();
        Suit.$.post(url, function(__this) {
            return function(data) {
                data = Suit.$.parseJSON(data);
                for (var html in data["html"]) {
                    Suit.$(data["html"][html][0]).html(data["html"][html][1]);
                }                
                Suit.$(Suit.$(__this).data("loaderselector")).loadOverStop();
            }
        }(this));
        return false;
    });
    
    Suit.$(document).on("click", ".ajax-confirm-link", function() {
        if(confirm(Suit.$(this).data("question"))) {
            var url = Suit.$(this).attr("href");
            Suit.$(Suit.$(this).data("loaderselector")).loadOverStart();
            Suit.$.post(url, function(__this) {
                return function(data) {   
                    data = Suit.$.parseJSON(data);
                    for (var html in data["html"]) {
                        $(data["html"][html][0]).html(data["html"][html][1]);
                    }                
                    Suit.$(Suit.$(__this).data("loaderselector")).loadOverStop();
                }
            }(this));
        }
        return false;
    });

    // Filter expander
    Suit.$(document).on("click", ".filter-expander", function() {
        var url = Suit.$(this).attr("href");
        var filter_id = Suit.$(this).data("filterid");
        if(Suit.$(this).hasClass("filter-closed")) {
            var option_place = Suit.$("#filter-options-place-"+filter_id);
            var option_wrapper = Suit.$("#filter-options-wrapper-"+filter_id);

            if(option_wrapper.length > 0) {
                Suit.$(option_wrapper).parent().show();
                Suit.$(this).html('<img src="/static/icons/toggle-small.png" />');
                Suit.$(this).removeClass("filter-closed");                
                Suit.$.post(url, function(data) {
                    data = Suit.$.parseJSON(data);
                    for (var html in data["html"])
                        Suit.$(data["html"][html][0]).html(data["html"][html][1]);
                });
            } else {
                if(option_place.length > 0) {
                    Suit.$(option_place).html('<td></td><td colspan="3" id="filter-options-wrapper-'+filter_id+'">' +
                        '<p><img src="/static/img/ajax-loader.gif" />'+Suit.$(option_place).data("placeholder")+'</p>' +
                        '</td>');
                    Suit.$(option_place).removeAttr("id");
                    Suit.$(this).html('<img src="/static/icons/toggle-small.png" />');
                    Suit.$(this).removeClass("filter-closed");
                    Suit.$.post(url, function(data) {
                        data = Suit.$.parseJSON(data);
                        for (var html in data["html"])
                            Suit.$(data["html"][html][0]).html(data["html"][html][1]);
                    });
                }
            }
        } else {
            Suit.$(this).addClass("filter-closed");
            Suit.$(this).html('<img src="/static/icons/toggle-small-expand.png" />');
            Suit.$("#filter-options-wrapper-"+filter_id).parent().hide();
            Suit.$.post('/superadmin/filters/close-options-for-filter/'+filter_id+'/', function(data) {});
        }
        return false;
    });
}


$(document).ready(function() {
    update_ajax_links();
    window.update_ajax_links = update_ajax_links;
});

$(document).ajaxSend(function(event, xhr, settings) {
    function sameOrigin(url) {
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }
    function safeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
        xhr.setRequestHeader("X-CSRFToken", $.cookie("csrftoken"));
    }
});