# coding: utf-8
from django.conf import settings

HELPBAR_DOCS_REPOSITORY = getattr(settings, 'HELPBAR_DOCS_REPOSITORY', '')

HELPBAR_URLS = getattr(settings, 'HELPBAR_URLS', tuple())

HELPBAR_INDEX_URL = getattr(settings, 'HELPBAR_INDEX_URL', '')
