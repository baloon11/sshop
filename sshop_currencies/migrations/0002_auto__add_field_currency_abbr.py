# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Currency.abbr'
        db.add_column('sshop_currencies_currency', 'abbr',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=10),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Currency.abbr'
        db.delete_column('sshop_currencies_currency', 'abbr')


    models = {
        'sshop_currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'coeffitient': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['sshop_currencies']