# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Currency'
        db.create_table('sshop_currencies_currency', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('coeffitient', self.gf('django.db.models.fields.FloatField')(default=1.0)),
        ))
        db.send_create_signal('sshop_currencies', ['Currency'])


    def backwards(self, orm):
        # Deleting model 'Currency'
        db.delete_table('sshop_currencies_currency')


    models = {
        'sshop_currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'coeffitient': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['sshop_currencies']