# coding: utf-8
from django.utils.translation import ugettext as _
from django.contrib import admin

from .models import Currency
from .utils import convert


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'abbr', 'coeffitient')
    search_fields = ['name']

    actions = ['init_data']

    def init_data(self, request, queryset):
        from lfs.core.utils import get_default_shop
        shop = get_default_shop(request)
        for q in queryset:
            q.coeffitient = convert(
                from_curr=shop.default_currency.code, to_curr=q.code)
            q.save()
    init_data.short_description = _(u"Get convertion coefficient")

admin.site.register(Currency, CurrencyAdmin)
