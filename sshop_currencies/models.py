# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Currency(models.Model):
    name = models.CharField(_(u'Name'), max_length=70)
    code = models.CharField(_(u'Code'), unique=True, max_length=10)
    abbr = models.CharField(_(u'Abbr'), max_length=10)
    format_str = models.CharField(
        _(u'Format string'),
        max_length=50, default=u'%(value).2f %(abbr)s')

    coeffitient = models.FloatField(_(u'Coeffitient'), default=1.0)

    class Meta:
        verbose_name = _(u'Currency')
        verbose_name_plural = _(u'Currencies')

    def __unicode__(self):
        return self.name

    def get_formatted_value(self, value, convert=False):
        import locale
        if not value:
            value = 0.0

        if convert:
            value *= self.coeffitient

        return locale.format_string(self.format_str, {
            'value': value,
            'abbr': self.abbr,
            'code': self.code,
            'name': self.name,
        })
