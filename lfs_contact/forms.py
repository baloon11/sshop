# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.Form):
    """Simple contact form.
    """
    name = forms.CharField(label=_(u'Name'))
    email = forms.EmailField(label=_(u'E-Mail'))
    message = forms.CharField(label=_(u'Message'), widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['message'].widget.attrs['class'] = 'input-xxlarge'
