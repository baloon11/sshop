# SSHOP

Django based all-in-one ecommerce platform.

## Requirements

- Python 2.7
- Django 1.4
- Node.js 0.8.14

## Node.js

Download Node.js sources from here: [http://nodejs.org/download/](http://nodejs.org/download/)

    ./configure
    make
    sudo make install

Install LESS

    sudo npm install -g less

Install CoffeeScript

    sudo npm install -g coffee-script

Install Stylus

    sudo npm install -g stylus

## Installing PostgreSQL

    $ sudo apt-get install postgresql
    $ sudo apt-get install postgresql-client
    $ sudo netstat -tanp |grep postgre

    $ sudo passwd postgres
    $ sudo -u postgres createuser localdev
    # Shall the new role be a superuser? (y/n) n
    # Shall the new role be allowed to create databases? (y/n) y
    # Shall the new role be allowed to create more new roles? (y/n) n

    $ sudo -u postgres psql template1
    # alter user localdev password '123456';

    # alter role postgres with encrypted password '123456';
    # \q

    $ sudo nano /etc/postgresql/9.1/main/postgresql.conf
    password_encryption = on

    $ sudo apt-get install phppgadmin
    $ /etc/init.d/apache2 restart

    $ sudo -u postgres createdb sshop
