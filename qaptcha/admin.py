# coding: utf-8
from django.contrib import admin
from qaptcha.models import Qaptcha


class QaptchaAdmin(admin.ModelAdmin):
    list_display = ('key', 'expiration')

admin.site.register(Qaptcha, QaptchaAdmin)
