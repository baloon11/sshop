from django import forms
from qaptcha.fields import QaptchaField


class TestForm(forms.Form):
    text = forms.IntegerField()
    lol = QaptchaField()
