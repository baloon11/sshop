# -*- coding: utf-8 -*-

# import logging

# from salad.terrains.common import *
# # from salad.terrains.browser import *
# from salad.terrains.djangoify import *
# from lettuce import before, after, world
# from salad.logger import logger
# from splinter.browser import Browser

# logger = logging.getLogger(__name__)
# world.old_database_name = None


# @before.runserver
# def prepare_database(server):
#     from south.management.commands import patch_for_test_db_setup
#     from django.db import connection
#     from django.core.management import call_command
#     from django.conf import settings

#     logger.info("Setting up a test database ...\n")
#     patch_for_test_db_setup()
#     world.old_database_name = settings.DATABASES["default"]["NAME"]
#     connection.creation.create_test_db()
#     call_command('syncdb', interactive=False, verbosity=0)


# @after.runserver
# def teardown_database(server):
#     from django.db import connection
#     logger.info("Destroying test database ...\n")
#     connection.creation.destroy_test_db(world.old_database_name)


# @before.each_scenario
# def init_data(scenario):
#     '''
#     Init data before each scenario
#     '''
#     from django.core.management import call_command
#     call_command('lfs_init', 'test_mode', verbosity=0)


# @after.each_scenario
# def flush_database(scenario):
#     '''
#     Flush DB after each scenario
#     '''
#     from django.core.management import call_command
#     call_command('flush', verbosity=0)
