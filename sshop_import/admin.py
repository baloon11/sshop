# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, url
from django.contrib import admin
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, render, redirect
import mimetypes

from sshop_import.models import Task

class TaskAdmin(admin.ModelAdmin):
    # fields = ('files', 'started_at', 'finished_at', 'status')
    list_display = ('files', 'started_at', 'finished_at', 'status', 'download')

    def get_urls(self):
        urls = super(TaskAdmin, self).get_urls()
        my_urls = patterns('',
                url(r'^get-1c-file/(?P<task_id>\d*)$', self.admin_site.admin_view(self.get_1c_file)),
            )
        return urls + my_urls

    def get_1c_file(self, request, task_id):
        """Return the 1c file.
        """
        file_path = Task.objects.get(pk=task_id).files.split('\n')[0]
        file_path = file_path.replace('\r', '')

        try:
            response = HttpResponse(FileWrapper(open(file_path)), content_type=mimetypes.guess_type(file_path))
            response['Content-Disposition'] = 'attachment; filename=%s' % file_path.split('/')[-1]
        except Exception, e:
            response = HttpResponse(str(e))
        return response

    def download(self, obj):
        return u'<a class="btn btn-info" title="download" href="get-1c-file/%s"><i class="icon-download-alt icon-white"></i></a>' % obj.pk
    download.allow_tags = True
    download.short_description = "download file"
    def __init__(self,*args,**kwargs):
        super(TaskAdmin, self).__init__(*args, **kwargs)
admin.site.register(Task, TaskAdmin)