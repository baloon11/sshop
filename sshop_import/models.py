from django.db import models
import datetime
from django.utils.translation import ugettext_lazy as _

class Task(models.Model):
    TASK_STATUSES = (
        (0, _(u'Authorized')),
        (5, _(u'Init')),
        (10, _(u'Saving files')),
        (15, _(u'Ready to process')),
        (20, _(u'Done successfully')),
        (25, _(u'Failed')),
        (30, _(u'In processing')),
        )

    files = models.TextField(default='')
    started_at = models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)
    finished_at = models.DateTimeField(default=datetime.datetime.now)
    status = models.IntegerField(default=0, choices=TASK_STATUSES)