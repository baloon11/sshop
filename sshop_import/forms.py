from django import forms

class ImportProductForm(forms.Form):
    file = forms.FileField(required=True)

class ImportPriceForm(forms.Form):
    file = forms.FileField(required=True)