# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django import forms
from .models import Callback


class CallbackForm(forms.ModelForm):
    class Meta:
        model = Callback
        fields = ('name', 'phone', 'time_from', 'time_to')


def callback(request, template_name="callback/callback.html"):
    if request.method == "POST" and request.is_ajax():
        form = CallbackForm(data=request.POST)
        if form.is_valid():
            callback = form.save(commit=False)
            if str(callback.time_from) == '00:00:00' and \
                    str(callback.time_to) == '23:59:00':
                message = _(u'We\'ll call you back during a day')
            else:
                message = _(u'We\'ll call you from %(from)s to %(to)s') % {
                    'from': callback.time_from, 'to': callback.time_to}
            callback.save()
            return render_to_response(
                template_name,
                RequestContext(request, {'message': message}))

    else:
        form = CallbackForm(
            initial={'time_from': '00:00', 'time_to': '23:59'})

    return render_to_response(template_name, RequestContext(request, {
        "form": form,
    }))
