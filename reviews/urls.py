from django.conf.urls.defaults import *

# Catalog
urlpatterns = patterns(
    'reviews.views',
    url(r'^add/(?P<content_type_id>\d*)/(?P<content_id>\d*)$',
        "add_form", name="reviews_add"),
    url(r'^preview$', "preview", name="reviews_preview"),
    url(r'^reedit$', "reedit_or_save", name="reviews_reedit"),
    url(r'^thank-you$', "thank_you", name="reviews_thank_you"),
    url(r'^already-rated$', "already_rated", name="reviews_already_rated"),
    url(r'^average-rating$', "average_rating", name="reviews_average_for_instance"),
    url(r'^mark$', "mark", name="reviews_mark_for_instance"),
    url(r'^amount-feedbacks$', "amount_feedbacks", name="reviews_amount_feedbacks_for_instance")
)
