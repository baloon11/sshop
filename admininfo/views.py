# coding: utf-8
import os
import sys
import platform
import getpass
import psutil
import django

from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from django.contrib import messages


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def system_info(request, template='admininfo/system-info.html'):
    PID = os.getpid()
    system_user = getpass.getuser()
    memory_stats = psutil.virtual_memory()

    current_process = psutil.Process(PID)

    databases = getattr(settings, 'DATABASES', None)
    db_driver = _(u'Cannot find default database.')
    if databases is not None:
        default_db = databases.get('default', None)
        if default_db is not None:
            db_driver = default_db.get('ENGINE', None)

    caches = getattr(settings, 'CACHES', None)
    cache_driver = _(u'Cannot find default cache config.')
    if caches is not None:
        default_cache = caches.get('default', None)
        if default_cache is not None:
            cache_driver = default_cache.get('BACKEND', None)

    return render_to_response(template, RequestContext(request, {
        'shop_version': getattr(
            settings, 'SSHOP_VERSION', _(u'SSHOP_VERSION is not defined.')),
        'python_version': sys.version,
        'django_version': django.get_version(),
        'os_version': platform.platform(),
        'virtualenv_dir': getattr(
            sys, 'prefix', _(u'sys.prefix is not defined.')),
        'system_user': system_user,
        'memory_stats': memory_stats,
        'pid': PID,
        'current_process': current_process,
        'db_driver': db_driver,
        'cache_driver': cache_driver,
    }))


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def clear_cache(request):
    from lfs.caching.utils import clear_cache as c_clear
    c_clear()
    messages.success(request, _(u'Cache was cleared.'))
    return HttpResponseRedirect(reverse('admin_system_info'))


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def restart_engine(request):
    from sbits_plugins import mediator
    mediator.publish(None, 'restart_project')
    messages.success(request, _(u'Engine was restarted.'))
    return HttpResponseRedirect(reverse('admin_system_info'))


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def collect_garbage(request):
    import gc
    gc.collect()
    messages.success(
        request, _(u'Garbage collector cleared the collected objects.'))
    return HttpResponseRedirect(reverse('admin_system_info'))
