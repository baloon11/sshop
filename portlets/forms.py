# coding: utf-8
from django import forms
from django.contrib.admin.sites import site
from django.contrib.admin.widgets import ForeignKeyRawIdWidget
from django.db.models.fields.related import ManyToOneRel
from .models import (
    PortletBlocking,
)
from lfs.catalog.models import Category
from adminextras.widgets import ContentTypeSelect


class PortletBlockingForm(forms.ModelForm):
    class Meta:
        model = PortletBlocking

    def __init__(self, *args, **kwargs):
        super(PortletBlockingForm, self).__init__(*args, **kwargs)
        try:
            model = self.instance.content_type.model_class()
            model_key = model._meta.pk.name
        except Exception:
            model = Category
            model_key = 'id'

        self.fields['content_id'].widget = \
            ForeignKeyRawIdWidget(
                rel=ManyToOneRel(model, model_key), admin_site=site)
        self.fields['content_type'].widget.widget = \
            ContentTypeSelect(
                'lookup_',
                self.fields['content_type'].widget.widget.attrs,
                self.fields['content_type'].widget.widget.choices)
