��          �   %   �      0     1     8  ,   V  O   �  %   �     �        
          (        ?     S     d     v     �     �     �     �     �  G   �                 �       �  9   �  M   !  �   o  P   "     s     �     �     �  b   �     -  '   M  '   u  )   �  )   �     �      	     	  
   	  b   )	     �	     �	     �	             	                    
                                                                                              Active Blocked in inheritors`s slots Class name of the portlet, e.g. TextPortlet. Database may not has PortletAssignment objects without link to correct portlet. Delete incorrect items from database. Failed Fixed Horizontal Name Name of the portlet which used in admin. Portlet assignments Portlet blocking Portlet blockings Portlet registration Portlet registrations Position Slot Slots Text There are %d PortletAssignment objects without link to correct portlet. Title Type Vertical Project-Id-Version: LFS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-08-02 16:49+0300
PO-Revision-Date: 2014-08-02 16:50+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Активный Заблокирован в дочерних слотах Название класса портлета, например TextPortlet. База данных не должна содержать объекты PortletAssignment без ссылок на корректные экземпляры портлетов. Удалить некорректные записи из базы данных. Не удалось Исправлено Горизонтальный Название Название портлета которое используется в интерфейсе. Задания портлета Блокировка портлетов Блокировки портлетов Регистрация портлетов Регистрации портлетов Позиция Колонка Колонки Текст Найдено %d объектов PortletAssignment с некорректными данными. Заголовок Тип Вертикальный 