��          �   %   �      @     A     H  ,   f  O   �  %   �     	       
        !  (   &     O     R     f     w     �     �     �     �     �     �  G   �                  �  )     �  #   �  ,   �  V   	  (   `     �     �     �     �  /   �     �     �     �     	          2     F     M     R     X  Q   ]     �     �     �             	                    
                                                                                             Active Blocked in inheritors`s slots Class name of the portlet, e.g. TextPortlet. Database may not has PortletAssignment objects without link to correct portlet. Delete incorrect items from database. Failed Fixed Horizontal Name Name of the portlet which used in admin. OK Portlet assignments Portlet blocking Portlet blockings Portlet registration Portlet registrations Position Slot Slots Text There are %d PortletAssignment objects without link to correct portlet. Title Type Vertical Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-23 17:04+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 Aktivní Zablokovaný ve slotech inheritorů Název třídy portletu, např. TextPortlet. Databáze nesmí obsahovat PortletAssignment objekty bez odkazu na správný  portlet. Smazat nesprávné položky z databáze. Neuspěšně Opraveno Horizontální Jméno Jméno portletu, který se používá v adminu. OK Úlohy portletu Portlet blocking Blokování portletu Registrace portletu Registrace portletu Pozice Slot Sloty Text Existuje %d PortletAssignment objekty bez odkazu na správný to correct portlet. Název Typ Vertikální 