from django.contrib import admin

from portlets.models import PortletAssignment
from portlets.models import PortletBlocking
from portlets.models import PortletRegistration
from portlets.models import Slot
from .forms import PortletBlockingForm

admin.site.register(PortletAssignment)


class PortletRegistrationAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'active')
    search_fields = ['name']
    fields = ['name', 'type', 'active']
    list_editable = ('active',)

admin.site.register(PortletRegistration, PortletRegistrationAdmin)


class PortletBlockingAdmin(admin.ModelAdmin):
    list_display = ('slot', 'content')
    search_fields = ['slot__name']
    form = PortletBlockingForm

admin.site.register(PortletBlocking, PortletBlockingAdmin)


class SlotAdmin(admin.ModelAdmin):
    list_display = ('name', 'type_of_slot')
    search_fields = ['name']

admin.site.register(Slot, SlotAdmin)
