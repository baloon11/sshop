# coding: utf-8
import re

from django.db import models
from django.utils.translation import ugettext_lazy as _

from lfs.catalog.models import Category


class FilterUrlTemplate(models.Model):
    category = models.ForeignKey(Category)
    template = models.TextField(_(u"Template"))
    validators = models.TextField(_(u"Validators"), blank=True)
    order = models.IntegerField(_(u"Order"), default=0)

    class Meta:
        verbose_name = _(u'Template')
        verbose_name_plural = _(u'Templates')
        ordering = ('order',)

    def __unicode__(self):
        return '%s' % (self.template)

    def get_validator_for_parameter(self, parameter):
        validators = {}

        for v in self.validators.replace('\r', '').split('\n'):
            if not v:
                continue
            tt = v.split(':')
            validators[tt[0]] = tt[1]

        try:
            return validators[parameter]
        except KeyError:
            return None

    def get_all_template_parameters(self):
        """ Return list of all parameters in current template
        """
        c = re.compile(self.template)
        return c.groupindex.keys()
