# -*- coding:utf-8 -*-
from django.template import Library
from django.contrib.sites.models import Site

register = Library()

@register.inclusion_tag('loginza/share_us.html', takes_context=True)
def share_us_widget(context):
    site = Site.objects.get_current()
    request = context.get('request')
    url = request.build_absolute_uri()
    return {
        'url':url,
    }


