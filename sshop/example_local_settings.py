# -*- coding: utf-8 -*-
LOCAL_SETTINGS = True
from settings import *

DEBUG = True

EMAIL_DEBUG = DEBUG
CONTACT_EMAIL = 'sshop@the7bits.com'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
DEFAULT_FROM_EMAIL = 'no-reply@the7bits.com'

# COMPRESS_ENABLED = True
# COMPRESS_OFFLINE = True

INSTALLED_APPS = (
     'videostyle_theme',
) + INSTALLED_APPS


# Extentions
ALLOW_REGIONS = True


if DEBUG:
    INTERNAL_IPS = ('127.0.0.1')

    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
        'sshop.middleware.ProfileMiddleware',
        'lfs.utils.middleware.ProfileMiddleware',
        'sshop.middleware.memoryMiddleware',
        # 'sshop.middleware.LoggingMiddleware',
    )

    INSTALLED_APPS += (
        'debug_toolbar',
    )

    DEBUG_TOOLBAR_PANELS = (
        'debug_toolbar.panels.version.VersionDebugPanel',
        'debug_toolbar.panels.timer.TimerDebugPanel',
        'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
        'debug_toolbar.panels.headers.HeaderDebugPanel',
        'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
        'debug_toolbar.panels.template.TemplateDebugPanel',
        'debug_toolbar.panels.sql.SQLDebugPanel',
        'debug_toolbar.panels.signals.SignalDebugPanel',
        'debug_toolbar.panels.logger.LoggingPanel',
    )

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }

PLUGINS_REPOSITORY_DEFAULT_URL = 'http://sitepanel.the7bits.com/'
PROJECT_IDENTIFIER = 'testproj' # example
PROJECT_SECRET_CODE = '123456' # example
