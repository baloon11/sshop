# coding: utf-8
import logging

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
import autocomplete_light

logger = logging.getLogger('sshop')

autocomplete_light.autodiscover()
admin.autodiscover()
from sshop.admin import *

handler500 = 'lfs.core.views.server_error'

# import warnings
# from django.core.cache import CacheKeyWarning
# # warnings.simplefilter('error', DeprecationWarning)
# warnings.simplefilter('error', CacheKeyWarning)

urlpatterns = patterns(
    '',
    url(r'^logs/', include('logtailer.urls')),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^report_builder/', include('report_builder.urls')),
    url(r'^simple_import/', include('simple_import.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'filebrowser/',
        include('filebrowser.urls')),  # Deprecated in 0.4
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'doc/',
        include('django.contrib.admindocs.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'diagnostic/',
        include('diagnostic.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'info/',
        include('admininfo.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'config/',
        include('adminconfig.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'catalog/',
        include('lfs.catalog.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'filters/',
        include('lfs.filters.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'fields/',
        include('lfs.fields.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'mail/',
        include('lfs.mail.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'order/',
        include('lfs.order.urls')),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False) + r'logout/',
        'lfs.core.views.logout_view'),
    (
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False),
        include('smuggler.urls')),
    url(
        r'^' + getattr(settings, 'ADMIN_ROOT_PATH', False),
        include(admin.site.urls)),
)


if getattr(settings, 'DEBUG', False):
    urlpatterns += patterns(
        'django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': getattr(settings, 'MEDIA_ROOT', ''),
            'show_indexes': True,
            }),
    )


# Sitemaps
urlpatterns += patterns(
    'lfs.core.sitemap',
    url(r'^sitemap\.xml$', 'index'),
    url(r'^sitemap/category/(?P<category>.+)\.xml$', 'products_sitemap'),
    url(r'^sitemap/pages\.xml$', 'pages_sitemap'),
)

# Append extra sitemaps
for s in settings.EXTRA_SITEMAPS:
    urlpatterns += patterns(
        s['module'],
        url(s['url'], s['view'])
    )

urlpatterns += patterns(
    '',
    (r'^reviews/', include('reviews.urls')),
    (r'^import/', include('sshop_import.urls')),
    (r'^loginza/', include('loginza.urls')),
    (r'^qaptcha/', include('qaptcha.urls')),
    (r'^forms/', include('form_designer.urls')),
    (r'', include('lfs.core.urls')),
)

# Load urls from extensions
import pkg_resources
for entrypoint in pkg_resources.iter_entry_points(
        group=settings.ENTRY_POINT_GROUP):
    if entrypoint.name == 'update_urls':
        update_urls = entrypoint.load()
        new_data = update_urls(globals())
        for i in new_data.keys():
            globals()[i] = new_data[i]
    elif entrypoint.name == 'install':
        module_name = entrypoint.module_name
        from sbits_plugins.models import Extension, Repository
        try:
            ext = Extension.objects.get(slug=module_name)
            if not ext.is_initialized:
                install_func = entrypoint.load()
                try:
                    install_func(settings.PROJECT_DIR)
                    ext.is_initialized = True
                except Exception, e:
                    logger.error(
                        u'Trouble when looking for install extension '
                        u'"%(module)s": %(exception)s' % {
                            'module': module_name,
                            'exception': unicode(e),
                        })
            ext.is_installed = True
            ext.save()
        except Extension.DoesNotExist:
            ext = Extension.objects.create(
                repository=Repository.objects.all()[0],
                name=module_name,
                slug=module_name,
                is_installed=True,
                installed_version=entrypoint.dist.version,
                latest_version=entrypoint.dist.version,
                downloadable_links=[{
                    'version': entrypoint.dist.version,
                    'file': '',
                    'changelog': '',
                }])
            logger.warning(
                u'Extension "%(module)s" is installed but Extension record '
                u'in DB not found. Fixed automatically.' % {
                    'module': module_name,
                })
        except Extension.MultipleObjectsReturned:
            extensions = Extension.objects.filter(slug=module_name)
            first_pk = extensions[0].pk
            extensions_for_remove = extensions.exclude(pk=first_pk)
            extensions_for_remove.delete()
            logger.error(
                u'Found more than one Extension record for extension '
                u'"%(module)s" in DB. Dublicate records were removed.' % {
                    'module': module_name,
                })
    elif entrypoint.name == 'upgrade':
        module_name = entrypoint.module_name
        from sbits_plugins.models import Extension
        try:
            ext = Extension.objects.get(slug=module_name)
            if ext.need_run_update:
                update_func = entrypoint.load()
                try:
                    update_func(
                        ext.old_version,
                        ext.installed_version,
                        settings.PROJECT_DIR)
                    ext.need_run_update = False
                    ext.save()
                except Exception, e:
                    logger.error(
                        u'Trouble when looking for upgrade extension '
                        u'"%(module)s": %(exception)s' % {
                            'module': module_name,
                            'exception': unicode(e),
                        })
        except Extension.DoesNotExist:
            logger.error(
                u'Cannot find Extension record in DB for extension '
                u'"%(module)s" when looking for update script.' % {
                    'module': module_name,
                })
        except Extension.MultipleObjectsReturned:
            extensions = Extension.objects.filter(slug=module_name)
            first_pk = extensions[0].pk
            extensions_for_remove = extensions.exclude(pk=first_pk)
            extensions_for_remove.delete()
            logger.error(
                u'Found more than one Extension record for extension '
                u'"%(module)s" in DB. Dublicate records were removed.' % {
                    'module': module_name,
                })

# Set macroses as signal receivers
from lfs.core.utils import import_symbol
from macroses.models import Macros
from macroses.listeners import get_macros_listener
macroses = Macros.objects.exclude(signal_name='')
funcs = []
for macros in macroses.iterator():
    try:
        signal = import_symbol(macros.signal_name)
    except Exception:
        logger.error(
            u'Cannot import signal "%s" for macros (pk=%s).' % (
                macros.signal_name,
                macros.id))
        continue
    f = get_macros_listener(macros.id)
    funcs.append(f)
    if macros.signal_sender:
        try:
            sender = import_symbol(macros.signal_sender)
        except Exception:
            logger.error(
                u'Cannot import sender "%s" for macros (pk=%s).' % (
                    macros.signal_sender,
                    macros.id))
            continue
        signal.connect(f, sender=sender)
    else:
        signal.connect(f)

# Try to update urlpatterns if macros wants it
macroses = Macros.objects.all()
for macros in macroses.iterator():
    t = macros.change_urlpatterns(urlpatterns)
    if t is not None:
        urlpatterns = t

from lfs.core.signals import project_startup
project_startup.send(None)
