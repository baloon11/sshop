# coding: utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.core import urlresolvers
from django.core.urlresolvers import reverse
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.contrib.auth.admin import UserAdmin as oldUserAdmin
from django.contrib.sessions.models import Session
from django.conf.urls import patterns, url

try:
    import django
    User = django.contrib.auth.get_user_model()
except:
    from django.contrib.auth.models import User


class UserAdmin(oldUserAdmin):
    list_display = (
        'username',
        'email',
        'is_staff',
        'is_superuser',
        'last_login',
        'date_joined',
        'get_groups',
    )
    ordering = ('-date_joined', 'username')
    date_hierarchy = 'date_joined'
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    change_form_template = 'admin/user_change_form.html'

    def get_groups(self, obj):
        groups = []
        for g in obj.groups.all():
            groups.append('<a href="%s">%s</a>' % (
                urlresolvers.reverse(
                    'admin:%s_%s_change' % ('auth', 'group'),
                    args=[g.id]),
                g.name,
            ))
        return ', '.join(groups)
    get_groups.short_description = _('Groups')
    get_groups.allow_tags = True

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        user = User.objects.get(pk=object_id)
        customer = user.customer_set.all()[0]
        extra_context['customer'] = customer
        return super(UserAdmin, self).change_view(
            request, object_id,
            form_url, extra_context=extra_context)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)


class SessionAdmin(admin.ModelAdmin):
    list_display = ['session_key', '_session_data', 'expire_date']
    date_hierarchy = 'expire_date'
    search_fields = ('session_key',)
    ordering = ['-expire_date']

    def _session_data(self, obj):
        return obj.get_decoded()

    def get_urls(self):
        """Returns the additional urls used by the Session admin."""
        urls = super(SessionAdmin, self).get_urls()
        admin_site = self.admin_site
        opts = self.model._meta
        info = opts.app_label, opts.module_name,
        session_urls = patterns(
            "",
            url(
                "^set-clear-task/$",
                admin_site.admin_view(self.set_clear_task_view),
                name='%s_%s_setcleartask' % info),
        )
        return session_urls + urls

    def set_clear_task_view(self, request, extra_context=None):
        from django.core.exceptions import PermissionDenied
        # check if user has change or add permissions for model
        if not self.has_change_permission(request) \
                and not self.has_add_permission(request):
            raise PermissionDenied
        from tasks.api import TaskQueueManager
        task_manager = TaskQueueManager()
        task_manager.schedule(
            'lfs.core.jobs.clear_sessions',
            repeat=60,
        )
        model = self.model
        opts = model._meta
        return HttpResponseRedirect(
            reverse("%s:%s_%s_changelist" % (
                self.admin_site.name, opts.app_label, opts.module_name)))

    def changelist_view(self, request, extra_context=None):
        """Renders the change view."""
        opts = self.model._meta
        context = {
            "setcleartask_url": reverse(
                "%s:%s_%s_setcleartask" % (
                    self.admin_site.name,
                    opts.app_label,
                    opts.module_name)),
        }
        context.update(extra_context or {})
        return super(SessionAdmin, self).changelist_view(request, context)

admin.site.register(Session, SessionAdmin)
