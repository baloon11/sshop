��    3      �  G   L      h     i     r     �     �     �     �     �     �     �  	   �     �     �     �  !        :     C     T     i     }     �     �  %   �     �  
   �     �     �     �               "     '  	   -     7     G  
   W     b  
   h     s     �     �     �     �     �     �     �     �     �     	          "  �  9     	  !   .	  .   P	     	     �	  )   �	     �	     �	     �	      
  (   
     8
  8   K
  G   �
     �
     �
  $          %  0   F  .   w     �  N   �          (  !   5     W  
   h  "   s     �     �  
   �     �  +   �  !   	  #   +     O     ^     |  "   �     �  D   �               .     H  F   W     �  #   �      �  &   �               -                0      ,   &                 	   *                                         '       (              /   $           !   )   %      3                          "             
           .      #      2          +   1              Accounts By default email By default phone Captcha Catalog Checkout Compatibility Configuration Customer Customers Customers and orders DB task queue Default price calculator Default shipping price calculator Diagnose Discount on cart Discount on category Discount on product Do not use captcha Dummy task queue Email Enter full number with regional code. FAQ questions FAQ topics File management General Groups HTML with images Import data Jobs Login Marketing Message sending Null task queue Pagination Phone Plain text Registration Reports and analytics Service Set task for clear sessions Settings Shop Simple search config Slider Standard regex and rules System info Task priorities Username Vouchers and discounts Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-02 19:39+0300
PO-Revision-Date: 2014-03-05 12:53+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Облікові записи За стандартним email За стандартним телефоном Каптча Каталог Оформлення замовлення Сумісність Конфігурація Клієнт Клієнти Клієнти та замовлення Черга в БД Стандартиний калькулятор ціни Стандартний калькулятор ціни доставки Діагностика Знижка на кошик Знижка на категорію Знижка на продукт Не використовувати каптчу Запускати задачі негайно Електронна пошта Введіть повний номер з регіональним кодом. Питання FAQ Теми FAQ Керування файлами Загальне Групи HTML із зображеннями Імпорт даних Завдання Логін Маркетинг Відсилання повідомлень Ігнорувати задачі Нумерація сторінок Телефон Звичайний текст Реєстрація Звіти та аналітика Сервіс Встановити задачу для очистки сессій Налаштування Магазин Простий пошук Слайдер Стандартні регулярні вирази і правила Про систему Пріоритети завдань Ім’я користувача Сертифікати і знижки 