# coding: utf-8

import sys
import os
import re
import hotshot, hotshot.stats
import tempfile
import StringIO
import logging
import time

from django.template import RequestContext, loader
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseForbidden, HttpResponsePermanentRedirect
from django.http import QueryDict
from django.utils.cache import add_never_cache_headers
from django.utils import timezone
from django.db import connection

from pympler import muppy
from pympler.muppy import summary
from pympler import refbrowser
from pympler.asizeof import asizeof


words_re = re.compile(r'\s+')

group_prefix_re = [
    re.compile("^.*/django/[^/]+"),
    re.compile("^(.*)/[^/]+$"),  # extract module path
    re.compile(".*"),           # catch strange entries
]


class DeleteNoneMiddleware(object):
    def process_request(self, request):
        new_get = QueryDict('', mutable=True)

        for key in request.GET.iterkeys():
            tmp_list = request.GET.getlist(key)
            tmp_list = [i for i in tmp_list if i not in (None, 'none', 'None')]
            if tmp_list:
                new_get.setlist(key, tmp_list)
        request.GET = new_get


class TimezoneMiddleware(object):
    def process_request(self, request):
        tz = request.session.get('django_timezone')
        if tz:
            timezone.activate(tz)


class Http403(Exception):
    pass


def render_to_403(*args, **kwargs):
    """
        Returns a HttpResponseForbidden whose content
        is filled with the result of calling
        django.template.loader.render_to_string() with the passed arguments.
    """
    if not isinstance(args, list):
        args = []
        args.append('403.html')

    httpresponse_kwargs = {'mimetype': kwargs.pop('mimetype', None)}
    response = HttpResponseForbidden(
        loader.render_to_string(*args, **kwargs), **httpresponse_kwargs)

    return response


class Http403Middleware(object):
    def process_exception(self, request, exception):
        if isinstance(exception, Http403):
            if settings.DEBUG:
                raise PermissionDenied
            return render_to_403(context_instance=RequestContext(request))


class ProfileMiddleware(object):
    """
    Displays hotshot profiling for any view.
    http://yoursite.com/yourview/?prof

    Add the "prof" key to query string by appending ?prof (or &prof=)
    and you'll see the profiling results in your browser.
    It's set up to only be available in django's debug mode,
    is available for superuser otherwise,
    but you really shouldn't add this middleware
    to any production configuration.

    WARNING: It uses hotshot profiler which is not thread safe.
    """
    def process_request(self, request):
        if (settings.DEBUG or request.user.is_superuser) and\
                'prof' in request.GET:
            self.tmpfile = tempfile.mktemp()
            self.prof = hotshot.Profile(self.tmpfile)

    def process_view(self, request, callback, callback_args, callback_kwargs):
        if (settings.DEBUG or request.user.is_superuser) and\
                'prof' in request.GET:
            return self.prof.runcall(
                callback, request, *callback_args, **callback_kwargs)

    def get_group(self, file):
        for g in group_prefix_re:
            name = g.findall(file)
            if name:
                return name[0]

    def get_summary(self, results_dict, sum):
        list = [(item[1], item[0]) for item in results_dict.items()]
        list.sort(reverse=True)
        list = list[:40]

        res = "      tottime\n"
        for item in list:
            res += "%4.1f%% %7.3f %s\n" % (
                100*item[0]/sum if sum else 0, item[0], item[1])

        return res

    def summary_for_files(self, stats_str):
        stats_str = stats_str.split("\n")[5:]

        mystats = {}
        mygroups = {}

        sum = 0

        for s in stats_str:
            fields = words_re.split(s)
            if len(fields) == 7:
                time = float(fields[2])
                sum += time
                file = fields[6].split(":")[0]

                if file not in mystats:
                    mystats[file] = 0
                mystats[file] += time

                group = self.get_group(file)
                if group not in mygroups:
                    mygroups[group] = 0
                mygroups[group] += time

        return "<pre>" + \
               " ---- By file ----\n\n" + self.get_summary(mystats, sum) + \
               "\n" + \
               " ---- By group ---\n\n" + self.get_summary(mygroups, sum) + \
               "</pre>"

    def process_response(self, request, response):
        if (settings.DEBUG or request.user.is_superuser) and \
                'prof' in request.GET:
            self.prof.close()

            out = StringIO.StringIO()
            old_stdout = sys.stdout
            sys.stdout = out

            stats = hotshot.stats.load(self.tmpfile)
            stats.sort_stats('time', 'calls')
            stats.print_stats()

            sys.stdout = old_stdout
            stats_str = out.getvalue()

            if response and response.content and stats_str:
                response.content = "<pre>" + stats_str + "</pre>"

            response.content = "\n".join(response.content.split("\n")[:40])

            response.content += self.summary_for_files(stats_str)

            os.unlink(self.tmpfile)

        return response


def output_function(o):
    return str(type(o))


class memoryMiddleware(object):
    """
    Measure memory taken by requested view, and response
    """
    def process_request(self, request):
        if (settings.DEBUG or request.user.is_superuser) and \
                'mem' in request.GET:
            req = request.META['PATH_INFO']
            if req.find('site_media') == -1:
                self.start_objects = muppy.get_objects()

    def process_response(self, request, response):
        if (settings.DEBUG or request.user.is_superuser) and \
                'mem' in request.GET:
            req = request.META['PATH_INFO']
            if req.find('site_media') == -1:
                print req
                self.end_objects = muppy.get_objects()
                sum_start = summary.summarize(self.start_objects)
                sum_end = summary.summarize(self.end_objects)
                diff = summary.get_diff(sum_start, sum_end)
                summary.print_(diff)
                # print '~~~~~~~~~'
                # cb = refbrowser.ConsoleBrowser(response, maxdepth=2, str\
                #    _func=output_function)
                # cb.print_tree()
                print '~~~~~~~~~'
                a = asizeof(response)
                print 'Total size of response object in kB: %s' % str(a/1024.0)
                print '~~~~~~~~~'
                a = asizeof(self.end_objects)
                print 'Total size of end_objects in MB: %s' % str(a/1048576.0)
                b = asizeof(self.start_objects)
                print 'Total size of start_objects in MB: %s' % str(
                    b/1048576.0)
                print '~~~~~~~~~'
        return response


class LoggingMiddleware(object):

    def process_request(self, request):
        self.start_time = time.time()

    def process_response(self, request, response):
        try:
            remote_addr = request.META.get('REMOTE_ADDR')
            if remote_addr in getattr(settings, 'INTERNAL_IPS', []):
                remote_addr = request.META.get(
                    'HTTP_X_FORWARDED_FOR') or remote_addr
            user_email = "-"
            extra_log = ""
            if hasattr(request, 'user'):
                user_email = getattr(request.user, 'email', '-')
            req_time = time.time() - self.start_time
            content_len = len(response.content)
            if settings.DEBUG:
                sql_time = sum(
                    float(q['time']) for q in connection.queries) * 1000
                extra_log += " (%s SQL queries, %s ms)" % (
                    len(connection.queries), sql_time)
            logging.info("%s %s %s %s %s %s (%.02f seconds)%s" % (
                remote_addr,
                user_email,
                request.method,
                request.get_full_path(),
                response.status_code,
                content_len,
                req_time,
                extra_log)
            )
        except Exception, e:
            logging.error("LoggingMiddleware Error: %s" % e)
        return response


class DisableClientCachingMiddleware(object):
    '''
    This middleware set correct HTTP headers,
    that provide not caching response.
    It middleware process all response.
    '''
    def process_response(self, request, response):
        add_never_cache_headers(response)
        return response


# from django.db import connection
from django.utils.log import getLogger

logger = getLogger(__name__)


class QueryCountDebugMiddleware(object):
    """
    This middleware will log the number of queries run
    and the total time taken for each request (with a
    status code of 200). It does not currently support
    multi-db setups.
    """
    def process_response(self, request, response):
        if response.status_code == 200:
            total_time = 0

            for query in connection.queries:
                query_time = query.get('time')
                if query_time is None:
                    # django-debug-toolbar monkeypatches the connection
                    # cursor wrapper and adds extra information in each
                    # item in connection.queries. The query time is stored
                    # under the key "duration" rather than "time" and is
                    # in milliseconds, not seconds.
                    query_time = query.get('duration', 0) / 1000
                total_time += float(query_time)

            logger.debug('%s queries run, total %s seconds' % (
                len(connection.queries), total_time))
            print '%s queries run, total %s seconds' % (
                len(connection.queries), total_time)
        return response


class MakeLowerUrlMiddleware(object):

    # def process_request(self, request):
    def process_response(self, request, response):
        if getattr(settings, 'URL_CASE_INSENSITIVE', False):
            if response.status_code != 404:
                return response
            try:
                from lfs.catalog.models import Category, Product
                from lfs.page.models import Page
                from lfs.filters.models import Filter, FilterOption
                from urllib import quote, urlencode
                make_redirect = False
                categories = [c['slug'] for c in Category.objects.filter(
                    exclude_from_navigation=False).values('slug')]
                products = [
                    p['slug'] for p in Product.objects.all().values('slug')]
                filters = [
                    f['identificator'] for f in Filter.objects.all().values(
                        'identificator')]
                filter_options = [
                    fo['identificator'] for fo in FilterOption.objects.all(
                    ).values('identificator')]
                original_path = request.get_full_path()
                path = request.path
                ends_with_slash = path.endswith('/')
                splitted_path = path.strip('/').split('/')

                if len(splitted_path) == 1:
                    if (splitted_path[0].lower() in categories and
                            splitted_path[0].lower() != splitted_path[0]) or\
                            (splitted_path[0].lower() in products and
                                splitted_path[0].lower() != splitted_path[0])\
                            or (
                                getattr(
                                    settings, 'SBITS_PAGES_INFO_PAGE_URL', None
                                )
                                and splitted_path[0].lower() ==
                                settings.SBITS_PAGES_INFO_PAGE_URL and
                                splitted_path[0] !=
                                settings.SBITS_PAGES_INFO_PAGE_URL) or (
                            splitted_path[0].lower() == 'pages' and
                            splitted_path[0].lower() != splitted_path[0]):
                        path = path.lower()
                        make_redirect = True
                elif len(splitted_path) > 1:
                    if splitted_path[0].lower() in categories:
                        new_paths = []
                        if splitted_path[0].lower() != splitted_path[0]:
                            new_paths.append(splitted_path[0].lower())
                            make_redirect = True
                        else:
                            new_paths.append(splitted_path[0])
                        for sp in splitted_path[1:]:
                            if sp.lower() in filter_options\
                                    and sp.lower() != sp:
                                new_paths.append(sp.lower())
                                make_redirect = True
                            else:
                                new_paths.append(sp)

                        path = '/' + '/'.join(new_paths)
                    elif splitted_path[0].lower() == 'page':
                        pages = [
                            p['slug'] for p in Page.objects.all(
                            ).values('slug')]
                        new_paths = []
                        if splitted_path[0].lower() != splitted_path[0]:
                            new_paths.append(splitted_path[0].lower())
                            make_redirect = True
                        else:
                            new_paths.append(splitted_path[0])
                        for sp in splitted_path[1:]:
                            if sp.lower() in pages and\
                                    sp.lower() != sp:
                                new_paths.append(sp.lower())
                                make_redirect = True
                            else:
                                new_paths.append(sp)
                        path = '/' + '/'.join(new_paths)
                    elif getattr(
                        settings, 'SBITS_PAGES_INFO_PAGE_URL', None) and\
                            splitted_path[0].lower() ==\
                            settings.SBITS_PAGES_INFO_PAGE_URL:
                        if 'sbits_pages' in settings.INSTALLED_APPS:
                            from sbits_pages.models import (
                                SCMS_Category, SCMS_News)
                            sbp_categories = [
                                c['slug'] for c in SCMS_Category.objects.all(
                                ).values('slug')]
                            sbp_news = [
                                n['slug'] for n in SCMS_News.objects.all(
                                ).values('slug')]
                            new_paths = []
                            if splitted_path[0].lower() != splitted_path[0]:
                                new_paths.append(splitted_path[0].lower())
                                make_redirect = True
                            else:
                                new_paths.append(splitted_path[0])
                            for sp in splitted_path[1:]:
                                if (sp.lower() in sbp_categories or
                                    sp.lower() in sbp_news or
                                    sp.lower().replace('.html', '') in sbp_news
                                    )\
                                        and sp.lower() != sp:
                                    new_paths.append(sp.lower())
                                    make_redirect = True
                                else:
                                    new_paths.append(sp)
                            path = '/' + '/'.join(new_paths)
                if request.GET:
                    for key in request.GET.keys():
                        if key.lower() in filters:
                            val = request.GET[key].split(',')
                            new_vals = []
                            for v in val:
                                if v.lower() != v:
                                    make_redirect = True
                                new_vals.append(
                                    quote(
                                        v.lower(
                                        ) if v.lower() in filter_options else v
                                    )
                                )
                            if key == key.lower():
                                request.GET[key] = ','.join(new_vals)
                            else:
                                del request.GET[key]
                                request.GET[key.lower()] = ','.join(new_vals)
                                make_redirect = True
                        elif key.lower() == 'start' and key != 'start':
                            val = request.GET[key]
                            del request.GET[key]
                            request.GET[key.lower()] = val
                            make_redirect = True
                if ends_with_slash:
                    path += '/'
                if request.GET:
                    new_path = '?'.join(
                        [path, urlencode(request.GET)]).replace('%2C', ',')
                else:
                    new_path = path
                if original_path != new_path and make_redirect:
                    return HttpResponsePermanentRedirect(
                        'http://%s%s' % (
                            request.get_host(),
                            new_path
                        )
                    )
                else:
                    return response
            except:
                return response
        return response
