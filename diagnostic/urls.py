# coding: utf-8
from django.conf.urls.defaults import patterns, url

# These url patterns use for admin interface
urlpatterns = patterns(
    'diagnostic.views',
    url(r'^diagnose/$', 'diagnose', name="admin_diagnose"),
    url(r'^run-test/$', 'run_test', name="admin_diagnose_run_test"),
    url(r'^fix-test/$', 'fix_test', name="admin_diagnose_fix_test"),
)
