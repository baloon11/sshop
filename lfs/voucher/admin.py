# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db import IntegrityError
from suit.admin import SortableModelAdmin

from .models import Voucher, VoucherGroup
from .forms import GenerateVouchersForm
from .utils import create_voucher_number


class VoucherGroupAdmin(SortableModelAdmin):
    sortable = 'position'
    list_display = ['name', 'creator', 'creation_date']
    search_fields = ['name']
    date_hierarchy = 'creation_date'
    actions = [
        'generate_vouchers_action',
    ]

    def generate_vouchers_action(self, request, queryset):
        """Generate an amount of vouchers of
        specified group.
        """
        if 'do_action' in request.POST:
            form = GenerateVouchersForm(request.POST)
            if form.is_valid():
                amount = form.cleaned_data['amount']
                for voucher_group in queryset:
                    for i in range(0, amount):
                        while 1:
                            try:
                                Voucher.objects.create(
                                    number=create_voucher_number(),
                                    group=voucher_group,
                                    creator=request.user,
                                    kind_of=form.cleaned_data["kind_of"],
                                    value=form.cleaned_data["value"],
                                    start_date=form.cleaned_data["start_date"],
                                    end_date=form.cleaned_data["end_date"],
                                    effective_from=
                                    form.cleaned_data["effective_from"],
                                    limit=form.cleaned_data["limit"]
                                )
                            except IntegrityError:
                                pass
                            else:
                                break
                return
        else:
            form = GenerateVouchersForm()
        return render_to_response(
            'admin/voucher_includes/generate-vouchers-action.html', {
                'title': _(u'Generate vouchers'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    generate_vouchers_action.short_description = _(u'Generate vouchers')


class VoucherAdmin(admin.ModelAdmin):
    list_display = ['number', 'group', 'creator', 'creation_date', 'active']
    search_fields = ['number']
    date_hierarchy = 'creation_date'
    list_filter = ['group', 'active']
    ordering = ['-active', 'creation_date']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'creator':
            kwargs['initial'] = request.user.id
        return super(VoucherAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

admin.site.register(Voucher, VoucherAdmin)
admin.site.register(VoucherGroup, VoucherGroupAdmin)
