# coding: utf-8
import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from lfs.voucher.settings import KIND_OF_CHOICES
from lfs.voucher.settings import ABSOLUTE
from lfs.voucher.settings import PERCENTAGE
from lfs.voucher.settings import MESSAGES


class VoucherGroup(models.Model):
    """Groups vouchers together.
    """
    name = models.CharField(_(u'Name'), max_length=100)
    creator = models.ForeignKey(User, verbose_name=_(u'Creator'))
    creation_date = models.DateTimeField(
        _(u'Creation date'), auto_now_add=True)
    position = models.PositiveSmallIntegerField(_(u'Position'), default=10)

    class Meta:
        ordering = ("position", )
        verbose_name = _(u'Voucher group')
        verbose_name_plural = _(u'Voucher groups')

    def __unicode__(self):
        return self.name


class Voucher(models.Model):
    """A voucher.

    Parameters:

        - number
            The unique number of the voucher. This number has to be provided
            by the shop customer within the checkout in order to get the
            credit.

        - group
            The group the voucher belongs to.

        - creator
            The creator of the voucher

        - creation_date
            The date the voucher has been created

        - start_date
            The date the voucher is going be valid. Before that date the
            voucher can't be used.

        - end_date
            The date the voucher is going to expire. After that date the
            voucher can't be used.

        - effective_from
            The cart price the voucher is from that the voucher is valid.

        - kind_of
            The kind of the voucher. Absolute or percentage.

        - value
            The value of the the voucher, which is interpreted either as an
            absolute value in the current currency or a percentage quotation.

        - active
            Only active vouchers can be redeemed.

        - used
            Indicates whether a voucher has already be used. Every voucher can
            only used one time.

        - used_date
            The date the voucher has been redeemed.

        - The quanity of how often the voucher can be used. Let it empty
          the voucher can be used unlimited.
    """
    number = models.CharField(
        _(u'Voucher number'), max_length=100, unique=True)
    group = models.ForeignKey(
        VoucherGroup, related_name="vouchers", verbose_name=_(u'Group'))
    creator = models.ForeignKey(User, verbose_name=_(u'Creator'))
    creation_date = models.DateTimeField(
        _(u'Creation date'), auto_now_add=True)
    start_date = models.DateField(_(u'Start date'), blank=True, null=True)
    effective_from = models.FloatField(
        _(u'Effective from'), default=0.0, help_text=_(u'Minimal price'))
    end_date = models.DateField(
        _(u'End date'), blank=True, null=True)
    kind_of = models.PositiveSmallIntegerField(
        _(u'Discount type'), choices=KIND_OF_CHOICES)
    value = models.FloatField(_(u'Value'), default=0.0)
    active = models.BooleanField(_(u'Active'), default=True)
    used_amount = models.PositiveSmallIntegerField(
        _(u'Used amount'), default=0)
    last_used_date = models.DateTimeField(
        _(u'Last used date'), blank=True, null=True)
    limit = models.PositiveSmallIntegerField(
        _(u'Limit'), blank=True, null=True, default=1)

    class Meta:
        ordering = ("creation_date", "number")
        verbose_name = _(u'Voucher')
        verbose_name_plural = _(u'Vouchers')

    def __unicode__(self):
        return self.number

    def get_price(self, request, cart=None):
        """Returns the gross price of the voucher.
        """
        if self.kind_of == ABSOLUTE:
            return self.value
        else:
            return cart.get_price(request) * (self.value / 100)

    def mark_as_used(self):
        """Mark voucher as used.
        """
        self.used_amount += 1
        self.last_used_date = datetime.datetime.now()
        if self.limit:
            if self.used_amount >= self.limit:
                self.active = False
        self.save()

    def is_effective(self, request, cart):
        """Returns True if the voucher is effective.
        """
        if self.active is False:
            return (False, MESSAGES[1])
        if (self.limit > 0) and (self.used_amount >= self.limit):
            return (False, MESSAGES[2])
        if self.start_date and self.start_date > datetime.date.today():
            return (False, MESSAGES[3])
        if self.end_date and self.end_date < datetime.date.today():
            return (False, MESSAGES[4])
        if self.effective_from > cart.get_price(request):
            return (False, MESSAGES[5])

        return (True, MESSAGES[0])

    def is_absolute(self):
        """Returns True if voucher is absolute.
        """
        return self.kind_of == ABSOLUTE

    def is_percentage(self):
        """Returns True if voucher is percentage.
        """
        return self.kind_of == PERCENTAGE
