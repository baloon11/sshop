# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.http import HttpResponse


def test(request):
    """
    """
    return render_to_response("test.html", RequestContext(request))


def upload_test(request):
    """
    """
    if request.method == "GET":
        return render_to_response("testuploadform.html")

    return HttpResponse()
