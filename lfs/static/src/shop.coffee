# Little monkey patching for arrays

Array::remove = (value) ->
    i = @.indexOf(value)
    if i > -1
        @.splice(i, 1)
    @



# Mediator is a special object for communication between system components

class Mediator
    constructor: ->
        @channels = {}

    subscribe: (channel, fn) ->
        @channels[channel] ?= []
        @channels[channel].push
            context: @
            callback: fn
        @

    publish: (channel, args...) ->
        if not @channels[channel]
            return false
        for subscription in @channels[channel]
            subscription.callback(subscription.context, args)
        @

@mediator = new Mediator

class Config
    constructor: ->
        @left_portlet_count = 0
        @right_portlet_count = 0

@config = new Config

outer_slot_width = 'span2'
inner_slot_width_type_1 = 'span8'
inner_slot_width_type_2 = 'span10'

change_slots_width = (obj, slots_width) ->
    [outer_slot_width, inner_slot_width_type_1, inner_slot_width_type_2] = slots_width.toString().split(',')
    false

mediator.subscribe 'change_slots_width', change_slots_width

class PageDesigner
    constructor: ->
        mediator.subscribe 'document_ready', @page_rebuild

    page_rebuild: ->
        console?.log "page_rebuild #{config.left_portlet_count} - #{config.right_portlet_count}"
        $('#middle-slot-container').attr('style', '')
        if config.left_portlet_count > 0
            $('#left-slot-container').addClass outer_slot_width
        if config.right_portlet_count > 0
            $('#right-slot-container').addClass outer_slot_width
        if config.left_portlet_count > 0 and config.right_portlet_count > 0
            $('#middle-slot-container').addClass inner_slot_width_type_1
        else if config.left_portlet_count > 0 and config.right_portlet_count == 0
            $('#middle-slot-container').addClass inner_slot_width_type_2
        else if config.left_portlet_count == 0 and config.right_portlet_count > 0
            $('#middle-slot-container').addClass inner_slot_width_type_2
            $('#middle-slot-container').css('margin-left','0px')
        else if config.left_portlet_count == 0 and config.right_portlet_count == 0
            $('#middle-slot-container').addClass 'span12'
            $('#middle-slot-container').css('margin-left','0px')

@page_designer = new PageDesigner

class FilterSystem
    constructor: ->
        @vars = @get_url_vars()
        $('[id^="filter-portlet-form-"]').submit (e)->
           e.preventDefault()
           true
        $(".filter_10").click ->
            remove_start_from_get @
            checkbox_change @
        $(".filter_0").click ->
            remove_start_from_get @
            radio_change @
        $(".filter_5").change ->
            remove_start_from_get @
            select_change @
        $(".filter-slider-input").blur ->
            remove_start_from_get @
            slider_change @

        $(".form_submit").click ->
            go()

        remove_start_from_get = (@obj) =>
            if @vars.start?
                delete @vars['start']

        update = =>
            $('[id^="filter-portlet-form-"]').submit (e)->
               e.preventDefault()
               true
            $(".filter_10").unbind 'click'
            $(".filter_0").unbind 'click'
            $(".filter_5").unbind 'click'
            $(".filter-slider-input").unbind 'blur'
            $(".form_submit").unbind 'click'

            $(".filter_10").click ->
                checkbox_change @
            $(".filter_0").click ->
                radio_change @
            $(".filter_5").change ->
                select_change @
            $(".filter-slider-input").blur ->
                slider_change @
            $(".form_submit").click ->
                go()

        mediator.subscribe 'update_filter_system', update

        checkbox_change = (@obj) =>
            changed_checkbox_value(@vars, @obj)
            if $("#use_after_choose")[0].value == "True"
                go()

        radio_change = (@obj) =>
            changed_radio_value(@vars, @obj)
            if $("#use_after_choose")[0].value == "True"
                go()

        select_change = (@obj) =>
            changed_select_value(@vars, @obj)
            if $("#use_after_choose")[0].value == "True"
                go()
            if($("#load_related_options").length)
                if $("#load_related_options")[0].value == "True"
                    load_related_options(@obj)

        slider_change = (@obj) =>
            if @obj.id.indexOf("min") >= 0
                changed_min_slider_value(@vars, @obj)
            else
                changed_max_slider_value(@vars, @obj)

        go = =>
            url = get_hr_urls(@vars)
            for filter in $('[id^="filter-portlet-form-"]')
                $(filter).loadOverStart()
            window.location.href = url
            # param = ""
            # for i of @vars
            #     param += i + "=" + @vars[i] + "&"
            # if param[param.length - 1] == "&"
            #     param = param.slice(0, -1)
            # location.search = param

        mediator.subscribe 'submit_filters', go

        load_related_options = (@obj) =>
            current_select = @obj
            form = $(current_select).parents('form')
            form.loadOverStart()
            $.ajax
                url: "/load-related-options/",
                type: 'POST',
                dataType: 'json',
                data:
                    get_params: $(@obj).parents('form').serialize(),
                    category_id: $('#filter_category_id')[0].value
                success: (data) ->
                    for i of data['html']
                        form.find('select[name='+i+']').parents('.filter-block').html(data['html'][i])
                    form.loadOverStop()
                    mediator.publish 'update_filter_system'



        clean_radio_filter = (obj, filter_ident) =>
            delete @vars[filter_ident]
            go()

        mediator.subscribe 'clean_radio_filter', clean_radio_filter

        get_vars = =>
            param = ""
            for i of @vars
                param += i + "=" + @vars[i] + "&"
            if param[param.length - 1] == "&"
                param = param.slice(0, -1)
            window.param = param

        mediator.subscribe 'get_vars', get_vars

    changed_checkbox_value= (@vars, @obj) =>
        checked = @obj.checked
        if checked
            if @vars[@obj.name]?
                @vars[@obj.name] += "," + @obj.value
            else
                @vars[@obj.name] = @obj.value
        else
            new_values = []
            value = @obj.value
            name = @obj.name
            values = @vars[@obj.name].split(',')
            values.forEach (el) ->
                new_values.push el  if el isnt value and el isnt ""
            @vars[name] = new_values.join()
            if @vars[name] == ""
                delete @vars[name]

    changed_radio_value= (@vars, @obj) =>
        @vars[@obj.name] = @obj.value

    changed_select_value= (@vars, @obj) =>
        value = @obj.value
        if value != "none"
            @vars[@obj.name] = value
        else if @vars[@obj.name]?
            delete @vars[@obj.name]

    changed_min_slider_value= (@vars, @obj) =>
        if @obj.value != @obj.defaultValue and @obj.value != $(@obj).attr("min_value")
            if @vars[@obj.name]?
                values = @vars[@obj.name].split('..')
                @vars[@obj.name] = @obj.value + ".." + values[1]
            else
                @vars[@obj.name] = @obj.value + ".."

        if @vars[@obj.name]? and @obj.value == $(@obj).attr("min_value")
            values = @vars[@obj.name].split('..')
            if values[1] != ""
                @vars[@obj.name] = ".." + values[1]
            else
                delete @vars[@obj.name]

    changed_max_slider_value= (@vars, @obj) =>
        if @obj.value != @obj.defaultValue and @obj.value != $(@obj).attr("max_value")
            if @vars[@obj.name]?
                values = @vars[@obj.name].split('..')
                @vars[@obj.name] = values[0] + ".." + @obj.value
            else
                @vars[@obj.name] = ".." + @obj.value

        if @vars[@obj.name]? and @obj.value == $(@obj).attr("max_value")
            values = @vars[@obj.name].split('..')
            if values[0] != ""
                @vars[@obj.name] = values[0] + ".."
            else
                delete @vars[@obj.name]

    get_url_vars: ->
        vars = {}
        try
            get_params = $('#unpacked_hr_urls')[0].value

            if get_params != ""
                hashes = get_params.split('&')
                for i in [0...hashes.length]
                    hash = hashes[i].split('=')
                    vars[hash[0]] = hash[1]
        return vars

    get_hr_urls= (@vars) =>
        templates = $('#hr_urls_templates')[0].value.split(';')
        # find the best template
        template = ''
        template_vars = []
        best_count = 0
        for temp in templates
            temp_vars = []
            # get keys from template
            for v in temp.split('/')
                if v.indexOf('$') == 0
                    temp_vars.push(v.replace('$', ''))

            count = 0
            for v in temp_vars
                if v of @vars and @vars[v].indexOf(',') == -1
                    count += 1

            if count > best_count and count == temp_vars.length
                best_count = count
                template = temp
                template_vars = temp_vars

        get_params = {}
        url_params = {}
        if template != ""
            for v of @vars
                if v in template_vars
                    url_params[v] = @vars[v]
                else
                    get_params[v] = @vars[v]
        else
            get_params = @vars

        port = ''
        if location.port
            port = ':' + location.port
        url = window.location.protocol + '//' + window.location.hostname + port + '/' + $('#category_slug')[0].value
        if template != ""
            for v of url_params
                template = template.replace('$' + v, url_params[v])
            url += template

        param = ''
        if get_params.start?
            delete get_params['start']
        for i of get_params
            param += i + "=" + get_params[i] + "&"
        if param[param.length - 1] == "&"
            param = param.slice(0, -1)

        if param != ''
            url += '?' + param
        return url


new_filter_system = ->
    @filter_system = new FilterSystem

mediator.subscribe 'document_ready', new_filter_system


modal = ->
    $(".modal-form").submit (e)->
       e.preventDefault()
       true
    $(".modal-form .btn").click ->
        data = $(".modal-form").ajaxSubmit
            url : $(".modal-form").attr("data"),
            "success" : (data) ->
                $('#sshop-modal .modal-body').html data
                $('#sshop-modal .modal-header h3').html($('div.header h3').html())
                $('div.header').remove()
                mediator.publish 'modal_show'
                false
   false

mediator.subscribe 'modal_show', modal

change_amount_modal_cart_item = ->
    dict = {}
    for input in $('.modal-cart-amount')
        dict[input.name] = input.value
    $.post $("#change_modal_cart").attr('action'), dict, (data) ->
        $('#sshop-modal .modal-body').html data
        $('#cart-container').load '/cart-block'
        true
    false

mediator.subscribe 'change_amount_modal_cart_item', change_amount_modal_cart_item

remove_modal_cart_item = ->
    mediator.publish 'change_amount_modal_cart_item'
    false

mediator.subscribe 'remove_modal_cart_item', remove_modal_cart_item

add_to_cart = ->
    $('.add-to-cart-form').unbind 'submit'
    $('.add-to-cart-form').submit ->
        $.post $(@).attr('action'), $(@).serialize(), (data) ->
            mediator.publish 'show_modal_cart', data
        false

mediator.subscribe 'add_to_cart', add_to_cart


show_modal_cart = (obj, data) ->
    $('#sshop-modal .modal-header h3').html gettext('Cart')
    $('#sshop-modal .modal-footer').removeClass('hide')
    $('#sshop-modal .modal-body').html data
    $('#sshop-modal').css('width', '940px').css('left', '40%')
    $('.modal-footer')[0].innerHTML = '<a data-dismiss="modal" class="pointer" style="margin-right:10px" aria-hidden="true">' + gettext('Continue shopping') + '</a><button class="btn btn-success checkout" aria-hidden="true">  <i class="icon-ok icon-white"></i> ' + gettext('Checkout') + '</button>'
    if $('#empty-modal-cart').length
        $('#sshop-modal .checkout').hide();
    $(".checkout").click ->
        window.location.href = '/checkout'
    $('#sshop-modal').modal('show')
    $('#cart-container').load '/cart-block'

mediator.subscribe 'show_modal_cart', show_modal_cart


load_all_options = ->
    $('.load_all_options').each (i, el) ->
        $(el).unbind 'click'
        $(el).click ->
            checked_options = $($(el)[0].parentElement).find(".chk-area.chk-checked")
            checked_values = []
            for checked_option in checked_options
                checked_value = checked_option.parentElement.getElementsByTagName('input')[0]
                checked_values.push checked_value
            mediator.publish 'get_vars'
            $.ajax
                url: "/get-filter-options/" + el.id + '?' + window.param,
                success: (data) ->
                    $(el).parents('.filter-block')[0].innerHTML = data
                    for checked_value in checked_values
                        value = $(checked_value).attr('value')
                        filter_option = $(".filter_option > input[value='" + value + "']")
                        $(filter_option[0]).prop('checked', true) 
                    show_only_popular_filter_options()
                    mediator.publish 'update_filter_system'
        true

mediator.subscribe 'load_all_options', load_all_options

show_only_popular_filter_options = ->
    objs = $('.show_only_popular')
    objs.unbind 'click'
    objs.each (i, el) ->
        $(el).click ->
            for filter_option in $(el.parentElement).find('.filter_option')
                if filter_option.getAttribute('popular') == 'False'
                    $(filter_option).hide()
            $(el).attr('class', 'show_all_options')
            $(el).text gettext('Show all')
            show_all_filter_options()

show_all_filter_options = ->
    objs = $('.show_all_options')
    objs.unbind 'click'
    objs.each (i, el) ->
        $(el).click ->
            for filter_option in $(el.parentElement).find('.filter_option')
                if filter_option.getAttribute('popular') == 'False'
                    $(filter_option).show()
            $(el).attr('class', 'show_only_popular')
            $(el).text gettext('Only popular')
            show_only_popular_filter_options()

show_filter_children = ->
    id = @.id
    obj = $('#' + id)
    parent = @.parentElement
    if obj.hasClass('icon-plus')
        obj.toggleClass('icon-plus').toggleClass('icon-minus')
        if $('#children-' + id)[0].innerHTML == ""
            $(parent).loadOverStart();
            mediator.publish 'get_vars'
            $.ajax
                url: "/get-filter-children/" + @.id + '?' + window.param
                success: (data) ->
                    $('#children-' + id)[0].innerHTML = data
                    $('#children-' + id).show();
                    $(parent).loadOverStop();
                    mediator.publish 'update_filter_system'
                    mediator.publish 'load_all_options'
        else
            $('#children-' + id).show();
    else
        obj.toggleClass('icon-minus').toggleClass('icon-plus')
        $('#children-' + id).hide();
    false

filter_show_children = ->
    $('.filter_show_children').live('click', show_filter_children)
    

mediator.subscribe 'filter_show_children', filter_show_children

# Working with custom forms

update_custom_form = ->
    @custom_form = $(@).parents('.custom-form:first')
    ajax_url = $(@custom_form).attr('data-update-url')
    params = $(@custom_form).find('input,select,textarea').serialize()
    $(@custom_form).loadOverStart()
    data = $.ajax
        url : ajax_url,
        type: 'POST',
        data: params,
        "success" : (data) =>
            $(@custom_form).html data
            $(@custom_form).loadOverStop()
            false

action__confirm_when_click = ->
    message = $(@).attr('data-message')
    confirm message

action__checkout__select_phone = ->
    value = $(@).val()
    if value == "!NEW"
        $(".checkout-form__phone--custom").show()
    else
        $(".checkout-form__phone--custom").hide()
        $(".checkout-form__phone--custom").val("")

action__checkout__select_email = ->
    value = $(@).val()
    if value == "!NEW"
        $(".checkout-form__email--custom").show()
    else
        $(".checkout-form__email--custom").hide()
        $(".checkout-form__email--custom").val("")


run_scripts_from = (obj, str) ->
    scripts = str.toString().match(/<script.*?>([\s\S]*?)<\/script>/g);
    for script in scripts
        script = script.replace('<script type="text/javascript">', '')
        script = script.replace('<\/script>', '')
        eval script

mediator.subscribe 'run_scripts_from', run_scripts_from

modal_link_show = ->
    $('#sshop-modal .modal-footer').addClass('hide')
    $('#sshop-modal .modal-body').load $(@).attr('href'), null, ->
        $('#sshop-modal .modal-header h3').html($('div.header h3').html())
        $('div.header').remove()
        mediator.publish 'modal_show'
        false
    $('#sshop-modal').modal 'show'
    false

$ ->
    $('#top-bar-container').load '/top-bar'
    $('#cart-container').load '/cart-block'

    $('#product-tabs').tabs
        ajaxOptions:
            error: (xhr, status, index, anchor) ->
                $(anchor.hash).html "Ошибка загрузки данных."
        spinner: '<img src="/static/img/horizontal-ajax-loader.gif"> Загрузка...'   #TODO: add localization

    mediator.publish 'add_to_cart'

    $("#sumbit_order").click ->
        $("#sumbit_order").unbind 'click'
        $('.help-block.error, .alert.alert-error').remove()
        $('.error').removeClass 'error'

        is_valid = true
        $('.required_input').each (i,el) ->
            if (el.value != "undefined" and el.value.length == 0) or el.value == "undefined"
                is_valid = false
                $(el.parentElement.parentElement).addClass 'error'
                $(el.parentElement).append '<p class="help-block error">Поле обязательно к заполнению.</p>'
            true

        if is_valid
            $('.checkout-form').submit()
            mediator.publish 'submit_order'
        else
            $('.checkout-form').parents('div:eq(1)').prepend '<div class="alert alert-error">Операция не может быть выполнена, так как есть одна или несколько ошибок. Пожалуйста, повторите отправку формы после исправления следующих полей:</div>'
            $('html').animate {scrollTop:0}, 'slow' # IE, FF
            $('body').animate {scrollTop:0}, 'slow'
        false

    $(".modal-link").live('click', modal_link_show)
        

    # for filters show all filter options
    mediator.publish 'load_all_options'

    # filters with children
    mediator.publish 'filter_show_children'

    $('.label-popover').popover()

    $("#seo-text-wrapper").appendTo("#middle-slot-container").css
        position: 'inherit'
        width: '100%'
        'margin-left': '0'
        left: '0'

    $(".custom-form__widget--action").live('change', update_custom_form)

    # Global actions

    $(".__action__confirm-when-click").live('click', action__confirm_when_click)

    # Hide some elements after 10 seconds
    setTimeout ->
        $(".__action__hide-after-10-seconds").fadeOut('fast')
    , 10000

    mediator.publish 'document_ready'

    if $('#ajax_load_children').length > 0
        for id in $('#ajax_load_children')[0].value.split(',')
            $('#' + id).click()

    $(".checkout-form__phone--custom").hide()
    $(".__action__checkout__select-phone").live(
        'change', action__checkout__select_phone)

    $(".checkout-form__email--custom").hide()
    $(".__action__checkout__select-email").live(
        'change', action__checkout__select_email)

    $(".__action__show-on-ready").show()
    $(".__action__hide-on-ready").hide()
