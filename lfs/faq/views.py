# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .forms import FaqAddForm
from .models import Question


def add_faq(request, template_name="lfs/faq/add_faq.html"):
    if request.method == "POST":
        form = FaqAddForm(request.POST)
        if form.is_valid():
            question = form.save()
            question.created_by = request.user
            question.save()
            return HttpResponseRedirect(reverse("faq_thank_you"))
    else:
        form = FaqAddForm()
    return render_to_response(template_name, RequestContext(request, {
        "form": form,
        'service_page': True,
    }))


def faq_view(request):
    questions = Question.objects.filter(
        published=True).order_by('-creation_date')
    return render_to_response(
        'lfs/faq/faq.html',
        RequestContext(request, {
            'questions': questions,
            'service_page': True,
        }))


def thank_you(request, template_name="lfs/faq/thank_you.html"):
    """Displays a thank you page.
    """
    return render_to_response(template_name, RequestContext(request, {'service_page': True,}))
