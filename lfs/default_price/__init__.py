# coding: utf-8
from ..plugins import PriceCalculator, ShippingMethodPriceCalculator


class DefaultPriceCalculator(PriceCalculator):
    """
    Default price calculator for SSHOP

    See lfs.plugins.PriceCalculator for more information.
    """
    pass


class DefaultShippingPriceCalculator(ShippingMethodPriceCalculator):
    """
    Default shipping price calculator for SSHOP

    See lfs.plugins.PriceCalculator for more information.
    """
    pass
