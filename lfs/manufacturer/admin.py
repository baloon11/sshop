# coding: utf-8
from django.contrib import admin
from lfs.catalog.models import Manufacturer


class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_popular')
    list_filter = ('is_popular',)
    search_fields = ['name']

admin.site.register(Manufacturer, ManufacturerAdmin)
