# coding: utf-8
from django import forms
from lfs.catalog.models import (
    Category,
    Property,
)
from .models import (
    Filter,
    FilterOption,
)
from django_select2.widgets import Select2MultipleWidget


class AddFilterForm(forms.ModelForm):
    '''
    Form for add the new filter.
    '''
    class Meta:
        model = Filter
        widgets = {
            'properties': Select2MultipleWidget(
                attrs={'class': 'input-large'}),
        }
        fields = (
            'category',
            'parent',
            'special_filter',
            'properties',
            'displayed_title',
            'type',
            'is_addition',
            'template',
            'identificator',
            'use_after_choose',
            'auto_update_add',
            'take_popularity',
            'show_as_tree',
        )

    def __init__(self, *args, **kwargs):
        if 'initial' not in kwargs:
            kwargs['initial'] = {}
        self.request = kwargs.pop('request', None)
        if 'category_id' in self.request.GET:
            category = Category.objects.get(pk=self.request.GET['category_id'])
            kwargs['initial']['category'] = category

        super(AddFilterForm, self).__init__(*args, **kwargs)
        if 'category_id' in self.request.GET:
            self.fields['properties'].queryset = \
                Property.objects.filter(
                    property_values__product__categories=category)\
                .exclude(is_group=True).distinct().order_by('name')
            self.fields['parent'].queryset = Filter.objects.filter(
                category=category, parent=None)\
                .distinct().order_by('displayed_title')


class EditFilterForm(forms.ModelForm):
    class Meta:
        model = Filter
        widgets = {
            'properties': Select2MultipleWidget(
                attrs={'class': 'input-large'}),
        }

    def __init__(self, *args, **kwargs):
        super(EditFilterForm, self).__init__(*args, **kwargs)
        self.fields['parent'].queryset = Filter.objects.filter(
            category=self.instance.category,
            parent=None).distinct().order_by('displayed_title')


class EditFilterOptionForm(forms.ModelForm):
    class Meta:
        model = FilterOption
        exclude = ['products']

    def __init__(self, *args, **kwargs):
        if 'initial' not in kwargs:
            kwargs['initial'] = {}
        self.request = kwargs.pop('request', None)
        if 'filter_id' in self.request.GET:
            f = Filter.objects.get(pk=self.request.GET['filter_id'])
            kwargs['initial']['filter'] = f

        super(EditFilterOptionForm, self).__init__(*args, **kwargs)
