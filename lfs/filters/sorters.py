# coding: utf-8
import re
from django.utils.translation import ugettext as _


def atof(text):
    try:
        return float(text)
    except ValueError:
        return text


def natural_keys(obj):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [
        atof(c) for c in re.split('([\d.]+)', obj.title.replace(',', '.'))]


class BaseFilterOptionSorter(object):
    """Basic class for implement the sorting method for filter options.
    """
    name = ''

    def sort(self, queryset):
        raise NotImplementedError


class AscFilterOptionSorter(BaseFilterOptionSorter):
    """Sorting from A to Z
    """
    name = _(u'ASC')

    def sort(self, queryset):
        i = 0
        options = queryset.order_by('title')
        for o in options.iterator():
            o.position = i
            o.save()
            i += 1


class DescFilterOptionSorter(BaseFilterOptionSorter):
    """Sorting from Z to A
    """
    name = _(u'DESC')

    def sort(self, queryset):
        i = 0
        options = queryset.order_by('-title')
        for o in options.iterator():
            o.position = i
            o.save()
            i += 1


class CountAZFilterOptionSorter(BaseFilterOptionSorter):
    """Sorting from bigger to smaller
    """
    name = _(u'From more to less')

    def sort(self, queryset):
        for o in queryset.iterator():
            o.position = o.products.count()
            o.save()


class CountZAFilterOptionSorter(BaseFilterOptionSorter):
    """Sorting from smaller to bigger
    """
    name = _(u'From less to more')

    def sort(self, queryset):
        for o in queryset.iterator():
            o.position = -o.products.count()
            o.save()


class NumberAZFilterOptionSorter(BaseFilterOptionSorter):
    """Sorting from 0 to 999
    """
    name = _(u'Number ASC')

    def sort(self, queryset):
        ordered = sorted(queryset, key=natural_keys)
        i = 0
        for o in ordered:
            o.position = i
            o.save()
            i += 1


class NumberZAFilterOptionSorter(BaseFilterOptionSorter):
    """Sorting from 999 to 0
    """
    name = _(u'Number DESC')

    def sort(self, queryset):
        ordered = sorted(queryset, key=natural_keys, reverse=True)
        i = 0
        for o in ordered:
            o.position = i
            o.save()
            i += 1
