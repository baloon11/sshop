# coding: utf-8
from django.utils.translation import ugettext as _
from django.contrib import admin
from django.http import HttpResponse

from ..catalog.admin import CategoryFilter
from .forms import (
    AddFilterForm,
    EditFilterForm,
    EditFilterOptionForm,
)
from .models import (
    Filter,
    FilterOption,
)


class FilterAdmin(admin.ModelAdmin):
    list_display = (
        'displayed_title', 'category', 'type',
        'special_filter', 'show_as_tree')
    search_fields = ['displayed_title']
    ordering = ['category', 'position']
    list_filter = (CategoryFilter, 'type', 'special_filter')

    add_form = AddFilterForm
    edit_form = EditFilterForm
    prepopulated_fields = {"identificator": ("displayed_title",)}

    base_readonly_fields = tuple()
    add_readonly_fields = tuple()
    edit_readonly_fields = ('category', 'position', 'special_filter')

    add_fielsets = [
        (None, {
            'fields': [
            'category',
            'parent',
            'special_filter',
            'properties',
            'displayed_title',
            'type',
            'is_addition',
            'template',
            'identificator',
            'use_after_choose',
            'auto_update_add',
            'show_as_tree',
            ]
        }),
    ]

    base_fieldsets = [
        (None, {
            'fields': [
                'category',
                'parent',
                'displayed_title',
                'type',
                'special_filter',
                'properties',
                'position',
            ]
        }),
        (_(u'View'), {
            'fields': [
                'identificator',
                'template',
                'use_after_choose',
                'take_popularity',
                'show_as_tree',
            ]
        }),
        (_(u'Automate'), {
            'fields': [
                'auto_update_add',
            ]
        }),
    ]

    choice_fieldsets = [
        (_(u'Choice'), {
            'fields': [
                'is_addition',
            ]
        }),
    ]
    numeric_fieldsets = [
        (_(u'Numeric'), {
            'fields': [
                'min_value',
                'max_value',
                'avg_value',
                'unit',
            ]
        }),
    ]

    def response_add(self, request, obj, post_url_continue=None):
        if "_popup" in request.POST:
            return HttpResponse(
                '<!DOCTYPE html><html><head><title></title></head><body>'
                '<script type="text/javascript">'
                'opener.on_filter_popup_close(window);'
                '</script></body></html>')

    def response_change(self, request, obj):
        if "_popup" in request.POST:
            return HttpResponse(
                '<!DOCTYPE html><html><head><title></title></head><body>'
                '<script type="text/javascript">'
                'opener.on_filter_popup_close(window);'
                '</script></body></html>')

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during category creation
        """
        defaults = {}
        if obj is None:
            class AddFilterFormProxy(AddFilterForm):
                def __new__(cls, *args, **kwargs):
                    kwargs['request'] = request
                    return AddFilterForm(*args, **kwargs)
            defaults.update({
                'form': AddFilterFormProxy,
            })
            self.readonly_fields = \
                self.base_readonly_fields + self.add_readonly_fields
            self.fieldsets = self.add_fielsets
        else:
            class EditFilterFormProxy(EditFilterForm):
                def save(self, commit=True):
                    instance = super(EditFilterForm, self).save(commit=False)
                    if commit:
                        instance.save()
                    return instance
            defaults.update({
                'form': EditFilterFormProxy,
            })
            self.readonly_fields = \
                self.base_readonly_fields + self.edit_readonly_fields
            if obj.type != 20:
                self.fieldsets = self.base_fieldsets + self.choice_fieldsets
            else:
                self.fieldsets = self.base_fieldsets + self.numeric_fieldsets
        defaults.update(kwargs)
        return super(FilterAdmin, self).get_form(request, obj, **defaults)


class FilterOptionAdmin(admin.ModelAdmin):
    search_fields = ['displayed_title']
    list_display = (
        'title', 'filter', 'is_popular', 'operator_type', 'ignore_case')
    list_filter = ('operator_type', 'ignore_case',)
    fields = (
        'filter',
        'title',
        'identificator',
        'regex',
        'operator_type',
        'ignore_case',
        'is_popular',
        'position',
    )
    exclude = ['products']

    def response_add(self, request, obj, post_url_continue=None):
        if "_popup" in request.POST:
            return HttpResponse(
                '<!DOCTYPE html><html><head><title></title></head><body>'
                '<script type="text/javascript">'
                'function GetURLParameter(sParam)'
                '{'
                '    var sPageURL = window.location.search.substring(1);'
                '    var sURLVariables = sPageURL.split("&");'
                '    for (var i = 0; i < sURLVariables.length; i++) '
                '    {'
                '        var sParameterName = sURLVariables[i].split("=");'
                '        if (sParameterName[0] == sParam) '
                '        {'
                '            return sParameterName[1];'
                '        }'
                '    }'
                '}'
                'opener.on_filter_option_popup_close(window);</script></body></html>')

    def response_change(self, request, obj):
        if "_popup" in request.POST:
            return HttpResponse(
                '<!DOCTYPE html><html><head><title></title></head><body>'
                '<script type="text/javascript">'
                'function GetURLParameter(sParam)'
                '{'
                '    var sPageURL = window.location.search.substring(1);'
                '    var sURLVariables = sPageURL.split("&");'
                '    for (var i = 0; i < sURLVariables.length; i++) '
                '    {'
                '        var sParameterName = sURLVariables[i].split("=");'
                '        if (sParameterName[0] == sParam) '
                '        {'
                '            return sParameterName[1];'
                '        }'
                '    }'
                '}'
                'opener.on_filter_option_popup_close(window, GetURLParameter("filter_id"), GetURLParameter("start"));'
                '</script></body></html>')

    def get_form(self, request, obj=None, **kwargs):
        defaults = {}

        class EditFilterOptionFormProxy(EditFilterOptionForm):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return EditFilterOptionForm(*args, **kwargs)
        defaults.update({
            'form': EditFilterOptionFormProxy,
        })
        defaults.update(kwargs)
        return super(FilterOptionAdmin, self)\
            .get_form(request, obj, **defaults)

    def save_model(self, request, obj, form, change):
        obj.save()
        obj.parse()


admin.site.register(Filter, FilterAdmin)
admin.site.register(FilterOption, FilterOptionAdmin)
