# coding:utf-8
import logging
import pytils

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db.models import Max, Min, Avg

from mptt.models import TreeForeignKey
from ..manufacturer.models import Manufacturer
from ..catalog.settings import STANDARD_PRODUCT, VARIANT
from ..catalog.models import (
    Category,
    Product,
    Property,
    ProductPropertyValue,
    PropertyType,
)

logger = logging.getLogger('sshop')

FILTER_TYPE_CHOICES = (
    (0, _(u'Only one can be selected (radio buttons)')),
    (5, _(u'Only one can be selected (choice)')),
    (10, _(u'Many items can be selected')),
    (20, _(u'Integer value slider')),
)

SPECIAL_FILTER_CHOICES = (
    (0, _(u'Property')),
    (10, _(u'Price')),
    (20, _(u'Manufacturer')),
    (30, _(u'Status')),
)

FILTER_OPTION_OPERATOR_CHOICES = (
    (0, _(u'Strong equal')),
    (5, _(u'Strong equal with variants')),
    (10, _(u'Regular expression')),
    (15, _(u'For multiple properties')),
)


class Filter(models.Model):
    category = TreeForeignKey(Category, verbose_name=_(u'Category'))
    special_filter = models.PositiveSmallIntegerField(
        _(u'Implementation type'),
        choices=SPECIAL_FILTER_CHOICES,
        default=0)
    properties = models.ManyToManyField(
        Property, verbose_name=_('Property'),
        help_text=_(u'For property filters only'), null=True, blank=True)
    displayed_title = models.CharField(
        _(u'Displayed title'), max_length=255, default='', blank=True)
    position = models.SmallIntegerField(default=999)
    type = models.PositiveSmallIntegerField(
        _(u"Visible type"), choices=FILTER_TYPE_CHOICES, default=0)
    is_addition = models.BooleanField(_(u"Is addition"), default=True)
    take_popularity = models.BooleanField(
        _(u'Take into consideration popularity'), default=False)

    # For internal use only
    min_value = models.FloatField(_(u'Min integer value'), default=0)
    max_value = models.FloatField(_(u'Max integer value'), default=0)
    avg_value = models.FloatField(_(u'Avg integer value'), default=0)
    property_values = models.ManyToManyField(
        ProductPropertyValue, null=True, blank=True)
    unit = models.CharField(
        _(u"Unit"), blank=True, max_length=25)  # DEPRECATED
    identificator = models.CharField(
        _(u'Identificator'), default='', max_length=50, blank=True)
    template = models.CharField(
        _(u'Template'), default='%s', max_length=50, blank=True)
    use_after_choose = models.BooleanField(
        _(u"Use filter right after choose"), default=True)  # TODO: remove it
    auto_update_add = models.BooleanField(
        _(u"Update this filter options automatically"), default=True)
    sorting = models.CharField(
        _(u'Sorting'),
        blank=True, null=True,
        max_length=300)

    parent = models.ForeignKey(
        'Filter',
        verbose_name=_(u'Parent filter'),
        blank=True, null=True, related_name='children')
    show_as_tree = models.BooleanField(_(u'Show as tree'), default=False)

    class Meta:
        verbose_name = _(u'Filter')
        verbose_name_plural = _(u'Filters')
        ordering = ('position',)
        unique_together = (("category", "identificator"),)

    def __unicode__(self):
        return self.displayed_title + ' - ' + self.category.name

    def fill_identificator(self, save=True):
        if self.special_filter == 10:
            self.identificator = 'price'
        elif self.special_filter == 20:
            self.identificator = 'manufacturer'
        elif self.special_filter == 30:
            self.identificator = 'status'
        else:
            self.identificator = pytils.translit.slugify(
                self.displayed_title)[:49].lower()

        if save:
            self.save()

    def get_iden(self):
        a = self.identificator
        return a

    def save(self, *args, **kwargs):
        iden = self.identificator
        count = 1
        if iden.strip() == '':
            self.fill_identificator(save=False)
        while Filter.objects.filter(
                identificator=iden,
                category=self.category).exclude(pk=self.pk).exists():
            iden = '%s_%s' % (self.identificator, count)
            count += 1
        self.identificator = iden
        super(Filter, self).save(*args, **kwargs)

    def get_title(self):
        if self.displayed_title:
            return self.displayed_title
        else:
            return _(u'Unknown')

    def get_type_str(self):
        return dict(FILTER_TYPE_CHOICES)[self.type]

    def get_special_filter_str(self):
        return dict(SPECIAL_FILTER_CHOICES)[self.special_filter]

    def has_options(self):
        return self.filteroption_set.count() > 0

    def get_template_name(self):
        if self.special_filter == 0:    # Property
            return 'lfs/filters/representations/property_%d.html' % self.type
        elif self.special_filter == 10:    # Price
            return 'lfs/filters/representations/price_%d.html' % self.type
        elif self.special_filter == 20:    # Manufacturer
            return 'lfs/filters/representations/manufacturer_%d.html' % \
                self.type
        elif self.special_filter == 30:    # Status
            return 'lfs/filters/representations/status_%d.html' % self.type
        else:
            return ''

    def autofill(self):
        if self.special_filter == 10:   # Price
            result = self.category.products.filter(
                active=True,
                status__is_visible=True,
                sub_type__in=[STANDARD_PRODUCT, VARIANT],
            ).aggregate(
                Max('effective_price'),
                Min('effective_price'),
                Avg('effective_price'),
            )
            self.min_value, self.max_value, self.avg_value = \
                result['effective_price__min'], \
                result['effective_price__max'], \
                result['effective_price__avg']
            if self.min_value is None:
                self.min_value = 0.0
            if self.max_value is None:
                self.max_value = 0.0
            if self.avg_value is None:
                self.avg_value = 0.0
            self.save()
        elif self.special_filter == 0:
            if self.properties.count() == 1:
                p = self.properties.all()[0]
                t_values = ProductPropertyValue.objects.filter(
                    property=p,
                    product__categories=self.category,
                    product__active=True,
                    product__sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    product__status__is_visible=True,
                )
                result = t_values.aggregate(
                    Max('value_as_float'),
                    Min('value_as_float'),
                    Avg('value_as_float'),
                )
                self.min_value, self.max_value, self.avg_value = \
                    result['value_as_float__min'], \
                    result['value_as_float__max'], \
                    result['value_as_float__avg']
                if self.min_value is None:
                    self.min_value = 0.0
                if self.max_value is None:
                    self.max_value = 0.0
                if self.avg_value is None:
                    self.avg_value = 0.0
                self.property_values = t_values
                self.save()

    def get_unit(self):
        if self.special_filter == 0:
            if self.properties.count() == 1:
                p = self.properties.all()[0]
                try:
                    p_type = PropertyType.objects.get(
                        categories=self.category, property=p)
                except PropertyType.DoesNotExist:
                    return False
                except PropertyType.MultipleObjectsReturned:
                    logger.error(
                        u'Property %s (pk=%d) for category %s (pk=%d) has '
                        u'more than one specified types.' % (
                            p.name,
                            p.id,
                            self.category,
                            self.category_id,
                        ))
                    return ''
                if p_type.unit is not None:
                    return p_type.unit.name
                else:
                    return ''
        return ''

    def is_number_property(self):  # TODO: review this later
        p_count = self.properties.count()
        if p_count == 1:
            p = self.properties.all()[0]
            try:
                p_type = PropertyType.objects.get(
                    categories=self.category, property=p)
            except PropertyType.DoesNotExist:
                return False
            except PropertyType.MultipleObjectsReturned:
                logger.error(
                    u'Property %s (pk=%d) for category %s (pk=%d) has more '
                    u'than one specified types.' % (
                        p.name,
                        p.id,
                        self.category,
                        self.category_id,
                    ))
                return False
            return p_type.is_number()
        else:
            pass
        return False

    def parse(self):
        options = self.filteroption_set.all()
        for option in options:
            option.parse()

    def get_product_count(self):
        products = Product.objects.filter(filteroption__filter=self).distinct()
        return products.count()

    def get_products_from_category(self):
        return self.category.products.filter(
            active=True,
            status__is_visible=True)

    def get_all_products_count(self):
        return self.get_products_from_category().count()

    def get_coverage_percent(self):
        in_category_count = self.get_products_from_category().count()
        in_filter_count = self.get_product_count()
        if in_category_count != 0:
            return float(in_filter_count) / in_category_count * 100
        else:
            return 0


class FilterOption(models.Model):
    filter = models.ForeignKey(Filter, verbose_name=_(u'Filter'))
    title = models.CharField(_(u'Title'), max_length=255)
    products = models.ManyToManyField(
        Product, verbose_name=_(u'Products'), blank=True, null=True)
    position = models.IntegerField(default=999)
    identificator = models.CharField(
        _(u'Identificator'), default='',
        max_length=50, blank=True,
        help_text=_(
            u'If you leave this field blank P-Cart will generate '
            u'correct value automatically.'))
    is_popular = models.BooleanField(_(u'Popular'), default=False)

    # Parser
    regex = models.TextField(_(u'Regular expression'))
    operator_type = models.PositiveSmallIntegerField(
        _(u'Operator type'), choices=FILTER_OPTION_OPERATOR_CHOICES,
        default=0)
    ignore_case = models.BooleanField(_(u"Ignore case"), default=False)

    class Meta:
        verbose_name = _(u'Filter option')
        verbose_name_plural = _(u'Filter options')
        ordering = ('position',)
        unique_together = (("filter", "identificator"),)

    def __unicode__(self):
        return self.filter.get_title() + u' : ' + self.title

    def fill_identificator(self, save=True):
        if getattr(settings, 'FILTER_REPLACE_DOT_COMMA', False):
            # self.title = self.title.replace('.', '-').replace(',', '-')
            identificator = self.filter.template % pytils.translit.slugify(
                self.title.replace('.', '-').replace(',', '-'))
        else:
            identificator = self.filter.template % pytils.translit.slugify(
                self.title
            )
        self.identificator = identificator[:40].lower()
        count = 1
        while FilterOption.objects.filter(
                identificator=self.identificator,
                filter=self.filter).exclude(pk=self.pk).exists():
            self.identificator = '%s_%s' % (self.identificator, count)
            count += 1
        if save:
            self.save()

    def save(self, *args, **kwargs):
        iden = self.identificator
        if iden.strip() == '':
            self.fill_identificator(save=False)
        if not self.pk and self.filter.special_filter == 20:
            #TODO: review this later
            try:
                self.is_popular = Manufacturer.objects.get(
                    name=self.title).is_popular
            except:
                pass
        super(FilterOption, self).save(*args, **kwargs)

    def get_operator_type_str(self):
        return dict(FILTER_OPTION_OPERATOR_CHOICES)[self.operator_type]

    def parse(self):
        if self.filter.special_filter == 0:   # Property
            if self.operator_type == 0:    # Strong equal
                if self.ignore_case:
                    p_values = ProductPropertyValue.objects.filter(
                        property__in=self.filter.properties.all(),
                        value__iexact=self.regex.strip(),
                    )
                else:
                    p_values = ProductPropertyValue.objects.filter(
                        property__in=self.filter.properties.all(),
                        value=self.regex.strip(),
                    )
                products = Product.objects.filter(
                    categories=self.filter.category,
                    property_values__in=p_values,
                    sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    active=True,
                    status__is_visible=True,
                )
                self.products = products
            elif self.operator_type == 5:  # Strong equal with variants (OR)
            #TODO: add for number values
                if self.filter.is_number_property():
                    values = filter(
                        bool,
                        map(lambda x: x.strip(), self.regex.split('\n')))
                    p_values = ProductPropertyValue.objects.filter(
                        property__in=self.filter.properties.all())
                    for v in values:
                        t = v.split()
                        if t[0] == '<':
                            p_values = p_values.filter(
                                value_as_float__lt=float(t[1]))
                        elif t[0] == '<=':
                            p_values = p_values.filter(
                                value_as_float__lte=float(t[1]))
                        elif t[0] == '>':
                            p_values = p_values.filter(
                                value_as_float__gt=float(t[1]))
                        elif t[0] == '>=':
                            p_values = p_values.filter(
                                value_as_float__gte=float(t[1]))
                        elif t[0] == '==':
                            p_values = p_values.filter(
                                value_as_float=float(t[1]))
                        elif t[0] == '!=':
                            p_values = p_values.exclude(
                                value_as_float=float(t[1]))

                    products = Product.objects.filter(
                        categories=self.filter.category,
                        property_values__in=p_values,
                        sub_type__in=[STANDARD_PRODUCT, VARIANT],
                        active=True,
                        status__is_visible=True,
                    )
                    self.products = products
                else:
                    values = filter(
                        bool,
                        map(lambda x: x.strip(), self.regex.split('\n')))
                    p_values = ProductPropertyValue.objects.filter(
                        property__in=self.filter.properties.all(),
                        value__in=values,
                    )
                    products = Product.objects.filter(
                        categories=self.filter.category,
                        property_values__in=p_values,
                        active=True,
                        status__is_visible=True,
                    )
                    self.products = products
            elif self.operator_type == 10:  # Regular expressions
                p_values = ProductPropertyValue.objects.filter(
                    property__in=self.filter.properties.all(),
                    value__iregex=self.regex,
                )
                products = Product.objects.filter(
                    categories=self.filter.category,
                    property_values__in=p_values,
                    sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    active=True,
                    status__is_visible=True,
                )
                self.products = products
            elif self.operator_type == 15:  # For different properties (OR)
                expression = map(lambda x: x.strip(), self.regex.split('\n'))
                products = Product.objects.none()
                for s in expression:
                    t = s.split('~~*~~')
                    p_name = t[0]
                    value = t[1]

                    p_property = self.filter.properties.get(name=p_name)
                    if self.ignore_case:
                        p_values = ProductPropertyValue.objects.filter(
                            property=p_property,
                            value__iexact=value,
                        )
                    else:
                        p_values = ProductPropertyValue.objects.filter(
                            property=p_property,
                            value=value,
                        )
                    products = products | Product.objects.filter(
                        categories=self.filter.category,
                        property_values__in=p_values,
                        sub_type__in=[STANDARD_PRODUCT, VARIANT],
                        active=True,
                        status__is_visible=True,
                    )  # TODO: add other types
                products = products.distinct()
                self.products = products
        elif self.filter.special_filter == 20:  # Manufacturer
            if self.operator_type == 0:    # Strong equal
                if self.ignore_case:
                    products = Product.objects.filter(
                        categories=self.filter.category,
                        manufacturer__name__iexact=self.regex.strip(),
                        sub_type__in=[STANDARD_PRODUCT, VARIANT],
                        active=True,
                        status__is_visible=True,
                    )
                else:
                    products = Product.objects.filter(
                        categories=self.filter.category,
                        manufacturer__name=self.regex.strip(),
                        sub_type__in=[STANDARD_PRODUCT, VARIANT],
                        active=True,
                        status__is_visible=True,
                    )
                self.products = products
            elif self.operator_type == 5:  # Strong equal with variants (OR)
                values = filter(
                    bool,
                    map(lambda x: x.strip(), self.regex.split('\n')))
                products = Product.objects.filter(
                    categories=self.filter.category,
                    manufacturer__name__in=values,
                    sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    active=True,
                    status__is_visible=True,
                )
                self.products = products
            elif self.operator_type == 10:  # Regular expressions
                products = Product.objects.filter(
                    categories=self.filter.category,
                    manufacturer__name__iregex=self.regex,
                    sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    active=True,
                    status__is_visible=True,
                )
                self.products = products
        elif self.filter.special_filter == 30:  # Status
            if self.operator_type == 0:    # Strong equal
                if self.ignore_case:
                    products = Product.objects.filter(
                        categories=self.filter.category,
                        status__name__iexact=self.regex.strip(),
                        sub_type__in=[STANDARD_PRODUCT, VARIANT],
                        active=True,
                        status__is_visible=True,
                    )
                else:
                    products = Product.objects.filter(
                        categories=self.filter.category,
                        status__name=self.regex.strip(),
                        sub_type__in=[STANDARD_PRODUCT, VARIANT],
                        active=True,
                        status__is_visible=True,
                    )
                self.products = products
            elif self.operator_type == 5:  # Strong equal with variants (OR)
                values = filter(
                    bool,
                    map(lambda x: x.strip(), self.regex.split('\n')))
                products = Product.objects.filter(
                    categories=self.filter.category,
                    status__name__in=values,
                    sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    active=True,
                    status__is_visible=True,
                )
                self.products = products
            elif self.operator_type == 10:  # Regular expressions
                products = Product.objects.filter(
                    categories=self.filter.category,
                    status__name__iregex=self.regex,
                    sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    active=True,
                    status__is_visible=True,
                )
                self.products = products
        elif self.filter.special_filter == 10:  # Price
            if self.operator_type == 5:  # Strong equal with variants (OR)
                values = filter(
                    bool,
                    map(lambda x: x.strip(), self.regex.split('\n')))
                products = Product.objects.filter(
                    categories=self.filter.category,
                    sub_type__in=[STANDARD_PRODUCT, VARIANT],
                    active=True,
                    status__is_visible=True,
                )
                for v in values:
                    t = v.split()
                    if t[0] == '<':
                        products = products.filter(
                            effective_price__lt=float(t[1]))
                    elif t[0] == '<=':
                        products = products.filter(
                            effective_price__lte=float(t[1]))
                    elif t[0] == '>':
                        products = products.filter(
                            effective_price__gt=float(t[1]))
                    elif t[0] == '>=':
                        products = products.filter(
                            effective_price__gte=float(t[1]))
                self.products = products

    def get_absolute_url(self):
        from hr_urls.utils import get_hr_urls
        return '%s%s' % (
            self.filter.category.get_absolute_url(),
            get_hr_urls(
                self.filter.category,
                '%s=%s' % (self.filter.identificator, self.identificator)
            )
        )
