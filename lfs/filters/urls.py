# coding: utf-8
from django.conf.urls.defaults import *

# These url patterns use for admin interface
urlpatterns = patterns(
    'lfs.filters.admin_views',
    url(r'^filters-for-category/(?P<category_id>[-\d]*)/$',
        'filters_for_category', name="admin_filters_for_category"),
    url(r'^delete-filters-from-category/(?P<category_id>[-\d]*)/$',
        'delete_filters_from_category',
        name="admin_delete_filters_from_category"),
    url(r'^option-list-for-filter/$',
        'option_list_for_filter', name="admin_option_list_for_filter"),

    url(r'^parse-filters-for-category/(?P<category_id>[-\d]*)/$',
        'parse_filters_for_category',
        name="admin_parse_filters_for_category"),
    url(r'^close-options-for-filter/(?P<filter_id>[-\d]*)/$',
        'close_options_for_filter', name="admin_close_options_for_filter"),
    url(r'^options-for-filter/(?P<filter_id>[-\d]*)/$',
        'options_for_filter', name="admin_options_for_filter"),
    url(r'^move-filter-option/(?P<option_id>\d+)$',
        "move_filter_option", name="admin_move_filter_option"),
    url(r'^move-filter/(?P<filter_id>[-\d]*)/$',
        'move_filter', name="admin_move_filter"),
    url(r'^delete-filter/(?P<filter_id>\d+)$',
        "delete_filter", name="admin_delete_filter"),
    url(r'^parse-filter-option/(?P<option_id>\d+)$',
        "parse_filter_option", name="admin_parse_filter_option"),
    url(r'^parse-filter/(?P<filter_id>\d+)$',
        "parse_filter", name="admin_parse_filter"),
    url(r'^combine-filter-option/(?P<option_id>\d+)$',
        "combine_filter_option", name="admin_combine_filter_option"),
    url(r'^delete-filter-option/(?P<option_id>\d+)$',
        "delete_filter_option", name="admin_delete_filter_option"),

    url(r'^autoconfigure/(?P<filter_id>\d+)$',
        "autoconfigure_filter", name="admin_autoconfigure_filter"),
    url(r'^sort-filter/(?P<filter_id>\d+)$',
        "sort_filter", name="admin_sort_filter"),
    url(r'^action-for-filter/(?P<filter_id>\d+)$',
        "autoconfigurer_action", name="admin_autoconfigurer_action"),
    url(r'^autofill-filter/(?P<filter_id>\d+)$',
        "autofill_filter", name="admin_autofill_filter"),
    url(r'^clear-filter/(?P<filter_id>\d+)$',
        "clear_filter", name="admin_clear_filter"),
    url(r'^regenerate-filteroptions/(?P<filter_id>\d+)$',
        "regenerate_filteroptions",
        name="regenerate_filteroptions"),
    url(r'^clear-emty-filteroptions/(?P<filter_id>\d+)$',
        "clear_filteroptins_without_products",
        name="clear_filteroptins_without_products")
)
