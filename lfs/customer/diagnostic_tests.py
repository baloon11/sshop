from diagnostic.base import BaseDiagnosticTest
from django.utils.translation import ugettext as _


class UserWithoutCustomersTest(BaseDiagnosticTest):
    def get_description(self):
        return _(u'Authenticating users against customer')

    def how_to_fix(self):
        return _(u'Create customer for user')

    def _get_items_with_problems(self):
        from django.contrib.auth.models import User
        users = User.objects.filter(customer=None)
        return users

    def get_report(self):
        from django.core import urlresolvers
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for user in _items:
            _report += '<li><a href="%s" target="_blank">%s</a></li>' % (
                urlresolvers.reverse(
                        'admin:auth_user_change',
                        args=(user.pk,),
                        ), user.username)
        _report += '</ul>'
        return _report

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d users without customer.') % p_count)
        else:
            return (True, _(u'OK'))

    def fix(self, solution=0):
        from lfs.customer.models import Customer
        from ..core.utils import (
            generate_referral_code,
            get_default_shop,
            )
        # from ..core.signals import customer_added
        _items = self._get_items_with_problems()
        if _items:
            for user in _items:
                customer = Customer.objects.create(
                    user=user,
                    first_name=user.first_name,
                    last_name=user.last_name
                    )
                customer.save()
                customer.add_email(email=user.email)
                # adding ref_code for customer
                f = True
                ref_code = None
                while f:
                    shop = get_default_shop()
                    ref_code = generate_referral_code(
                        shop.template_of_discount_code
                        )
                    try:
                        z = Customer.objects.get(ref_code=ref_code)
                    except Customer.DoesNotExist:
                        f = False
                customer.ref_code = ref_code
                customer.save()
                # customer_added.send(user)
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))
