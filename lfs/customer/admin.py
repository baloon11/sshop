# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext as _

from .models import Customer, Address, Phone, EmailAddress


class PhoneInline(admin.TabularInline):
    model = Phone
    extra = 0


class AddressInline(admin.TabularInline):
    model = Address
    extra = 0


class EmailAddressInline(admin.TabularInline):
    model = EmailAddress
    extra = 0


class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'full_name',
        'user',
        'session',
        'selected_shipping_method',
        'selected_payment_method',
        'saving_account')
    list_filter = (
        'selected_shipping_method',
        'selected_payment_method')
    search_fields = ['user__username', 'session']
    readonly_fields = ('user', 'session')
    inlines = [PhoneInline, EmailAddressInline, AddressInline]
    fieldsets = [
        (None, {
            'fields': ['user', 'session']
        }),
        (_(u'Selected methods'), {
            'fields': ['selected_shipping_method', 'selected_payment_method']
        }),
        (None, {
            'fields': [
                'first_name',
                'last_name',
                'full_name',
                'date_of_birth',
                'ref_code',
                'saving_account',
                'extras',
            ]
        }),
    ]

admin.site.register(Customer, CustomerAdmin)
