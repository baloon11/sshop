# coding: utf-8
from .models import Customer, Phone, EmailAddress


class EmailFinder(object):
    def __init__(self, original_customer):
        self.original_customer = original_customer

    def find(self):
        all_emails = self.original_customer.emails.filter(default=True)
        emails_for_search = [x.email for x in all_emails]

        emails = EmailAddress.objects.filter(
            email__in=emails_for_search,
            default=True)
        customers = Customer.objects.filter(
            emails__in=emails).exclude(
                id=self.original_customer.id,
                user=None)
        if customers:
            return customers[0]
        else:
            return None


class PhoneFinder(object):
    def __init__(self, original_customer):
        self.original_customer = original_customer

    def find(self):
        all_phones = self.original_customer.phones.filter(default=True)
        phones_for_search = [x.number for x in all_phones]

        phones = Phone.objects.filter(
            number__in=phones_for_search,
            default=True)
        customers = Customer.objects.filter(
            phones__in=phones).exclude(
                id=self.original_customer.id,
                user=None)
        if customers:
            return customers[0]
        else:
            return None
