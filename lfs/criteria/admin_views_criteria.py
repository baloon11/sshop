# coding: utf-8
import simplejson

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from django.contrib.contenttypes.models import ContentType

from ..core.utils import LazyEncoder
from .models.criteria_objects import CriteriaObjects


def _render_criteria(request, content_type_id, object_id):
    content_type = ContentType.objects.get(id=content_type_id)
    criteria = CriteriaObjects.objects.filter(
        content_type=content_type, content_id=object_id)

    return render_to_string('admin/criteria_includes/criteria_list.html', {
        'object_id': object_id,
        'content_type_id': content_type_id,
        'criteria': criteria,
    })


@permission_required(
    "core.manage_criteria",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def criteria_for_object(request, content_type_id, object_id):
    return HttpResponse(_render_criteria(request, content_type_id, object_id))


def move_criterion(request, criterion_id):
    if request.user.has_perm('core.manage_criteria'):
        criterion = CriteriaObjects.objects.get(id=criterion_id)
        direction = request.GET.get("direction", "0")
        if direction == "1":
            criterion.position += 15
        else:
            criterion.position -= 15
            if criterion.position < 0:
                criterion.position = 10
        criterion.save()
        update_criteria_positions(criterion.content_type, criterion.content_id)
        result = simplejson.dumps({
            "html": [[
                "#criteria-list-wrapper",
                _render_criteria(
                    request,
                    criterion.content_type.id,
                    criterion.content_id)]]
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#criteria-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def delete_criterion(request, criterion_id):
    """Deletes the field.
    """
    if request.user.has_perm('core.manage_criteria'):
        criterion = CriteriaObjects.objects.get(id=criterion_id)
        criterion.delete()
        update_criteria_positions(criterion.content_type, criterion.content_id)
        result = simplejson.dumps({
            "html": [[
                "#criteria-list-wrapper",
                _render_criteria(
                    request,
                    criterion.content_type.id, criterion.content_id)]],
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#criteria-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


@permission_required(
    "core.manage_criteria",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def change_criterion(
        request, criterion_id,
        template_name="admin/criteria_includes/change_criterion.html"):
    criterion = CriteriaObjects.objects.get(id=criterion_id)
    if request.POST:
        form = criterion.criterion.form(data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(
                '<html><script>opener.on_criteria_popup_close();'
                'window.close();</script></html>')
    else:
        form = criterion.criterion.form()
    return render_to_response(template_name, RequestContext(request, {
        'form': form,
        'is_popup': True,
    }))


@permission_required(
    "core.manage_criteria",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def add_criterion(
        request, content_type_id, object_id, model_id,
        template_name="admin/criteria_includes/change_criterion.html"):
    model_class = ContentType.objects.get(id=model_id)
    content_object = ContentType.objects.get(id=content_type_id)
    if request.POST:
        criterion = model_class.model_class()

        form = criterion().form(data=request.POST)
        if form.is_valid():
            criteria = CriteriaObjects.objects.filter(
                content_type=content_object,
                content_id=object_id).order_by('-position')
            if criteria:
                position = criteria[0].position + 10
            else:
                position = 10
            c = form.save()
            co = CriteriaObjects()
            co.criterion = c
            co.content_type = content_object
            co.content_id = object_id
            co.position = position
            co.save()
            return HttpResponse(
                '<html><script>opener.on_criteria_popup_close();'
                'window.close();</script></html>')
    else:
        criterion = model_class.model_class()
        form = criterion().form()

    return render_to_response(template_name, RequestContext(request, {
        'form': form,
        'is_popup': True,
    }))


def delete_all_criteria(request, content_type_id, object_id):
    if request.user.has_perm('core.manage_criteria'):
        content_type = ContentType.objects.get(id=content_type_id)
        criteria = CriteriaObjects.objects.filter(
            content_type=content_type, content_id=object_id)
        for criterion in criteria:
            criterion.delete()
        result = simplejson.dumps({
            "html": [[
                "#criteria-list-wrapper",
                _render_criteria(request, content_type_id, object_id)]],
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#criteria-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def update_criteria_positions(content_type, content_id):
    for i, obj in enumerate(
            CriteriaObjects.objects.filter(
                content_type=content_type, content_id=content_id)):
        obj.position = (i + 1) * 10
        obj.save()
