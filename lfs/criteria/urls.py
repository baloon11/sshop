# coding: utf-8
from django.conf.urls.defaults import *

# These url patterns use for admin interface
urlpatterns = patterns(
    'lfs.criteria.admin_views_criteria',
    url(
        r'^criteria-for-object/(?P<content_type_id>[-\d]*)/'
        '(?P<object_id>[-\d]*)/$',
        'criteria_for_object', name="admin_criteria_for_object"),
    url(
        r'^move-criterion/(?P<criterion_id>[-\d]*)/$',
        'move_criterion', name="admin_move_criterion"),
    url(
        r'^delete-criterion/(?P<criterion_id>[-\d]*)$',
        "delete_criterion", name="admin_delete_criterion"),
    url(
        r'^change-criterion/(?P<criterion_id>[-\d]*)$',
        "change_criterion", name="admin_change_criterion"),
    url(
        r'^add-criterion/(?P<content_type_id>[-\d]*)/'
        '(?P<object_id>[-\d]*)/(?P<model_id>[-\d]*)/$',
        "add_criterion", name="admin_criteria_criterion_add"),
    url(
        r'^delete-all-criteria/(?P<content_type_id>[-\d]*)/'
        '(?P<object_id>[-\d]*)/$',
        'delete_all_criteria', name="admin_delete_all_criteria")
)
