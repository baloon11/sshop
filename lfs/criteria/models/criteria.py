# coding: utf-8
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes import generic
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _, ugettext
from suit.widgets import SuitDateWidget, SuitTimeWidget
from django import forms
import pytz
import autocomplete_light

# lfs imports
# import lfs.core.utils
# from lfs import shipping
from ...core.utils import import_symbol
# from ..core.models import Shop, Country
from .criteria_objects import CriteriaObjects
from ..settings import (
    EQUAL,
    LESS_THAN,
    LESS_THAN_EQUAL,
    GREATER_THAN,
    GREATER_THAN_EQUAL,
    NUMBER_OPERATORS,
    SELECT_OPERATORS,
    IS,
    IS_NOT,
    IS_VALID,
    IS_NOT_VALID,
)
from ...payment.models import PaymentMethod
from ...shipping.models import ShippingMethod
from ...catalog.models import Product, Category


class Criterion(object):
    """Base class for all lfs criteria.
    """
    pass


class CartPriceCriterion(models.Model, Criterion):
    """A criterion for the cart price.
    """
    operator = models.PositiveIntegerField(
        _(u"Operator"), blank=True, null=True, choices=NUMBER_OPERATORS)
    price = models.FloatField(_(u"Price"), default=0.0)

    criterion_name = ugettext(u"Cart Price")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        return ugettext(
            "Cart Price: %(operator)s %(price)s") %\
            {
                'operator': self.get_operator_display(),
                'price': self.price
            }

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"price"

    @property
    def name(self):
        """Returns the descriptive name of the criterion.
        """
        return self.criterion_name

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.

        If product is given the price is taken from the product otherwise from
        the cart.
        """

        if product is not None:
            cart_price = product.get_price(request)
        else:
            from lfs.cart import utils as cart_utils
            cart = cart_utils.get_cart(request)

            if cart is None:
                return False

            cart_price = cart.get_price(request)

        if self.operator == LESS_THAN and (cart_price < self.price):
            return True
        if self.operator == LESS_THAN_EQUAL and (cart_price <= self.price):
            return True
        if self.operator == GREATER_THAN and (cart_price > self.price):
            return True
        if self.operator == GREATER_THAN_EQUAL and (cart_price >= self.price):
            return True
        if self.operator == EQUAL and (cart_price == self.price):
            return True

        return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.price

    def form(self, **kwargs):
        return CartPriceCriterionForm(instance=self, **kwargs)


class CartPriceCriterionForm(forms.ModelForm):
    class Meta:
        model = CartPriceCriterion


class PaymentMethodCriterion(models.Model, Criterion):
    """A criterion for the payment method.
    """
    operator = models.PositiveIntegerField(
        _(u"Operator"), blank=True, null=True, choices=SELECT_OPERATORS)
    payment_methods = models.ManyToManyField(
        PaymentMethod, verbose_name=_(u"Payment methods"))

    criteria_objects = generic.GenericRelation(
        CriteriaObjects,
        object_id_field="criterion_id", content_type_field="criterion_type")

    criterion_name = ugettext(u"Payment method")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        values = []
        for value in self.value.all():
            values.append(value.name)

        return ugettext(
            "Payment: %(operator)s %(payments)s") % {
                'operator': self.get_operator_display(),
                'payments': ", ".join(values)}

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"payment_method"

    @property
    def name(self):
        """Returns the descriptive name of the criterion.
        """
        return self.criterion_name

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        from ...payment.utils import get_selected_payment_method
        from ..utils import is_valid as _is_valid
        content_object = self.criteria_objects.filter()[0].content
        if isinstance(content_object, PaymentMethod):
            is_payment_method = True
        else:
            is_payment_method = False

        if not is_payment_method and self.operator == IS:
            payment_method = get_selected_payment_method(request)
            return payment_method in self.payment_methods.all()
        elif not is_payment_method and self.operator == IS_NOT:
            payment_method = get_selected_payment_method(request)
            return payment_method not in self.payment_methods.all()
        elif self.operator == IS_VALID:
            for pm in self.payment_methods.all():
                if not _is_valid(request, pm, product):
                    return False
            return True
        elif self.operator == IS_NOT_VALID:
            for pm in self.payment_methods.all():
                if _is_valid(request, pm, product):
                    return False
            return True
        else:
            return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.payment_methods

    def form(self, **kwargs):
        return PaymentMethodCriterionForm(instance=self, **kwargs)


class PaymentMethodCriterionForm(forms.ModelForm):
    class Meta:
        model = PaymentMethodCriterion


class ShippingMethodCriterion(models.Model, Criterion):
    """A criterion for the shipping method.
    """
    operator = models.PositiveIntegerField(
        _(u"Operator"), blank=True, null=True, choices=SELECT_OPERATORS)
    shipping_methods = models.ManyToManyField(
        ShippingMethod, verbose_name=_(u"Shipping methods"))

    criteria_objects = generic.GenericRelation(
        CriteriaObjects,
        object_id_field="criterion_id", content_type_field="criterion_type")

    criterion_name = ugettext(u"Shipping method")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        values = []
        for value in self.value.all():
            values.append(value.name)

        return ugettext(
            "Shipping: %(operator)s %(shipping)s") % {
                'operator': self.get_operator_display(),
                'shipping': ", ".join(values)}

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"shipping_method"

    @property
    def name(self):
        """Returns the descriptive name of the criterion.
        """
        return self.criterion_name

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        # Check whether the criteria is part of a shipping method if so the
        # operator IS and IS_NOT are not allowed. This will later exluded by
        # the UID.

        # The reason why we have to check this is that the get_selected
        # shipping_method checks for valid shipping methods and call this
        # method again, so that we get an infinte recursion.

        from ...shipping.utils import get_selected_shipping_method
        from ..utils import is_valid as _is_valid

        content_object = self.criteria_objects.filter()[0].content
        if isinstance(content_object, ShippingMethod):
            is_shipping_method = True
        else:
            is_shipping_method = False

        if not is_shipping_method and self.operator == IS:
            shipping_method = get_selected_shipping_method(request)
            return shipping_method in self.shipping_methods.all()
        elif not is_shipping_method and self.operator == IS_NOT:
            shipping_method = get_selected_shipping_method(request)
            return shipping_method not in self.shipping_methods.all()
        elif self.operator == IS_VALID:
            for sm in self.shipping_methods.all():
                if not _is_valid(request, sm, product):
                    return False
            return True
        elif self.operator == IS_NOT_VALID:
            for sm in self.shipping_methods.all():
                if _is_valid(request, sm, product):
                    return False
            return True
        else:
            return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.shipping_methods

    def form(self, **kwargs):
        return ShippingMethodCriterionForm(instance=self, **kwargs)


class ShippingMethodCriterionForm(forms.ModelForm):
    class Meta:
        model = ShippingMethodCriterion


class UserCriterion(models.Model, Criterion):
    """A criterion for user content objects
    """
    users = models.ManyToManyField(User)

    criterion_name = ugettext(u"User")

    class Meta:
        app_label = "criteria"

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"user"

    @property
    def name(self):
        """Returns the descriptive name of the criterion.
        """
        return self.criterion_name

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        return request.user in self.users.all()

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.users

    def form(self, **kwargs):
        return UserCriterionForm(instance=self, **kwargs)


class UserCriterionForm(forms.ModelForm):
    class Meta:
        model = UserCriterion


class CustomCriterion(models.Model, Criterion):
    """A custom criterions
    """
    operator = models.PositiveIntegerField(
        _(u"Operator"),
        blank=True, null=True, choices=NUMBER_OPERATORS)
    function_path = models.CharField(
        _('Path to function'), max_length=250, default='')

    criterion_name = ugettext(u"Custom criterion")

    class Meta:
        app_label = "criteria"

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"custom"

    @property
    def name(self):
        """Returns the descriptive name of the criterion.
        """
        return self.criterion_name

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        try:
            check_valid = import_symbol(self.function_path)
            return check_valid(request, product)
        except:
            return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.function_path

    def form(self, **kwargs):
        return CustomCriterionForm(instance=self, **kwargs)


class CustomCriterionForm(forms.ModelForm):
    class Meta:
        model = CustomCriterion


class ProductAmountCriterion(models.Model, Criterion):
    """A criterion for amount of products.
    """
    product = models.ForeignKey(
        Product,
        verbose_name=ugettext(u'Product')
    )
    operator = models.PositiveIntegerField(
        _(u"Operator"), blank=True, null=True, choices=NUMBER_OPERATORS)
    amount = models.PositiveSmallIntegerField(ugettext(u'Amount'), default=0)

    criterion_name = ugettext(u"Product amount")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        return ugettext(
            "%(product_name)s amount: %(operator)s %(amount)s") %\
            {
                'operator': self.get_operator_display(),
                'product_name': self.product.name,
                'amount': self.amount
            }

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"product_amount"

    @property
    def name(self):
        """Returns the product name
        """
        return ugettext(u'Amount of %s') % self.product.name

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.

        If product is given the price is taken from the product otherwise from
        the cart.
        """
        from lfs.cart import utils as cart_utils
        cart = cart_utils.get_cart(request)

        if cart is None:
            return False

        if product is None:
            cart_items = cart.get_items()
        else:
            cart_items = cart.get_item(product)  # returned list

        for cart_item in cart_items:
            item_amount = cart_item.amount

            if self.product.uid == cart_item.product.uid:
                if self.operator == LESS_THAN and (item_amount < self.amount):
                    return True
                if self.operator == LESS_THAN_EQUAL and\
                        (item_amount <= self.amount):
                    return True
                if self.operator == GREATER_THAN and (item_amount > self.amount):
                    return True
                if self.operator == GREATER_THAN_EQUAL and\
                        (item_amount >= self.amount):
                    return True
                if self.operator == EQUAL and (item_amount == self.amount):
                    return True

        return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.amount

    def form(self, **kwargs):
        return ProductAmountCriterionForm(instance=self, **kwargs)


class ProductAmountCriterionForm(forms.ModelForm):

    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }
        model = ProductAmountCriterion


class ProductAmountCategoryCriterion(models.Model, Criterion):
    """A criterion for amount of products of category.
    """
    categories = models.ManyToManyField(
        Category,
        verbose_name=ugettext(u'Categories')
    )
    operator = models.PositiveIntegerField(
        _(u"Operator"), blank=True, null=True, choices=NUMBER_OPERATORS)
    amount = models.PositiveSmallIntegerField(ugettext(u'Amount'), default=0)

    criterion_name = ugettext(u"Product amount of category")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        return ugettext(
            "Amount products from %(category_name)s: %(operator)s %(amount)s") %\
            {
                'operator': self.get_operator_display(),
                'category_name': ', '.join([c.name for c in self.categories.all()]),
                'amount': self.amount
            }

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"product_amount_category"

    @property
    def name(self):
        """Returns the product name
        """
        return ugettext(u'Amount products from %s') %\
            ', '.join([c.name for c in self.categories.all()])

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        from lfs.cart import utils as cart_utils
        cart = cart_utils.get_cart(request)

        if cart is None:
            return False

        categories = {}
        # ProductAmountCategoryCalculator
        if 'category' in kwargs and 'amount' in kwargs:
            categories = {
                kwargs['category']: kwargs['amount']
            }
        # ProductAmountCalculator
        elif product is not None:
            cart_items = cart.get_item(product)
            for cart_item in cart_items:
                for category in cart_item.product.categories.all():
                    if category in categories:
                        categories[category] += cart_item.amount
                    else:
                        categories[category] = cart_item.amount
        # is not CategoryDiscountCalculator and not ProductDiscountCalculator
        else:
            cart_items = cart.get_items()
            # calculate amount products of categories
            for cart_item in cart_items:
                for category in cart_item.product.categories.all():
                    if category in categories:
                        categories[category] += cart_item.amount
                    else:
                        categories[category] = cart_item.amount

        for category in self.categories.all():
            if self.operator == LESS_THAN and\
                    category in categories and\
                    (categories[category] < self.amount):
                return True
            if self.operator == LESS_THAN_EQUAL and\
                    category in categories and\
                    (categories[category] <= self.amount):
                return True
            if self.operator == GREATER_THAN and\
                    category in categories and\
                    (categories[category] > self.amount):
                return True
            if self.operator == GREATER_THAN_EQUAL and\
                    category in categories and\
                    (categories[category] >= self.amount):
                return True
            if self.operator == EQUAL and\
                    category in categories and\
                    (categories[category] == self.amount):
                return True

        return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.amount

    def form(self, **kwargs):
        return ProductAmountCategoryCriterionForm(instance=self, **kwargs)


class ProductAmountCategoryCriterionForm(forms.ModelForm):

    class Meta:
        widgets = {
            'categories': autocomplete_light.MultipleChoiceWidget(
                'CategoryAutocomplete'
            ),
        }
        model = ProductAmountCategoryCriterion


class DateTimeCriterion(models.Model, Criterion):
    """A criterion for amount of products of category.
    """
    date = models.DateField(
        verbose_name=ugettext(u'Date'),
        blank=True,
        null=True
    )
    time = models.TimeField(
        verbose_name=ugettext(u'Time'),
        blank=True,
        null=True
    )
    operator = models.PositiveIntegerField(
        _(u"Operator"), blank=True, null=True, choices=NUMBER_OPERATORS)

    criterion_name = ugettext(u"Date and time")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        return ugettext(
            "Date and time: %(operator)s %(date)s %(time)s") %\
            {
                'operator': self.get_operator_display(),
                'date': self.date,
                'time': self.time,
            }

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"date_and_time"

    @property
    def name(self):
        """Returns the product name
        """
        return ugettext(u'Date and time')

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        from django.utils import timezone
        time = timezone.now()
        time = time.astimezone(pytz.timezone(settings.TIME_ZONE))
        if self.date:
            if time.date() < self.date:
                return False
        if self.time:
            if time.time() < self.time:
                return False

        return True

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        if self.date and not self.time:
            return self.date
        elif self.time and not self.date:
            return self.time
        elif self.time and self.date:
            return '%s %s' % (self.date, self.time)

        return ''

    def form(self, **kwargs):
        return DateTimeCriterionForm(instance=self, **kwargs)


class DateTimeCriterionForm(forms.ModelForm):

    class Meta:
        widgets = {
            'date': SuitDateWidget,
            'time': SuitTimeWidget,
        }
        model = DateTimeCriterion


class RegisteredUserCriterion(models.Model, Criterion):
    """A criterion for amount of products of category.
    """
    registered = models.BooleanField(
        ugettext(u'Registered'), default=False)

    criterion_name = ugettext(u"Registered user")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        return ugettext(
            "Registered user: %(registered)s") %\
            {
                'registered': self.registered
            }

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"registered_user"

    @property
    def name(self):
        """Returns the product name
        """
        return ugettext(u'Registered user')

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        if request.user.is_authenticated() is True:
            return True

        return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return self.registered

    def form(self, **kwargs):
        return RegisteredUserCriterionForm(instance=self, **kwargs)


class RegisteredUserCriterionForm(forms.ModelForm):

    class Meta:
        model = RegisteredUserCriterion


class GroupUserCriterion(models.Model, Criterion):
    """A criterion for amount of products of category.
    """
    groups = models.ManyToManyField(
        Group,
        verbose_name=ugettext(u'Groups'))

    criterion_name = ugettext(u"User groups")

    class Meta:
        app_label = "criteria"

    def __unicode__(self):
        return ugettext(
            "Groups: %(groups)s") %\
            {
                'groups': ', '.join([g.name for g in self.groups.all()])
            }

    @property
    def content_type(self):
        """Returns the content_type of the criterion as lower string.

        This is for instance used to select the appropriate form for the
        criterion.
        """
        return u"group_user"

    @property
    def name(self):
        """Returns the product name
        """
        return ugettext(u'User groups')

    def is_valid(self, request, product=None, **kwargs):
        """Returns True if the criterion is valid.
        """
        for group in request.user.groups.all():
            if group in self.groups.all():
                return True
        return False

    @property
    def value(self):
        """Returns the value of the criterion.
        """
        return ', '.join([g.name for g in self.groups.all()])

    def form(self, **kwargs):
        return GroupUserCriterionForm(instance=self, **kwargs)


class GroupUserCriterionForm(forms.ModelForm):

    class Meta:
        model = GroupUserCriterion
