# coding: utf-8
from django import template
from django.conf import settings
from django.contrib.contenttypes.models import ContentType


register = template.Library()


@register.inclusion_tag(
    'admin/criteria_includes/criteria_block.html', takes_context=True)
def admin_criteria_info_block(context):
    content_object = ContentType.objects.get(pk=context['content_type_id'])
    object_id = context['object_id']
    object = content_object.get_object_for_this_type(pk=object_id)
    models = [
        a.split('.')[-1].lower() for a in settings.CRITERIONS
    ]
    criteria_models = ContentType.objects.filter(
        model__in=models,
    )

    return {
        'content_type': content_object,
        'object': object,
        'criteria_models': criteria_models,
    }
