# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CriteriaObjects'
        db.create_table('criteria_criteriaobjects', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('criterion_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='criterion', to=orm['contenttypes.ContentType'])),
            ('criterion_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='content_type', to=orm['contenttypes.ContentType'])),
            ('content_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')(default=999)),
        ))
        db.send_create_signal('criteria', ['CriteriaObjects'])

        # Adding model 'CartPriceCriterion'
        db.create_table('criteria_cartpricecriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.FloatField')(default=0.0)),
        ))
        db.send_create_signal('criteria', ['CartPriceCriterion'])

        # Adding model 'CombinedLengthAndGirthCriterion'
        db.create_table('criteria_combinedlengthandgirthcriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('clag', self.gf('django.db.models.fields.FloatField')(default=0.0)),
        ))
        db.send_create_signal('criteria', ['CombinedLengthAndGirthCriterion'])

        # Adding model 'CountryCriterion'
        db.create_table('criteria_countrycriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('criteria', ['CountryCriterion'])

        # Adding M2M table for field countries on 'CountryCriterion'
        db.create_table('criteria_countrycriterion_countries', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('countrycriterion', models.ForeignKey(orm['criteria.countrycriterion'], null=False)),
            ('country', models.ForeignKey(orm['core.country'], null=False))
        ))
        db.create_unique('criteria_countrycriterion_countries', ['countrycriterion_id', 'country_id'])

        # Adding model 'HeightCriterion'
        db.create_table('criteria_heightcriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('height', self.gf('django.db.models.fields.FloatField')(default=0.0)),
        ))
        db.send_create_signal('criteria', ['HeightCriterion'])

        # Adding model 'LengthCriterion'
        db.create_table('criteria_lengthcriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('length', self.gf('django.db.models.fields.FloatField')(default=0.0)),
        ))
        db.send_create_signal('criteria', ['LengthCriterion'])

        # Adding model 'PaymentMethodCriterion'
        db.create_table('criteria_paymentmethodcriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('criteria', ['PaymentMethodCriterion'])

        # Adding M2M table for field payment_methods on 'PaymentMethodCriterion'
        db.create_table('criteria_paymentmethodcriterion_payment_methods', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('paymentmethodcriterion', models.ForeignKey(orm['criteria.paymentmethodcriterion'], null=False)),
            ('paymentmethod', models.ForeignKey(orm['payment.paymentmethod'], null=False))
        ))
        db.create_unique('criteria_paymentmethodcriterion_payment_methods', ['paymentmethodcriterion_id', 'paymentmethod_id'])

        # Adding model 'ShippingMethodCriterion'
        db.create_table('criteria_shippingmethodcriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('criteria', ['ShippingMethodCriterion'])

        # Adding M2M table for field shipping_methods on 'ShippingMethodCriterion'
        db.create_table('criteria_shippingmethodcriterion_shipping_methods', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('shippingmethodcriterion', models.ForeignKey(orm['criteria.shippingmethodcriterion'], null=False)),
            ('shippingmethod', models.ForeignKey(orm['shipping.shippingmethod'], null=False))
        ))
        db.create_unique('criteria_shippingmethodcriterion_shipping_methods', ['shippingmethodcriterion_id', 'shippingmethod_id'])

        # Adding model 'UserCriterion'
        db.create_table('criteria_usercriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('criteria', ['UserCriterion'])

        # Adding M2M table for field users on 'UserCriterion'
        db.create_table('criteria_usercriterion_users', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('usercriterion', models.ForeignKey(orm['criteria.usercriterion'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('criteria_usercriterion_users', ['usercriterion_id', 'user_id'])

        # Adding model 'WeightCriterion'
        db.create_table('criteria_weightcriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.FloatField')(default=0.0)),
        ))
        db.send_create_signal('criteria', ['WeightCriterion'])

        # Adding model 'WidthCriterion'
        db.create_table('criteria_widthcriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('width', self.gf('django.db.models.fields.FloatField')(default=0.0)),
        ))
        db.send_create_signal('criteria', ['WidthCriterion'])

        # Adding model 'DistanceCriterion'
        db.create_table('criteria_distancecriterion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('operator', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('distance', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('module', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('criteria', ['DistanceCriterion'])


    def backwards(self, orm):
        # Deleting model 'CriteriaObjects'
        db.delete_table('criteria_criteriaobjects')

        # Deleting model 'CartPriceCriterion'
        db.delete_table('criteria_cartpricecriterion')

        # Deleting model 'CombinedLengthAndGirthCriterion'
        db.delete_table('criteria_combinedlengthandgirthcriterion')

        # Deleting model 'CountryCriterion'
        db.delete_table('criteria_countrycriterion')

        # Removing M2M table for field countries on 'CountryCriterion'
        db.delete_table('criteria_countrycriterion_countries')

        # Deleting model 'HeightCriterion'
        db.delete_table('criteria_heightcriterion')

        # Deleting model 'LengthCriterion'
        db.delete_table('criteria_lengthcriterion')

        # Deleting model 'PaymentMethodCriterion'
        db.delete_table('criteria_paymentmethodcriterion')

        # Removing M2M table for field payment_methods on 'PaymentMethodCriterion'
        db.delete_table('criteria_paymentmethodcriterion_payment_methods')

        # Deleting model 'ShippingMethodCriterion'
        db.delete_table('criteria_shippingmethodcriterion')

        # Removing M2M table for field shipping_methods on 'ShippingMethodCriterion'
        db.delete_table('criteria_shippingmethodcriterion_shipping_methods')

        # Deleting model 'UserCriterion'
        db.delete_table('criteria_usercriterion')

        # Removing M2M table for field users on 'UserCriterion'
        db.delete_table('criteria_usercriterion_users')

        # Deleting model 'WeightCriterion'
        db.delete_table('criteria_weightcriterion')

        # Deleting model 'WidthCriterion'
        db.delete_table('criteria_widthcriterion')

        # Deleting model 'DistanceCriterion'
        db.delete_table('criteria_distancecriterion')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'catalog.deliverytime': {
            'Meta': {'ordering': "('min',)", 'object_name': 'DeliveryTime'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.FloatField', [], {}),
            'min': ('django.db.models.fields.FloatField', [], {}),
            'unit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.country': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'criteria.cartpricecriterion': {
            'Meta': {'object_name': 'CartPriceCriterion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'})
        },
        'criteria.combinedlengthandgirthcriterion': {
            'Meta': {'object_name': 'CombinedLengthAndGirthCriterion'},
            'clag': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'criteria.countrycriterion': {
            'Meta': {'object_name': 'CountryCriterion'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['core.Country']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'criteria.criteriaobjects': {
            'Meta': {'ordering': "['position']", 'object_name': 'CriteriaObjects'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'content_type'", 'to': "orm['contenttypes.ContentType']"}),
            'criterion_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'criterion_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'criterion'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '999'})
        },
        'criteria.distancecriterion': {
            'Meta': {'object_name': 'DistanceCriterion'},
            'distance': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'criteria.heightcriterion': {
            'Meta': {'object_name': 'HeightCriterion'},
            'height': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'criteria.lengthcriterion': {
            'Meta': {'object_name': 'LengthCriterion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'criteria.paymentmethodcriterion': {
            'Meta': {'object_name': 'PaymentMethodCriterion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'payment_methods': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['payment.PaymentMethod']", 'symmetrical': 'False'})
        },
        'criteria.shippingmethodcriterion': {
            'Meta': {'object_name': 'ShippingMethodCriterion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'shipping_methods': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shipping.ShippingMethod']", 'symmetrical': 'False'})
        },
        'criteria.usercriterion': {
            'Meta': {'object_name': 'UserCriterion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.User']", 'symmetrical': 'False'})
        },
        'criteria.weightcriterion': {
            'Meta': {'object_name': 'WeightCriterion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.FloatField', [], {'default': '0.0'})
        },
        'criteria.widthcriterion': {
            'Meta': {'object_name': 'WidthCriterion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operator': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.FloatField', [], {'default': '0.0'})
        },
        'fields.fieldsobjects': {
            'Meta': {'ordering': "['position']", 'object_name': 'FieldsObjects'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fields'", 'to': "orm['contenttypes.ContentType']"}),
            'field_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'field_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'field'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '999'})
        },
        'payment.paymentmethod': {
            'Meta': {'ordering': "('priority',)", 'object_name': 'PaymentMethod'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sshop_currencies.Currency']"}),
            'deletable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'extra': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'module': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tax': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tax.Tax']", 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'shipping.shippingmethod': {
            'Meta': {'ordering': "('priority',)", 'object_name': 'ShippingMethod'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'delivery_time': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.DeliveryTime']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'extra': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'price_calculator': ('django.db.models.fields.CharField', [], {'default': "'lfs.default_price.DefaultShippingPriceCalculator'", 'max_length': '200'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tax': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tax.Tax']", 'null': 'True', 'blank': 'True'})
        },
        'sshop_currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'coeffitient': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'format_str': ('django.db.models.fields.CharField', [], {'default': "u'%(value).2f %(abbr)s'", 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        },
        'tax.tax': {
            'Meta': {'object_name': 'Tax'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rate': ('django.db.models.fields.FloatField', [], {'default': '0'})
        }
    }

    complete_apps = ['criteria']