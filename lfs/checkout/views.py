# coding: utf-8
import locale
import logging

from django.conf import settings
from django.views.decorators.http import require_http_methods
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext
from django.utils import simplejson
from django.utils.translation import ugettext_lazy as _
from django import forms

from .forms import (
    OnePageCheckoutForm,
    ShippingMethodForm,
    PaymentMethodForm,
)
from .settings import CHECKOUT_TYPE_ANON
# from .settings import CHECKOUT_TYPE_AUTH
from ..customer.forms import RegisterForm
# from ..payment.models import PaymentMethod
from ..voucher.models import Voucher
from ..voucher.settings import (
    MESSAGES,
    ABSOLUTE,
)
from ..core.signals import cart_changed
from ..core.utils import (
    get_default_shop,
    set_message_cookie,
)
from ..cart.utils import (
    get_cart,
)
from ..shipping.utils import (
    get_selected_shipping_method,
    get_shipping_costs,
    get_valid_shipping_methods,
    get_default_shipping_method,
    update_to_valid_shipping_method,
)
from ..payment.utils import (
    get_selected_payment_method,
    get_valid_payment_methods,
    get_payment_costs,
    get_default_payment_method,
    process_payment,
    update_to_valid_payment_method
)
from ..discounts.utils import (
    get_valid_discounts,
)
from ..voucher.utils import (
    get_current_voucher_number,
    set_current_voucher_number,
)


logger = logging.getLogger('sshop')


def login(request, template_name="lfs/checkout/login.html"):
    """Displays a form to login or register/login the user within the check out
    process.

    The form's post request goes to lfs.customer.views.login where all the
    logic happens - see there for more.
    """
    from ..core.signals import customer_added
    # If the user is already authenticate we don't want to show
    # this view at all
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse("lfs_checkout"))

    # If only anonymous checkout allowed we don't want to show this
    # view at all.
    if not getattr(settings, 'CHECKOUT_AUTH_ONLY', False):
        return HttpResponseRedirect(reverse("lfs_checkout"))

    # Using Djangos default AuthenticationForm
    login_form = AuthenticationForm()
    login_form.fields["username"].label = \
        getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
    login_form.fields["username"].widget = forms.TextInput(
        attrs={
            'placeholder':
            getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
        })
    register_form = RegisterForm(request=request)

    if request.POST.get("action") == "login":
        login_form = AuthenticationForm(data=request.POST)
        login_form.fields["username"].label = \
            getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
        login_form.fields["username"].widget = forms.TextInput(
            attrs={
                'placeholder':
                getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
            })

        if login_form.is_valid():
            from django.contrib.auth import login
            login(request, login_form.get_user())

            return set_message_cookie(
                reverse("lfs_checkout"),
                msg=_(u"You have been logged in."))
    elif request.POST.get("action") == "register":
        register_form = RegisterForm(data=request.POST)
        if register_form.is_valid():
            email = register_form.data.get("email")
            password = register_form.data.get("password_1")

            # Create user
            user = User.objects.create_user(
                username=email, email=email, password=password)

            # Notify
            customer_added.send(user)

            # Log in user
            from django.contrib.auth import authenticate
            user = authenticate(username=email, password=password)

            from django.contrib.auth import login
            login(request, user)

            return set_message_cookie(
                reverse("lfs_checkout"),
                msg=_(u"You have been registered and logged in."))

    return render_to_response(template_name, RequestContext(request, {
        "login_form": login_form,
        "register_form": register_form,
        'service_page': True,
        'page_type': 'checkout-login',
    }))


def checkout_dispatcher(request):
    """Dispatcher to display the correct checkout form
    """
    shop = get_default_shop(request)
    cart = get_cart(request)

    if cart is None or not cart.get_items():
        return empty_page_checkout(request)

    if request.user.is_authenticated() or \
       shop.checkout_type == CHECKOUT_TYPE_ANON:
        return HttpResponseRedirect(reverse("lfs_checkout"))
    else:
        return HttpResponseRedirect(reverse("lfs_checkout_login"))


def cart_inline(
        request,
        template_name="lfs/checkout/checkout_cart_inline.html",
        updating=False,
        readonly=False):
    """Displays the cart items of the checkout page.

    Factored out to be reusable for the starting request (which renders the
    whole checkout page and subsequent ajax requests which refresh the
    cart items.
    """
    cart = get_cart(request)

    if not cart:
        return ''

    if updating:
        # Update Amounts
        for item in cart.get_items():
            try:
                value = request.POST.get(
                    "amount-cart-item_%s" % item.id, "0.0")
                if isinstance(value, unicode):
                    value = value.encode("utf-8")
                amount = abs(locale.atof(value))
            except (TypeError, ValueError):
                amount = 1.0

            item.amount = amount

            if item.amount == 0:
                item.delete()
            else:
                item.save()
        cart_changed.send(cart, request=request)

    if cart.get_amount_of_items() == 0:
        return ''

    # Shipping
    selected_shipping_method = get_selected_shipping_method(request)
    shipping_costs = get_shipping_costs(request, selected_shipping_method)

    # Payment
    selected_payment_method = get_selected_payment_method(request)
    payment_costs = get_payment_costs(request, selected_payment_method)
    # ratio = payment_costs['ratio']

    # Cart costs
    cart_price = cart.get_price(request) + shipping_costs["price"] \
        + payment_costs["price"]/payment_costs['ratio']

    discounts = get_valid_discounts(request)
    for discount in discounts:
        cart_price = cart_price - discount["price"]
        # cart_tax = cart_tax - discount["tax"]

    # Voucher
    show_voucher = getattr(settings, 'CHECKOUT_SHOW_VOUCHER', True)
    voucher_group = None
    voucher_is_absolute = True
    try:
        voucher_number = get_current_voucher_number(request)
        voucher = Voucher.objects.get(number=voucher_number, active=True)
        voucher_group = voucher.group.name
        voucher_is_absolute = voucher.kind_of == ABSOLUTE
    except Voucher.DoesNotExist:
        if voucher_number:
            display_voucher = True
        else:
            display_voucher = False
        voucher_value = 0
        voucher_message = MESSAGES[6]
    else:
        set_current_voucher_number(request, voucher_number)
        is_voucher_effective, voucher_message = \
            voucher.is_effective(request, cart)
        if is_voucher_effective:
            display_voucher = True
            voucher_value = voucher.get_price(request, cart)
            cart_price = cart_price - voucher_value
        else:
            display_voucher = True
            voucher_value = 0

    cart_items = []
    for cart_item in cart.get_items():
        product = cart_item.product
        quantity = product.get_clean_quantity(cart_item.amount)
        cart_items.append({
            "obj": cart_item,
            "quantity": quantity,
            "product": product,
            "product_price": cart_item.get_price(request),
            "product_price_total": cart_item.get_price_total(request),
        })

    return render_to_string(template_name, RequestContext(request, {
        "cart": cart,
        "cart_items": cart_items,
        "cart_price": cart_price,
        "display_voucher": display_voucher,
        "discounts": discounts,
        "voucher_value": voucher_value,
        "shipping_price": shipping_costs["price"],
        "payment_price": payment_costs["price"]/payment_costs['ratio'],
        "selected_shipping_method": selected_shipping_method,
        "selected_payment_method": selected_payment_method,
        "show_voucher": show_voucher,
        "voucher_number": voucher_number,
        "voucher_message": voucher_message,
        "voucher_group": voucher_group,
        "voucher_is_absolute": voucher_is_absolute,
        "page_type": "cart",
        "cart_readonly": readonly,
    }))


def __has_show_address_select(request, shipping_method, payment_method):
    from ..fields.arrays import FieldsDataManager
    from ..customer.utils import get_or_create_customer
    shop = get_default_shop(request)
    customer = get_or_create_customer(request)

    address_fields = shop.address_form_fields.split(',')
    show_address_select = False

    d = FieldsDataManager(data=shipping_method.extra)
    not_binded_actions = d.get_not_binded_actions()
    for action in not_binded_actions:
        if action['type'] == 'write_variable':
            t = action['variable']
            if t[0] == '$':
                t = t[1:]
            if t in address_fields:
                show_address_select = True
                break

    if not show_address_select:
        d = FieldsDataManager(data=payment_method.extra)
        not_binded_actions = d.get_not_binded_actions()
        for action in not_binded_actions:
            if action['type'] == 'write_variable':
                t = action['variable']
                if t[0] == '$':
                    t = t[1:]
                if t in address_fields:
                    show_address_select = True
                    break

    if customer.addresses.count() <= 1:
        show_address_select = False

    return show_address_select


def one_page_checkout(
        request, checkout_form=OnePageCheckoutForm,
        template_name="lfs/checkout/one_page_checkout.html"):
    """One page checkout form.
    """
    # If the user is not authenticated and the if only authenticate checkout
    # allowed we redirect to authentication page.
    from ..customer.utils import get_or_create_customer
    shop = get_default_shop(request)
    cart = get_cart(request)
    if cart is None or not cart.get_items():
        return empty_page_checkout(request)

    if request.user.is_anonymous() and \
            getattr(settings, 'CHECKOUT_AUTH_ONLY', False):
        return HttpResponseRedirect(reverse("lfs_checkout_login"))

    customer = get_or_create_customer(request)
    address = customer.get_default_address()

    shipping_method = get_selected_shipping_method(request)
    payment_method = get_selected_payment_method(request)

    show_address_select = __has_show_address_select(
        request, shipping_method, payment_method)

    if request.method == "POST":
        form = checkout_form(
            data=request.POST,
            customer=customer,
            request=request)

        if shop.cart_require_accept_rules:
            if "cart_require_accept_rules" not in request.POST:
                if form.errors is None:
                    form.errors = {}
                form.errors["cart_require_accept_rules"] = \
                    _(u"Please confirm our terms and conditions")

        shipping_form = ShippingMethodForm(
            shipping_method=shipping_method,
            request=request,
            address=address,
            data=request.POST)

        payment_form = PaymentMethodForm(
            payment_method=payment_method,
            request=request,
            address=address,
            data=request.POST)

        if form.is_valid() and \
                shipping_form.is_valid() and payment_form.is_valid():
            customer_fields = shop.checkout_form_fields.split(',')
            customer_data = {}
            for f in customer_fields:
                customer_data.update({f: form.cleaned_data.get(f)})
            customer.set_info(customer_data)
            customer.save()

            order_fields = shop.order_form_fields.split(',')
            order_data = {}
            for f in order_fields:
                order_data.update({f: form.cleaned_data.get(f)})

            customer.selected_payment_method_id = \
                request.POST.get("payment_method")
            customer.selected_shipping_method_id = \
                request.POST.get("shipping_method")
            customer.save()
            result = process_payment(
                request,
                checkout_form=form,
                shipping_form=shipping_form,
                payment_form=payment_form)
            if result["accepted"]:
                return HttpResponseRedirect(
                    result.get("next_url", reverse("lfs_thank_you")))
            else:
                if "message" in result:
                    form._errors[result.get("message_location")] = \
                        result.get("message")

        else:  # form is not valid
            customer.selected_payment_method_id = \
                request.POST.get("payment_method")
            customer.selected_shipping_method_id = \
                request.POST.get("shipping_method")

            # Save the selected information to the customer
            try:
                customer.save()
            except Exception:
                logger.error(u'Cannot save customer when checkout.')

    else:
        initial = customer.get_info()
        form = checkout_form(
            customer=customer,
            request=request,
            initial=initial)

        shipping_form = ShippingMethodForm(
            shipping_method=shipping_method,
            request=request,
            address=address)
        payment_form = PaymentMethodForm(
            payment_method=payment_method,
            request=request,
            address=address)

    show_shipping = getattr(settings, 'CHECKOUT_SHOW_SHIPPING', True)
    if show_shipping:
        _shipping = shipping_inline(request)
    else:
        _shipping = None
    show_payment = getattr(settings, 'CHECKOUT_SHOW_PAYMENT', True)
    if show_payment:
        _payment = payment_inline(request)
    else:
        _payment = None

    show_voucher = getattr(settings, 'CHECKOUT_SHOW_VOUCHER', True)
    if show_voucher:
        voucher_number = get_current_voucher_number(request)
    else:
        voucher_number = None

    response = render_to_response(template_name, RequestContext(request, {
        'show_address_select': show_address_select,
        "form": form,
        "cart_inline": cart_inline(request),
        "show_comment": getattr(settings, 'CHECKOUT_SHOW_COMMENT', True),
        "show_shipping": show_shipping,
        "show_payment": show_payment,

        "shipping_inline": _shipping,
        "payment_inline": _payment,
        "show_voucher": show_voucher,
        "voucher_number": voucher_number,
        "settings": settings,
        "shipping_form": shipping_form,
        "payment_form": payment_form,
        'service_page': True,
        'page_type': 'cart',
    }))

    return response


def one_page_checkout_authorization(
        request, checkout_form=OnePageCheckoutForm,
        template_name="lfs/checkout/one_page_checkout_authorization.html"):
    """One page checkout authorization.
    """
    # If the user is not authenticated and the if only authenticate checkout
    # allowed we rediret to authentication page.
    if request.user.is_anonymous() and \
            getattr(settings, 'CHECKOUT_AUTH_ONLY', False):
        return HttpResponseRedirect(reverse("lfs_checkout_login"))

    login_form = AuthenticationForm()
    login_form.fields["username"].label = \
        getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
    login_form.fields["username"].widget = forms.TextInput(
        attrs={
            'placeholder':
            getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
        })

    if request.POST.get("action") == "login":
        login_form = AuthenticationForm(data=request.POST)
        login_form.fields["username"].label = \
            getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
        login_form.fields["username"].widget = forms.TextInput(
            attrs={
                'placeholder':
                getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
            })

        if login_form.is_valid():
            from django.contrib.auth import login
            login(request, login_form.get_user())

            return set_message_cookie(
                reverse("lfs_checkout"),
                msg=_(u"You have been logged in."))

    cart = get_cart(request)
    if cart is None:
        return HttpResponseRedirect(reverse('lfs_checkout'))

    response = render_to_response(template_name, RequestContext(request, {
        "login_form": login_form,
        "cart_inline": cart_inline(request, readonly=True),
        'service_page': True,
        "page_type": "cart-auth",
    }))

    return response


def empty_page_checkout(
        request, template_name="lfs/checkout/empty_page_checkout.html"):
    """
    """
    return render_to_response(template_name, RequestContext(request, {
        "shopping_url": reverse("lfs.core.views.shop_view"),
    }))


def thank_you(request, template_name="lfs/checkout/thank_you_page.html"):
    """Displays a thank you page ot the customer
    """
    order = request.session.get("order")
    pay_link = order.get_pay_link(request) if order else None
    return render_to_response(template_name, RequestContext(request, {
        "order": order,
        "pay_link": pay_link,
        'service_page': True,
        "page_type": "cart-thank-you",
        'thank_you_text': settings.CHECKOUT_THANK_YOU_TEXT
    }))


def payment_inline(
        request, template_name="lfs/checkout/payment_inline.html"):
    """Displays the selectable payment methods of the checkout page.

    Factored out to be reusable for the starting request (which renders the
    whole checkout page and subsequent ajax requests which refresh the
    selectable payment methods.

    Passing the form to be able to display payment forms within the several
    payment methods, e.g. credit card form.
    """
    default_payment_only = getattr(
        settings, 'CHECKOUT_USE_DEFAULT_PAYMENT', False)
    if default_payment_only:
        selected_payment_method = get_default_payment_method(request)
    else:
        selected_payment_method = get_selected_payment_method(request)
    valid_payment_methods = get_valid_payment_methods(request)

    return render_to_string(template_name, RequestContext(request, {
        "default_payment_only": default_payment_only,
        "payment_methods": valid_payment_methods,
        "selected_payment_method": selected_payment_method,
    }))


def shipping_inline(
        request, template_name="lfs/checkout/shipping_inline.html"):
    """Displays the selectable shipping methods of the checkout page.

    Factored out to be reusable for the starting request (which renders the
    whole checkout page and subsequent ajax requests which refresh the
    selectable shipping methods.
    """
    default_shipping_only = getattr(
        settings, 'CHECKOUT_USE_DEFAULT_SHIPPING', False)
    if default_shipping_only:
        selected_shipping_method = get_default_shipping_method(request)
    else:
        selected_shipping_method = get_selected_shipping_method(request)
    shipping_methods = get_valid_shipping_methods(request)

    return render_to_string(template_name, RequestContext(request, {
        "default_shipping_only": default_shipping_only,
        "shipping_methods": shipping_methods,
        "selected_shipping_method": selected_shipping_method,
    }))


def check_voucher(request):
    """
    """
    voucher_number = get_current_voucher_number(request)
    set_current_voucher_number(request, voucher_number)

    result = simplejson.dumps({
        "html": (("#cart-inline", cart_inline(request)),)
    })

    return HttpResponse(result)


def update_shipping_form(
        request,
        template_name='lfs/checkout/shipping_form.html'):

    shipping_method = get_selected_shipping_method(request)
    form = ShippingMethodForm(
        shipping_method=shipping_method,
        request=request)
    return render_to_response(template_name, RequestContext(request, {
        "shipping_form": form,
    }))


def update_payment_form(
        request,
        template_name='lfs/checkout/payment_form.html'):

    payment_method = get_selected_payment_method(request)
    form = PaymentMethodForm(
        payment_method=payment_method,
        request=request)
    return render_to_response(template_name, RequestContext(request, {
        "payment_form": form,
    }))


def update_customer_form(
        request,
        template_name='lfs/checkout/customer_form.html'):
    from ..customer.utils import get_or_create_customer
    customer = get_or_create_customer(request)
    form = OnePageCheckoutForm(
        customer=customer,
        request=request
    )
    return render_to_response(template_name, RequestContext(request, {
        "form": form,
    }))


def update_order_form(
        request,
        template_name='lfs/checkout/order_form.html'):
    from ..customer.utils import get_or_create_customer
    customer = get_or_create_customer(request)
    form = OnePageCheckoutForm(
        customer=customer,
        request=request
    )
    return render_to_response(template_name, RequestContext(request, {
        "form": form,
    }))


@require_http_methods(["POST"])
def changed_checkout(request):
    """
    """
    from ..customer.utils import get_or_create_customer
    from ..customer.models import Address

    customer = get_or_create_customer(request)
    _save_customer(request, customer)

    address_id = request.POST.get('address_choice', None)
    address = None
    if address_id is not None:
        try:
            address = Address.objects.get(pk=address_id)
        except Address.DoesNotExist:
            logger.error(u'Address with pk=%s does not found.' % address_id)

    shipping_method = get_selected_shipping_method(request)
    shipping_form = ShippingMethodForm(
        shipping_method=shipping_method,
        request=request,
        address=address)

    payment_method = get_selected_payment_method(request)
    payment_form = PaymentMethodForm(
        payment_method=payment_method,
        request=request,
        address=address)

    show_address_select = __has_show_address_select(
        request, shipping_method, payment_method)

    cart = cart_inline(request, updating=True)
    redirect_url = ''
    if cart == '':
        redirect_url = reverse("lfs_checkout_dispatcher")

    result = simplejson.dumps({
        'show_address_select': show_address_select,
        'shipping': shipping_inline(request),
        "payment": payment_inline(request),
        "cart": cart,
        'redirect_url': redirect_url,
        "shipping_inline":
        render_to_string(
            'lfs/checkout/shipping_form.html',
            {'shipping_form': shipping_form}),
        "payment_inline":
        render_to_string(
            'lfs/checkout/payment_form.html',
            {'payment_form': payment_form}),
    })

    return HttpResponse(result)


def _save_customer(request, customer):
    """
    """
    shipping_method = request.POST.get("shipping_method")
    customer.selected_shipping_method_id = shipping_method

    payment_method = request.POST.get("payment_method")
    customer.selected_payment_method_id = payment_method

    customer.save()

    update_to_valid_shipping_method(request, customer)
    update_to_valid_payment_method(request, customer)
    customer.save()
