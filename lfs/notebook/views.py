# coding: utf-8
import json

from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from ..caching.utils import lfs_get_object_or_404
from ..catalog.models import Product
from django.template.loader import render_to_string

from django.utils import simplejson

from portlets.models import PortletAssignment


def add_product_view(request, product_id, portlet_assignment_id=None):
    ''' Add product to notebook
    '''
    try:
        product = lfs_get_object_or_404(Product, pk=product_id)
    except Product.DoesNotExist:
        return HttpResponse(json.dumps('Error'), mimetype="application/json")

    if request.session.get('notebook', False):
        notebook_list = request.session['notebook']
        if product not in notebook_list:
            notebook_list.append(product)
            request.session['notebook'] = notebook_list
    else:
        notebook_list = []
        notebook_list.append(product)
        request.session['notebook'] = notebook_list

    if portlet_assignment_id is None:
        return HttpResponse(json.dumps('Ok'), mimetype="application/json")
    else:
        return ajax_notebook_render(request, portlet_assignment_id)


def ajax_notebook_render(request, portlet_assignment_id):
    obj = PortletAssignment.objects.get(pk=portlet_assignment_id)

    notebook_list = []
    if request.session.get('notebook', False):
        notebook_list = request.session['notebook']
        if notebook_list:
            notebook_list = notebook_list[:5]

    html = render_to_string(
        "lfs/portlets/notebook.html",
        RequestContext(
            request,
            {
                "title": obj.portlet.title,
                "notebook_list": notebook_list,
                'ajax': True,
                'slot_name': obj.slot.name,
            }
        )
    )
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)


def remove_product_view(request, product_id):
    ''' Remove product from notebook
    '''
    try:
        product = lfs_get_object_or_404(Product, pk=product_id)
    except Product.DoesNotExist:
        return HttpResponse(json.dumps('Error'), mimetype="application/json")

    if request.session.get('notebook', False):
        notebook_list = request.session['notebook']
        try:
            notebook_list.remove(product)
            request.session['notebook'] = notebook_list
            return redirect('/notebook/')
        except ValueError:
            pass

    return redirect('/notebook/')


def notebook_view(request, template_name='lfs/notebook/notebook.html'):
    ''' View current select products of notebook
    '''
    if request.session.get('notebook', False):
        notebook_list = request.session['notebook']
    else:
        notebook_list = []

    return render_to_response(template_name, RequestContext(request, {
        'notebook_list': notebook_list,
        'products_count': len(notebook_list),
    }))


def remove_all_products_view(request):
    if request.session.get('notebook', False):
        request.session['notebook'] = []
    return redirect('/')


def notebook_remove_product_ajax(request, portlet_assignment_id, product_id):
    ''' Remove product from comparison
        (Delete product id from user session)
    '''
    obj = PortletAssignment.objects.get(pk=portlet_assignment_id)
    if request.session.get('notebook', False):
        notebook_list = request.session['notebook']
        try:
            notebook_list.remove(Product.objects.get(id=product_id))
            request.session['notebook'] = notebook_list
        except ValueError:
            pass
        # products = []
        # for product_id in notebook_list:
        #     try:
        #         products.append(Product.objects.get(id=product_id))
        #     except Product.DoesNotExist:
        #         pass

        html = render_to_string(
            "lfs/portlets/notebook.html",
            RequestContext(
                request,
                {
                    'comparison_list_portlet': obj.portlet,
                    'notebook_list': notebook_list,
                    'ajax': True,
                    'slot_name': obj.slot.name,
                }
            )
        )
        response = simplejson.dumps({'html': html})
        return HttpResponse(response)


def remove_all_products_ajax(request, portlet_assignment_id):
    obj = PortletAssignment.objects.get(pk=portlet_assignment_id)
    if request.session.get('notebook', False):
        request.session['notebook'] = []
        html = render_to_string(
            "lfs/portlets/notebook.html",
            RequestContext(
                request,
                {
                    'comparison_list_portlet': obj.portlet,
                    'ajax': True,
                    'slot_name': obj.slot.name,
                }
            )
        )
        response = simplejson.dumps({'html': html})
        return HttpResponse(response)


def get_products_ajax(request):
    ids = []
    append_id = ids.append
    for p in request.session.get('notebook', []):
        append_id(p.id)
    return HttpResponse(simplejson.dumps({
        'products': ids}))
