# coding: utf-8
from django.conf import settings
from django.core.cache import cache


def invalidate_portlet_cache(slot_name, instance):
    cache_keys = cache.get('%s-lfs-portlet-slot-%s-%s-%s' % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX,
        slot_name, instance.__class__.__name__,
        instance.id), [])
    cache.delete_many(cache_keys)
