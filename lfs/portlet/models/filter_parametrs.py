# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string
from portlets.models import Portlet
from ...filters.models import Filter, FilterOption
from django.utils.translation import ugettext_lazy as _
from django.db.models import Max, Min
from django.http import Http404
from lfs.catalog.utils import filter_product
from lfs.catalog.models import Category
from hr_urls.utils import unpack_hr_urls, get_hr_urls
from urlparse import urlparse, parse_qs
from urllib import unquote


class FilterParametrsPortlet(Portlet):

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get("request")
        category = context.get("category")
        full_url = request.get_full_path()
        tmp = full_url.split('?')
        if not '?' in full_url:
            tmp = (full_url, '')
        slugs = tmp[0].split('/')
        category_slug = slugs[1]
        if slugs > 2:
            slugs = slugs[2:]
            try:
                category = Category.objects.get(slug=category_slug)
            except:
                raise Http404
            filters, additional = unpack_hr_urls(
                category, '/%s' % '/'.join(slugs))
            params = '&'.join((filters, tmp[1])).strip('&')
            full_url = '/' + category_slug
            if params:
                full_url += '?' + params
        filter_parametrs = []
        for filter_identificator, options_identificators\
                in request.GET.items():
            try:
                _filter = Filter.objects.get(
                    category=category, identificator=filter_identificator)
            except:
                continue
            if '..' in options_identificators:
                d = {}
                url = _filter.identificator + '=' + options_identificators
                d['remove_url'] = request.get_full_path().replace(
                    '?' + url, '').replace('&' + url, '')
                categories = [category]
                products = category.products.all()
                product_filter = dict(request.GET)
                res = filter_product(categories, products, product_filter).aggregate(
                    Min('effective_price'), Max('effective_price'))
                d['bound'] = (
                    res['effective_price__min'], res['effective_price__max'])
                d['title'] = _filter.displayed_title.lower()
                filter_parametrs += [d]
            else:
                options = FilterOption.objects.filter(
                    filter=_filter,
                    identificator__in=options_identificators.split(','))

                for option in options:
                    params = parse_qs(urlparse(full_url).query)
                    params[_filter.identificator] = [
                        params[_filter.identificator][0].replace(
                            option.identificator, '')
                    ]
                    remove_url = ''
                    for f, o in params.items():
                        o = o[0]
                        if o:
                            o = o.replace(',,', ',').strip(',')
                            remove_url += u'%s=%s&' % (f, o)
                    remove_url = unquote(get_hr_urls(category, remove_url.strip('&'))).rstrip('/')
                    option.remove_url='/%s%s' % (category_slug, remove_url)

                filter_parametrs += list(options)
        return render_to_string(
            "lfs/portlets/filter_parametrs.html",
            RequestContext(request, {
                "title": self.title,
                "slot_name": context.get("slot_name"),
                "products_count": context.get('products_count'),
                "filter_parametrs": filter_parametrs,
                "reset_all_filters": '/%s' % category.slug,
            }))

    def form(self, **kwargs):
        return FilterParametrsPortletForm(instance=self, **kwargs)


class FilterParametrsPortletForm(forms.ModelForm):

    class Meta:
        model=FilterParametrsPortlet
