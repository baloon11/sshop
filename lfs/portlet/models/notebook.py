# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string
from django.contrib.contenttypes.models import ContentType

from portlets.models import Portlet, PortletAssignment


class NotebookPortlet(Portlet):
    """Portlet to display the notebook.
    """
    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        # notebook_list = []
        request = context.get("request")
        # if request.session.get('notebook', False):
        #     notebook_list = request.session['notebook']
        #     if notebook_list:
        #         notebook_list = notebook_list[:5]

        pa = PortletAssignment.objects.get(
            portlet_id=self.id,
            portlet_type=ContentType.objects.get_for_model(self)
        )

        return render_to_string(
            "lfs/portlets/notebook.html",
            RequestContext(
                request,
                {
                    # "title": self.title,
                    # "notebook_list": notebook_list,
                    'slot_name': context.get('slot_name'),
                    'portlet_assignment_id': pa.id,
                    'init': True,
                }
            )
        )

    def form(self, **kwargs):
        return NotebookPortletForm(instance=self, **kwargs)


class NotebookPortletForm(forms.ModelForm):
    """Form for NotebookPortlet.
    """
    class Meta:
        model = NotebookPortlet
