# coding: utf-8
from django import forms
from django.db import models
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from portlets.models import Portlet
from django.core.paginator import Paginator, PageNotAnInteger

from ...catalog.models import Category
from ...marketing.models import FeaturedProduct
from portlets.models import Slot


class FeaturedPortlet(Portlet):
    """A portlet for displaying featured products.
    """
    class Meta:
        app_label = 'portlet'

    name = _("Featured products")

    limit = models.IntegerField(_(u"Limit"), default=5)
    current_category = models.BooleanField(
        _(u"Use current category"), default=False)
    slideshow = models.BooleanField(_(u"Slideshow"), default=False)

    @property
    def rendered_title(self):
        return self.title or self.name

    def render(self, context, page=1):
        """Renders the portlet as html.
        """
        slot = Slot.objects.get(name=context.get('slot_name'))
        request = context.get("request")

        if self.current_category:
            obj = context.get("category") or context.get("product")
            if obj:
                if isinstance(obj, Category):
                    category = obj
                else:
                    category = obj.get_current_category(request)
                categories = [category]
                categories.extend(category.get_all_children())
                filters = {"product__categories__in": categories}
                products = [
                    x.product
                    for x in FeaturedProduct.objects.filter(**filters)]
            else:
                products = None
        else:
            products = [x.product for x in FeaturedProduct.objects.all()]

        paginator = Paginator(products, self.limit)
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)

        return render_to_string(
            "lfs/portlets/featured.html",
            RequestContext(request, {
                "title": self.rendered_title,
                "slideshow": self.slideshow,
                "products": products,
                "slot": slot,
                "portlet_id": self.id,
                "MEDIA_URL": context.get("MEDIA_URL"),
            }))

    def form(self, **kwargs):
        """
        """
        return FeaturedForm(instance=self, **kwargs)

    def __unicode__(self):
        return "%s" % self.id


class FeaturedForm(forms.ModelForm):
    """
    """
    class Meta:
        model = FeaturedPortlet
