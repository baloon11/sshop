from django import forms
from django.utils.translation import ugettext_lazy as _
from adminconfig.utils import BaseConfig


class PortletsForm(forms.Form):
    show_errors = forms.BooleanField(
        label=_(u'Show portlets errors.'),
        required=False
        )


class PortletsConfig(BaseConfig):
    form_class = PortletsForm
    block_name = 'portlet_errors'

    def __init__(self):
        super(PortletsConfig, self).__init__()

        self.default_data = {
            'PORTLET_SHOW_ERRORS': False,
        }

        self.option_translation_table = (
            ('PORTLET_SHOW_ERRORS', 'show_errors'),
        )
