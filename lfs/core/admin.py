# coding: utf-8
from django.utils.translation import ugettext as _
from django.contrib import admin
from treeadmin.admin import TreeAdmin
from django_singleton_admin.admin import SingletonAdmin

from .models import (
    Action,
    ActionGroup,
    Shop,
)
from .forms import ShopForm


class ShopAdmin(SingletonAdmin):
    list_display = ('name', 'shop_owner', 'default_currency')
    fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['name', 'shop_owner']
        }),
        (_(u'Email'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['from_email', 'notification_emails']
        }),
        (_(u'Description'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['description', 'image']
        }),
        (_(u'Google Analytics'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'google_analytics_id',
                'ga_site_tracking',
                'ga_ecommerce_tracking']
        }),

        (None, {
            'classes': ('suit-tab suit-tab-defaults',),
            'fields': ['product_cols', 'product_rows', 'category_cols']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-defaults',),
            'fields': ['price_calculator']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-defaults',),
            'fields': ['default_currency']
        }),

        (None, {
            'classes': ('suit-tab suit-tab-seo',),
            'fields': [
                'meta_title',
                'meta_keywords',
                'meta_description',
                'meta_seo_text']
        }),

        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['menu_type', 'max_menu_item_count', 'prerendered_menu']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['featured_badge', 'topseller_badge']
        }),
        (_(u'Search'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['default_search', 'search_field_text']
        }),
        (_(u'Watermark'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'watermark_image',
                'watermark_opacity',
                'watermark_position']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-cart',),
            'fields': [
                'checkout_form_fields',
                'ask_email_when_checkout',
                'ask_phone_when_checkout',
                'order_form_fields',
            ]
        }),
        (None, {
            'classes': ('suit-tab suit-tab-cart',),
            'fields': [
                'show_regular_customer_tab',
                'make_bg_registration',
                'allow_previous_data',
                ('cart_require_accept_rules', 'cart_link_to_rules'),
            ]
        }),
        (None, {
            'classes': ('suit-tab suit-tab-fields',),
            'fields': [
                'extra',
            ],
        }),
        (_(u'Addresses'), {
            'classes': ('suit-tab suit-tab-fields',),
            'fields': [
                'address_form_fields',
                'template_of_address',
            ],
        }),
        (_(u'Registration'), {
            'classes': ('suit-tab suit-tab-fields',),
            'fields': [
                'registration_form_fields',
                'ask_email_when_register',
                'ask_phone_when_register',
                ('require_accept_rules', 'link_to_rules'),
                'template_of_discount_code',
                'template_msg_of_registration',
            ],
        }),
    ]
    suit_form_tabs = (
        ('general', _(u'General')),
        ('defaults', _(u'Default values')),
        ('seo', _(u'SEO')),
        ('fields', _(u'Customer data')),
        ('cart', _(u'Cart')),
        ('portlets', _(u'Portlets')),
    )
    suit_form_includes = (
        ('admin/portlets_includes/portlets.html', 'top', 'portlets'),
        ('admin/fields_includes/fields.html', 'top', 'fields'),
    )
    actions = ['prerender_menu']

    form = ShopForm

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def prerender_menu(self, request, queryset):
        for q in queryset:
            q.prerender_menu()
    prerender_menu.short_description = _(u'Prerender main menu')

admin.site.register(Shop, ShopAdmin)


class ActionGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'type')
    search_fields = ['name']


class ActionAdmin(TreeAdmin):
    list_display = ('title', 'link', 'group', 'active',)
    search_fields = ['title']
    sortable = 'position'
    list_filters = ['group']


admin.site.register(Action, ActionAdmin)
admin.site.register(ActionGroup, ActionGroupAdmin)
