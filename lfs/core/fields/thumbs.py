# coding: utf-8

# python imports
try:
    import Image
except ImportError:
    from PIL import Image
try:
    import ImageEnhance
except ImportError:
    from PIL import ImageEnhance

from wand.image import Image as ImageMagick_Image
import cStringIO
import logging

# django imports
from django.core.files.base import ContentFile
from django.db.models import ImageField
from django.db.models.fields.files import ImageFieldFile
from django.conf import settings

# sshop import
from lfs.catalog.settings import sizes as SSHOP_IMAGE_SIZES
from lfs.catalog.settings import watermarked as SSHOP_IMAGE_WATERMARKED
from lfs.catalog.settings import watermarked_zoom as SSHOP_WATERMARKED_ZOOM


logger = logging.getLogger('sshop')


class GraphicProcessorException(Exception):
    pass


def generate_thumb(img, thumb_size, format, current_size, watermark=False):
    """
    Generates a thumbnail image and returns a ContentFile object with
    the thumbnail

    Args:
        img: File object
        thumb_size:  Desired thumbnail size, ie: (200,120)
        format: format of the original image ('jpeg','gif','png',...)
                (this format will be used for the generated thumbnail, too)
        current_size: text label of current size ('small', 'medium', 'large' or 'huge')
        watermark: bool variable, it indicates set watermark on current thumb

    Returns:
        ContentFile object with the thumbnail

    Raises:
        GraphicProcessorException('Use unknown graphic processor')
    """
    from lfs.core.utils import get_default_shop

    GRAPHIC_PROCESSOR = getattr(settings, 'GRAPHIC_PROCESSOR', 'PIL')
    thumb_width, thumb_height = thumb_size

    # check, if watermark image don't added
    shop = get_default_shop()
    if shop.watermark_image:
        opacity = shop.watermark_opacity
        position = shop.watermark_position
    else:
        watermark = False

    if GRAPHIC_PROCESSOR == 'PIL':
        img.seek(0)
        image = Image.open(img)

        # Convert to RGB if necessary
        if image.mode not in ('L', 'RGB'):
            image = image.convert('RGB')

        width, height = image.size
        new_width, new_height = scale_to_max_size(
            width,
            height,
            thumb_width,
            thumb_height)

        # get new resized image
        new_image = image.resize((new_width, new_height), Image.ANTIALIAS)

        if watermark:
            # this horrible code was found in google and it work
            # it's strange PIL
            wm = Image.open(shop.watermark_image.file)

            if wm.mode != 'RGBA':
                wm = wm.convert('RGBA')
            else:
                wm = wm.copy()
            alpha = wm.split()[3]
            alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
            wm.putalpha(alpha)

            new_wm_width = int(wm.size[0] * SSHOP_WATERMARKED_ZOOM[current_size])
            new_wm_height = int(wm.size[1] * SSHOP_WATERMARKED_ZOOM[current_size])
            wm = wm.resize((new_wm_width, new_wm_height), Image.ANTIALIAS)

            img_width, img_height = new_image.size
            wm_width, wm_height = wm.size

            wm_x, wm_y = watermark_position(
                position,
                img_width,
                img_height,
                wm_width,
                wm_height,
                SSHOP_WATERMARKED_ZOOM[current_size])

            if new_image.mode != 'RGBA':
                new_image = new_image.convert('RGBA')

            layer = Image.new('RGBA', new_image.size, (0, 0, 0, 0))
            layer.paste(wm, (wm_x, wm_y))
            new_image = Image.composite(layer,  new_image,  layer)

        io = cStringIO.StringIO()

        # PNG and GIF are the same, JPG is JPEG
        if format.upper() == 'JPG':
            format = 'JPEG'

        new_image.save(io, format)
        return ContentFile(io.getvalue())

    elif GRAPHIC_PROCESSOR == 'ImageMagick':
        img.seek(0)
        image = ImageMagick_Image(blob=img)

        new_width, new_height = scale_to_max_size(
            image.width,
            image.height,
            thumb_width,
            thumb_height)

        image.resize(new_width, new_height)

        if watermark:
            wm = ImageMagick_Image(file=shop.watermark_image.file)

            new_wm_width = int(wm.width * SSHOP_WATERMARKED_ZOOM[current_size])
            new_wm_height = int(wm.height * SSHOP_WATERMARKED_ZOOM[current_size])

            wm.resize(new_wm_width, new_wm_height)

            # ATTENTION!!!
            # In shop object stored opacity of watermark. PIL use `opacity`
            # and it's work good, but ImageMagick use `transparency`.
            # That's why I set next line
            transparency = 1 - opacity

            wm_x, wm_y = watermark_position(
                position,
                image.width,
                image.height,
                wm.width,
                wm.height,
                SSHOP_WATERMARKED_ZOOM[current_size])

            image.watermark(wm, transparency, wm_x, wm_y)

        io = cStringIO.StringIO()

        image.save(file=io)
        return ContentFile(io.getvalue())
    else:
        raise GraphicProcessorException('Use unknown graphic processor')


class ImageWithThumbsFieldFile(ImageFieldFile):
    """
    See ImageWithThumbsField for usage example
    """
    def __init__(self, *args, **kwargs):
        super(ImageWithThumbsFieldFile, self).__init__(*args, **kwargs)
        self.sizes = self.field.sizes
        self.watermark = self.field.watermark

        if self.sizes:
            def get_size(self, size):
                if not self:
                    return ''
                else:
                    split = self.url.rsplit('.', 1)
                    thumb_url = '%s.%sx%s.%s' % (split[0], w, h, split[1])
                    return thumb_url

            for size in self.sizes:
                (w, h) = size
                setattr(self, 'url_%sx%s' % (w, h), get_size(self, size))

    def save(self, name, content, save=True):
        super(ImageWithThumbsFieldFile, self).save(name, content, save)
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                file_name, format = self.name.rsplit('.', 1)
                thumb_name = '%s.%sx%s.%s' % (file_name, w, h, format)

                # add watermark only product image
                if self.watermark:
                    # check, if need set watermark on current thumb
                    image_sizes = SSHOP_IMAGE_SIZES.items()
                    current_size = filter(lambda x: size in x, image_sizes)

                    if len(current_size) > 1:
                        logger.warning('ImageField: incorrect SSHOP_IMAGE_SIZES, some size repeated.')

                    current_size = current_size[0][0]
                    is_watermark = SSHOP_IMAGE_WATERMARKED[current_size]
                else:
                    image_sizes = SSHOP_IMAGE_SIZES.items()
                    current_size = filter(lambda x: size in x, image_sizes)

                    if len(current_size) > 1:
                        logger.warning('ImageField: incorrect SSHOP_IMAGE_SIZES, some size repeated.')

                    current_size = current_size[0][0]
                    is_watermark = False

                thumb_content = generate_thumb(
                    self.file,
                    size,
                    format,
                    current_size,
                    is_watermark)

                thumb_name_ = self.storage.save(thumb_name, thumb_content)

                if not thumb_name == thumb_name_:
                    raise ValueError(
                        'There is already a file named %s' % thumb_name)

    def delete(self, save=True):
        name = self.name
        super(ImageWithThumbsFieldFile, self).delete(save)
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = name.rsplit('.', 1)
                thumb_name = '%s.%sx%s.%s' % (split[0], w, h, split[1])
                try:
                    self.storage.delete(thumb_name)
                except:
                    pass

    def get_original_image_url(self):
        ''' Return url of original image
            that was uploaded
        '''
        return self.url

    def __get_image_by_type(self, image_type):
        ''' Return url for different types of images.
            `image_type`: can be small, medium, large or huge.
        '''
        attr_name = 'url_%sx%s' % SSHOP_IMAGE_SIZES[image_type]
        url = getattr(self, attr_name)
        return url

    def url_small(self):
        ''' Return url for small image
        '''
        return self.__get_image_by_type('small')

    def url_medium(self):
        ''' Return url for medium image
        '''
        return self.__get_image_by_type('medium')

    def url_large(self):
        ''' Return url for large image
        '''
        return self.__get_image_by_type('large')

    def url_huge(self):
        ''' Return url for huge image
        '''
        return self.__get_image_by_type('huge')

    def resave(self):
        '''
        Resave the image with new image sizes and new watermark position,
        but not change self.sizes
        '''
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = self.name.rsplit('.', 1)
                thumb_name = '%s.%sx%s.%s' % (split[0], w, h, split[1])
                try:
                    self.storage.delete(thumb_name)
                except:
                    pass

        self.field.sizes = self.sizes = tuple(SSHOP_IMAGE_SIZES.values())
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                file_name, format = self.name.rsplit('.', 1)
                thumb_name = '%s.%sx%s.%s' % (file_name, w, h, format)

                # add watermark only product image
                if self.watermark:
                    # check, if need set watermark on current thumb
                    image_sizes = SSHOP_IMAGE_SIZES.items()
                    current_size = filter(lambda x: size in x, image_sizes)

                    current_size = current_size[0][0]
                    is_watermark = SSHOP_IMAGE_WATERMARKED[current_size]
                else:
                    image_sizes = SSHOP_IMAGE_SIZES.items()
                    current_size = filter(lambda x: size in x, image_sizes)

                    current_size = current_size[0][0]
                    is_watermark = False

                thumb_content = generate_thumb(
                    self.file,
                    size,
                    format,
                    current_size,
                    is_watermark)

                self.storage.save(thumb_name, thumb_content)
        self.instance.save()


class ImageWithThumbsField(ImageField):
    attr_class = ImageWithThumbsFieldFile
    """
    TODO: rewrite doc!!!

    Usage example:
    ==============
    photo = ImageWithThumbsField(
        upload_to='images', sizes=((125,125),(300,200),)

    To retrieve image URL, exactly the same way as with ImageField:
        my_object.photo.url
    To retrieve thumbnails URL's just add the size to it:
        my_object.photo.url_125x125
        my_object.photo.url_300x200

    Note: The 'sizes' attribute is not required. If you don't provide it,
    ImageWithThumbsField will act as a normal ImageField

    How it works:
    =============
    For each size in the 'sizes' atribute of the field it generates a
    thumbnail with that size and stores it following this format:

    available_filename.[width]x[height].extension

    Where 'available_filename' is the available filename
    returned by the storage
    backend for saving the original file.

    Following the usage example above: For storing a file
    called "photo.jpg" it saves:
    photo.jpg          (original file)
    photo.125x125.jpg  (first thumbnail)
    photo.300x200.jpg  (second thumbnail)

    With the default storage backend if photo.jpg already exists it
    will use these filenames:
    photo_.jpg
    photo_.125x125.jpg
    photo_.300x200.jpg

    Note: django-thumbs assumes that if filename "any_filename.jpg"
    is available filenames with this format
    "any_filename.[widht]x[height].jpg" will be available, too.

    To do:
    ======
    Add method to regenerate thubmnails

    """
    def __init__(
            self, verbose_name=None,
            name=None,
            width_field=None,
            height_field=None,
            sizes=None,
            watermark=False,
            **kwargs):

        super(ImageWithThumbsField, self).__init__(verbose_name=verbose_name,
                                                   name=name,
                                                   width_field=width_field,
                                                   height_field=height_field,
                                                   **kwargs)
        self.sizes = sizes
        self.watermark = watermark


def scale_to_max_size(img_width, img_height, thumb_width, thumb_height):

    if img_width <= thumb_width and img_height <= thumb_height:
        return img_width, img_height
    else:
        # resize proportinal
        prop_x = float(thumb_width) / img_width
        prop_y = float(thumb_height) / img_height

        if prop_y < prop_x:
            width = int(prop_y * img_width)
            return width, thumb_height
        else:
            height = int(prop_x * img_height)
            return thumb_width, height


def watermark_position(position, image_w, image_h, wm_w, wm_h, delta_x_coefficient):
    ''' Calculation watermark position in image.
        Return (x, y) - coordinate, where watermark must be placed (left top corner).
        Args:
            * position - place of watermark choiced in admin
            * image_w  - image width
            * image_h  - image height
            * wm_w     - watermark width
            * wm_h     - watermark height
            * delta_x_coefficient - coefficient of scaling delta x
    '''

    if position == 1:
        # place: top left
        x, y = 0, 0

    elif position == 2:
        # place: top right
        x = image_w - wm_w
        y = 0

    elif position == 3:
        # place: bottom left
        x = 0
        y = image_h - wm_h

    elif position == 4:
        # place: bottom right
        x = image_w - wm_w
        y = image_h - wm_h

    elif position == 5:
        # place: center
        x = image_w / 2 - wm_w / 2
        y = image_h / 2 - wm_h / 2

    elif position == 6:
        # place: top center
        x = image_w / 2 - wm_w / 2
        y = 0

    elif position == 7:
        # place: bottom center
        x = image_w / 2 - wm_w / 2
        y = image_h - wm_h

    elif position == 8:
        # place: right center
        x = image_w - wm_w
        y = image_h / 2 - wm_h / 2

    elif position == 9:
        # place: left center
        x = 0
        y = image_h / 2 - wm_h / 2

    x += int(delta_x_coefficient * settings.WATERMARK_X_DELTA)
    y += int(delta_x_coefficient * settings.WATERMARK_Y_DELTA)

    return x, y
