# coding: utf-8
import logging
from django.core.handlers.base import BaseHandler
from django.test.client import RequestFactory


# I do not sure that these two classes is really necessary.
# We use they for running all LFS' tests. But for new tests using
# the RequestFactory and SessionStore is more recommended.

class SessionMock(object):
    """
    """
    session_key = "42"


class RequestMock(RequestFactory):
    def request(self, **request):
        "Construct a generic request object."
        request = RequestFactory.request(self, **request)
        handler = BaseHandler()
        handler.load_middleware()
        for middleware_method in handler._request_middleware:
            if middleware_method(request):
                raise Exception("Couldn't create request mock object - "
                                "request middleware returned a response")
        return request
