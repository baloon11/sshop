# coding: utf-8
from django.conf import settings
from django import template
from lfs.core.utils import import_symbol
# from lfs.catalog.seo import CategorySEOGenerator
register = template.Library()


@register.simple_tag(takes_context=True)
def get_h1(context):
    request = context.get("request")
    seo_generators = getattr(settings, 'SEO_DATA_GENERATORS')
    seo_generators = sorted(
        seo_generators,
        key=lambda tup: tup[1], reverse=True)
    h1 = None
    for generator_url in seo_generators:
        generator = import_symbol(generator_url[0])
        obj = generator(request, context)
        h1 = obj.get_h1()
        if h1:
            break
    if h1:
        return h1
    else:
        return ''


@register.simple_tag(takes_context=True)
def get_title(context):
    request = context.get("request")
    seo_generators = getattr(settings, 'SEO_DATA_GENERATORS')
    seo_generators = sorted(
        seo_generators,
        key=lambda tup: tup[1], reverse=True)
    title = None
    for generator_url in seo_generators:
        generator = import_symbol(generator_url[0])
        obj = generator(request, context)
        title = obj.get_title()
        if title:
            break
    if title:
        return title
    else:
        return ''


@register.simple_tag(takes_context=True)
def get_description(context):
    request = context.get("request")
    seo_generators = getattr(settings, 'SEO_DATA_GENERATORS')
    seo_generators = sorted(
        seo_generators,
        key=lambda tup: tup[1], reverse=True)
    description = None
    for generator_url in seo_generators:
        generator = import_symbol(generator_url[0])
        obj = generator(request, context)
        description = obj.get_description()
        if description:
            break
    if description:
        return description
    else:
        return ''


@register.simple_tag(takes_context=True)
def get_keywords(context):
    request = context.get("request")
    seo_generators = getattr(settings, 'SEO_DATA_GENERATORS')
    seo_generators = sorted(
        seo_generators,
        key=lambda tup: tup[1], reverse=True)
    keywords = None
    for generator_url in seo_generators:
        generator = import_symbol(generator_url[0])
        obj = generator(request, context)
        keywords = obj.get_keywords()
        if keywords:
            break
    if keywords:
        return keywords
    else:
        return ''


@register.simple_tag(takes_context=True)
def get_seo_text_new(context):
    request = context.get("request")
    seo_generators = getattr(settings, 'SEO_DATA_GENERATORS')
    seo_generators = sorted(
        seo_generators,
        key=lambda tup: tup[1], reverse=True)
    seo_text = None
    for generator_url in seo_generators:
        generator = import_symbol(generator_url[0])
        obj = generator(request, context)
        seo_text = obj.get_seo_text()
        if seo_text:
            break
    if seo_text:
        return seo_text
    else:
        return ''
