# coding: utf-8

import random

from django.utils.translation import ugettext as _
from django.utils.translation import pgettext
from django import template
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.template.loader import render_to_string
from django.template import RequestContext

import lfs
import pytils
from lfs.discounts.models import Discount
from lfs.voucher.models import VoucherGroup
from lfs.marketing.models import ProductList
from lfs.shipping.models import ShippingMethod
from lfs.payment.models import PaymentMethod
from lfs.catalog.models import Category
from lfs.catalog.models import Property
from lfs.catalog.models import StaticBlock
from lfs.page.models import Page
from lfs.payment.models import PaymentMethod
from lfs.filters.models import Filter
from lfs.filters.models import FilterOption
from lfs.marketing.models import Topseller, FeaturedProduct
from sshop_currencies.models import Currency
from lfs.catalog.models import ProductStatus

register = template.Library()


def format_currency(request, value):
    if not value:
        value = 0.0

    cache_key = "%s-default-currency" %\
        (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX,
        )
    currency = cache.get(cache_key)
    if not currency:
        shop = lfs.core.utils.get_default_shop(request)
        currency = shop.default_currency
        cache.set(cache_key, currency)

    try:
        result = currency.get_formatted_value(value)
    except ValueError:
        result = value
    return result


def format_payment_currency(request, value, payment):
    if not value:
        value = 0.0

    currency = payment.currency
    if not currency:
        shop = lfs.core.utils.get_default_shop(request)
        currency = shop.default_currency

    value1 = value * currency.coeffitient
    try:
        result = currency.get_formatted_value(value1)
    except ValueError:
        result = value1
    return result


@register.simple_tag(takes_context=True)
def badges_for_product(context, product):
    cache_key = "%s-product-%s-badges" %\
        (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX,
            product.id
        )
    badges = cache.get(cache_key)
    if badges:
        return badges

    pos = 0
    badges = []
    shop = lfs.core.utils.get_default_shop()
    if settings.COMPATIBILITY_USE_TOPSELLER_AND_FEATURED:
        if FeaturedProduct.objects.filter(product=product).count():
            image = shop.featured_badge
            if image:
                badges.append((image, pos))
                pos += image.width

        if Topseller.objects.filter(product=product).count():
            image = shop.topseller_badge
            if image:
                badges.append((image, pos))
                pos += image.width

    for l in product.get_product_lists():
        try:
            if l.get_image().image:
                badges.append((l.get_image().image, pos))
                pos += l.get_image().image.width
        except:
            pass

    request = context.get('request')
    return render_to_string(
        "lfs/catalog/badges.html",
        RequestContext(request, {
            'badges': badges,
        }))

    # cache.set(cache_key, result)
    # return result


@register.simple_tag(takes_context=True)
def price(context, product):
    request = context['request']
    price = product.get_price(request)
    price_str = format_currency(request, price)
    return '''%s<span class="__discover--price"'
    ' data-product-id="%s"'
    ' data-product-type="price"></span>''' % (price_str, product.id)


@register.simple_tag(takes_context=True)
def standard_price(context, product):
    request = context['request']
    price = product.get_standard_price(request)
    price_str = format_currency(request, price)
    return '''%s<span class="__discover--price"'
    ' data-product-id="%s"'
    ' data-product-type="standard_price"></span>''' % (price_str, product.id)


@register.simple_tag(takes_context=True)
def for_sale_price(context, product):
    request = context['request']
    price = product.get_for_sale_price(request)
    price_str = format_currency(request, price)
    return '''%s<span class="__discover--price"'
    ' data-product-id="%s"'
    ' data-product-type="for_sale_price"></span>''' % (price_str, product.id)


@register.simple_tag(takes_context=True)
def price_difference(context, product):
    request = context['request']
    price = product.get_standard_price(request) - \
        product.get_for_sale_price(request)
    price_str = format_currency(request, price)
    return '''%s<span class="__discover--price"'
    ' data-product-id="%s"'
    ' data-product-type="price_difference"></span>''' % (price_str, product.id)


@register.simple_tag(takes_context=True)
def format_as_price(context, value):
    try:
        request = context['request']
    except:
        request = None
    price_str = format_currency(request, value)
    return price_str


@register.simple_tag(takes_context=True)
def cheapest_price(context, product, use_price_calculator=False):
    request = context['request']
    if settings.COMPATIBILITY_CHEAPEST_PRICE_DEPENDS_SORTING:
        sortable_fields = context.get('sortable_fields')
        if sortable_fields is not None:
            if '-max_variant_price' in sortable_fields:
                # Reverse order sorting
                price = product.get_most_expensive_price(
                    request, use_price_calculator)
                price_str = '%s %s' % (
                    pgettext('Parent product price prefix', u'to')
                    if price['rising_to'] else '',
                    format_currency(request, price['price']),
                )
                return '''%s<span class="__discover--price"'
                ' data-product-id="%s"'
                ' data-product-type="cheapest_price"></span>''' % (
                    price_str, product.id)

    price = product.get_cheapest_price(request, use_price_calculator)
    price_str = '%s %s' % (
        pgettext('Parent product price prefix', u'from')
        if price['starting_from'] else '',
        format_currency(request, price['price']),
    )
    return '''%s<span class="__discover--price"'
    ' data-product-id="%s"'
    ' data-product-type="cheapest_price"></span>''' % (price_str, product.id)


@register.simple_tag(takes_context=True)
def most_expensive_price(context, product):
    request = context['request']
    price = product.get_most_expensive_price(request)
    price_str = '%s %s' % (
        pgettext('Parent product price prefix', u'to')
        if price['rising_to'] else '',
        format_currency(request, price['price']),
    )
    return '''%s<span class="__discover--price"'
    ' data-product-id="%s"'
    ' data-product-type="most_expensive_price"></span>''' % (
        price_str, product.id)


@register.simple_tag(takes_context=True)
def format_as_payment_price(context, value, payment_name):
    try:
        request = context['request']
    except:
        request = None
    price_str = format_payment_currency(request, value, payment_name)
    return price_str


@register.simple_tag(takes_context=True)
def cart_item_price(context, cart_item):
    request = context['request']
    price = cart_item.get_price(request)
    price_str = format_currency(request, price)
    return price_str


@register.simple_tag(takes_context=True)
def cart_item_price_total(context, cart_item):
    request = context['request']
    price = cart_item.get_price_total(request)
    price_str = format_currency(request, price)
    return price_str


@register.filter(name='negate')
def negate(value):
    return -float(value)


@register.filter(name='lookup')
def lookup(value, index):
    return value.get(index, '')


#----------------

@register.simple_tag(takes_context=True)
def static_block(context, name):
    request = context['request']
    html = None
    su_html_message = u"\
       <div class='alert alert-danger' role='alert'>\
       Статический блок \"%s\" не существует!\
       </div>\
       " % name

    cache_key = "%s-statick-block-%s-%s" %\
        (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX,
            pytils.translit.slugify(name),
            request.user.is_superuser,
        )

    html = cache.get(cache_key)

    if html is not None:
        return html
 
    try:
        html = StaticBlock.objects.get(name=name).html
        cache.set(cache_key, html)
    except Exception:
        if request.user.is_authenticated() and request.user.is_superuser:
            html = su_html_message
        else:
            html = ""
    return html


@register.filter(name='clean_block')
def clean_block(value, request):  # WTF?
    if request.GET:
        return ''
    else:
        return value


@register.filter(name='remade_title')
def remade_title(title, request):
    if request.GET:
        get_vars = request.GET

        if get_vars.get('start', False):
            str_page = _(u'Страница') + ' ' + get_vars.get('start') + '. '
        else:
            str_page = ''

        options = []
        for k, v in get_vars.items():
            try:
                if k[0] == 'f':
                    options.append(FilterOption.objects.get(id=v))
            except:
                pass

        options_str = ' '
        if options:
            if len(options) > 3:
                temp = []
                for i in xrange(3):
                    t = random.choice(options)
                    temp.append(t.title)
                    options.remove(t)

                options_str += ', '.join(temp)
            else:
                options = [i.title for i in options]
                options_str += ', '.join(options)

        return str_page + title + options_str
    else:
        return title


@register.simple_tag(takes_context=True)
def show_product_id(context, product):
    if getattr(settings, 'SSHOP_USE_1C', False):
        request = context.get('request')
        return render_to_string(
            "lfs/catalog/products/product_code_with_1c.html",
            RequestContext(request, {
            'id': product.id_1c,
            }))
    else:
        request = context.get('request')
        return render_to_string(
            "lfs/catalog/products/product_code_without_1c.html",
            RequestContext(request, {
            'id': product.id,
            }))


@register.simple_tag(takes_context=True)
def load_customer(context):
    from lfs.customer.utils import get_customer
    request = context['request']
    if request.user.is_authenticated():
        customer = get_customer(request)
        context['customer'] = customer
    return ''


@register.simple_tag(takes_context=True)
def load_linked_loginza_users(context):
    from loginza.models import UserMap
    request = context['request']
    if request.user.is_authenticated():
        user = request.user
        usermaps = UserMap.objects.filter(user=user)
        context['loginza_usermaps'] = usermaps
    return ''


@register.filter
def is_false(arg):
    return arg is False


@register.filter
def is_true(arg):
    return arg is True
