# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
import os
from django.conf import settings


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        from lfs.catalog.models import PropertyType
        from lfs.filters.models import Filter
        from lfs.catalog.settings import PROPERTY_TYPE_FLOAT

        print('Found slider filters...')
        filters = Filter.objects.filter(type=20) # Sliders
        for f in filters:
            properties = f.properties.all()
            for p in properties:
                try:
                    p_type = PropertyType.objects.get(
                        categories=f.category, 
                        property=p,
                    )
                except PropertyType.DoesNotExist:
                    p_type = PropertyType.objects.create(property=p, type=PROPERTY_TYPE_FLOAT)
                    p_type.categories.add(f.category)
                    p_type.save()
                    print(u'Created: "%s"' % unicode(p_type))
                except PropertyType.MultipleObjectsReturned:
                    print(u'ERROR: Found more than one PropertyType for "%s" and "%s".' % (f.category, p))
                    
        print('OK')
