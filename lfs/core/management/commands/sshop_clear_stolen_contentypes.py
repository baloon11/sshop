# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        print 'Cleaning stolen ContentType...'
        from django.contrib.contenttypes.models import ContentType
        for c in ContentType.objects.all():
            if not c.model_class():
                print "deleting %s" % c
                c.delete()
        print '-------------OK'
