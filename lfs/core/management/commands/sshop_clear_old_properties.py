# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
import os
from django.conf import settings


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        from lfs.catalog.models import (
            ProductsPropertiesRelation,
            GroupsPropertiesRelation,
            PropertyGroup,
            )
        print 'ProductsPropertiesRelation deleting...'
        ProductsPropertiesRelation.objects.all().delete()
        print 'GroupsPropertiesRelation deleting...'
        for g in GroupsPropertiesRelation.objects.all():
            g.delete()
        print 'PropertyGroup deleting...'
        PropertyGroup.objects.all().delete()
        print 'OK'
