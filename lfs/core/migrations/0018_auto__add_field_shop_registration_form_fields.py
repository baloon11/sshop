# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Shop.registration_form_fields'
        db.add_column('core_shop', 'registration_form_fields',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Shop.registration_form_fields'
        db.delete_column('core_shop', 'registration_form_fields')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.action': {
            'Meta': {'object_name': 'Action'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'actions'", 'to': "orm['core.ActionGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['core.Action']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'core.actiongroup': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ActionGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'core.shop': {
            'FIO': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'FIO_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'Shop'},
            'address_fields': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'address_form': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'address_form_fields': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'address_form_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'allow_previous_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'captcha': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'cart_link_to_rules': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'cart_require_accept_rules': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category_cols': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'checkout_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'confirmation_by_sms': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_of_birthday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_of_birthday_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'default_currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sshop_currencies.Currency']", 'null': 'True', 'blank': 'True'}),
            'default_search': ('django.db.models.fields.CharField', [], {'default': "'searchapi.searchconfigs.SimpleSearchConfig'", 'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email_confirmation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'extra': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'featured_badge': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'from_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'ga_ecommerce_tracking': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ga_site_tracking': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'google_analytics_id': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'link_to_rules': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'make_bg_registration': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'max_menu_item_count': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'menu_type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_seo_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_title': ('django.db.models.fields.CharField', [], {'default': "'<name>'", 'max_length': '80', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'notification_emails': ('django.db.models.fields.TextField', [], {}),
            'phone': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prerendered_menu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price_calculator': ('django.db.models.fields.CharField', [], {'default': "'lfs.default_price.DefaultPriceCalculator'", 'max_length': '255'}),
            'product_cols': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'product_rows': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'ref_code': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ref_code_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'registration_form_fields': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'require_accept_rules': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'search_field_text': ('django.db.models.fields.CharField', [], {'default': "u'Search products...'", 'max_length': '50', 'blank': 'True'}),
            'send_email_as': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'send_users_data_to_1c': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'shop_owner': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'show_regular_customer_tab': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'template_msg_of_registration': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'template_of_address': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'template_of_discount_code': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'topseller_badge': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'use_for_login': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'watermark_image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'watermark_opacity': ('django.db.models.fields.FloatField', [], {'default': '1'}),
            'watermark_position': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'fields.fieldsobjects': {
            'Meta': {'ordering': "['position']", 'object_name': 'FieldsObjects'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fields'", 'to': "orm['contenttypes.ContentType']"}),
            'field_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'field_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'field'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '999'})
        },
        'sshop_currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'coeffitient': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'format_str': ('django.db.models.fields.CharField', [], {'default': "u'%(value).2f %(abbr)s'", 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['core']