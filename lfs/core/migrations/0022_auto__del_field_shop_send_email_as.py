# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Shop.send_email_as'
        db.delete_column('core_shop', 'send_email_as')


    def backwards(self, orm):
        # Adding field 'Shop.send_email_as'
        db.add_column('core_shop', 'send_email_as',
                      self.gf('django.db.models.fields.IntegerField')(default=5),
                      keep_default=False)


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.action': {
            'Meta': {'object_name': 'Action'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'actions'", 'to': "orm['core.ActionGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['core.Action']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'core.actiongroup': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ActionGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'core.shop': {
            'Meta': {'object_name': 'Shop'},
            'address_form_fields': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'allow_previous_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ask_email_when_checkout': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ask_email_when_register': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ask_phone_when_checkout': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ask_phone_when_register': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cart_link_to_rules': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'cart_require_accept_rules': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category_cols': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'checkout_form_fields': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'default_currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sshop_currencies.Currency']", 'null': 'True', 'blank': 'True'}),
            'default_search': ('django.db.models.fields.CharField', [], {'default': "'searchapi.searchconfigs.SimpleSearchConfig'", 'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'extra': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'featured_badge': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'from_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'ga_ecommerce_tracking': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ga_site_tracking': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'google_analytics_id': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'link_to_rules': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'make_bg_registration': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'max_menu_item_count': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'menu_type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_seo_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_title': ('django.db.models.fields.CharField', [], {'default': "'<name>'", 'max_length': '80', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'notification_emails': ('django.db.models.fields.TextField', [], {}),
            'order_form_fields': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'prerendered_menu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price_calculator': ('django.db.models.fields.CharField', [], {'default': "'lfs.default_price.DefaultPriceCalculator'", 'max_length': '255'}),
            'product_cols': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'product_rows': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'registration_form_fields': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'require_accept_rules': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'search_field_text': ('django.db.models.fields.CharField', [], {'default': "u'Search products...'", 'max_length': '50', 'blank': 'True'}),
            'shop_owner': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'show_regular_customer_tab': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'template_msg_of_registration': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'template_of_address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'template_of_discount_code': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'topseller_badge': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'watermark_image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'watermark_opacity': ('django.db.models.fields.FloatField', [], {'default': '1'}),
            'watermark_position': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'fields.fieldsobjects': {
            'Meta': {'ordering': "['position']", 'object_name': 'FieldsObjects'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fields'", 'to': "orm['contenttypes.ContentType']"}),
            'field_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'field_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'field'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '999'})
        },
        'sshop_currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'coeffitient': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'format_str': ('django.db.models.fields.CharField', [], {'default': "u'%(value).2f %(abbr)s'", 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['core']