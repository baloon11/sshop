# coding: utf-8
from datetime import datetime
from django.conf import settings

from django.contrib.sitemaps import Sitemap
from django.contrib.sites.models import get_current_site
from django.contrib.sitemaps.views import sitemap
from django.http import Http404
from django.template.response import TemplateResponse

from lfs.catalog.models import Category
from lfs.catalog.models import Product
from lfs.core.models import Shop
from lfs.page.models import Page
from lfs.catalog.settings import VARIANT

from django.core.exceptions import ImproperlyConfigured
from django.contrib.sites.models import Site
# from django.core import urlresolvers, paginator


class ProductSitemap(Sitemap):
    """Google's XML sitemap for products.
    """
    def __init__(self, category=None):
        super(ProductSitemap, self).__init__()
        self.category = category

    changefreq = "weekly"
    priority = 0.5

    def items(self):
        if self.category:
            if getattr(settings, 'CATEGORY_HIDE_VARIANTS', False):
                products = list(Product.objects.filter(
                    active=True,
                    status__is_visible=True,
                    categories=self.category
                ).exclude(
                    sub_type=VARIANT
                ))
            else:
                products = list(Product.objects.filter(
                    active=True,
                    status__is_visible=True, categories=self.category))
            return [self.category] + products
        else:
            if getattr(settings, 'CATEGORY_HIDE_VARIANTS', False):
                products = list(
                    Product.objects.filter(
                        active=True, status__is_visible=True
                    ).exclude(sub_type=VARIANT))
            else:
                products = list(
                    Product.objects.filter(
                        active=True, status__is_visible=True))
            return [self.category] + products

    def lastmod(self, obj):
        if isinstance(obj, Product):
            return obj.creation_date
        elif isinstance(obj, Category):
            return datetime.now()


if 'pcart_api' in settings.INSTALLED_APPS \
        and getattr(settings, 'PCART_API_ENABLE_REDIRECT', False):

    def __get(self, name, obj, default=None):
        try:
            attr = getattr(self, name)
        except AttributeError:
            return default
        if callable(attr):
            return attr(obj)
        return attr

    def get_urls(self, page=1, site=None, protocol=None):
        # Determine protocol
        if self.protocol is not None:
            protocol = self.protocol
        if protocol is None:
            protocol = 'http'

        # Determine domain
        if site is None:
            if Site._meta.installed:
                try:
                    site = Site.objects.get_current()
                except Site.DoesNotExist:
                    pass
            if site is None:
                raise ImproperlyConfigured(
                    "To use sitemaps, either enable the sites framework"
                    " or pass a Site/RequestSite object in your view.")
        domain = site.domain
        mobile_domain = getattr(settings, 'PCART_API_DOMAIN', '')
        urls = []
        for item in self.paginator.page(page).object_list:
            loc = "%s://%s%s" % (
                protocol, domain, self.__get('location', item))
            priority = self.__get('priority', item, None)
            mobile_loc = None
            if isinstance(item, Product):
                mobile_loc = "%s/p/%s/" % (
                    mobile_domain.strip('/'), item.slug)
            elif isinstance(item, Category):
                mobile_loc = "%s/c/%s/" % (
                    mobile_domain.strip('/'), item.slug)
            elif isinstance(item, Page):
                mobile_loc = "%s/page/%s/" % (
                    mobile_domain.strip('/'), item.slug)
            if 'sbits_pages' in settings.INSTALLED_APPS:
                from sbits_pages.models import SCMS_Category, SCMS_News
                if isinstance(item, SCMS_Category):
                    mobile_loc = "%s/info/%s/" % (
                        mobile_domain.strip('/'), item.slug)
                if isinstance(item, SCMS_News):
                    for scms_category in item.category.all():
                        mobile_loc = "%s/info/%s/%s/" % (
                            mobile_domain.strip('/'),
                            scms_category.slug,
                            item.slug
                        )
                        url_info = {
                            'item': item,
                            'location': loc,
                            'mobile_location': mobile_loc,
                            'lastmod': self.__get('lastmod', item, None),
                            'changefreq': self.__get('changefreq', item, None),
                            'priority': str(
                                priority is not None and priority or ''),
                        }
                        urls.append(url_info)
                    continue

            url_info = {
                'item': item,
                'location': loc,
                'mobile_location': mobile_loc,
                'lastmod': self.__get('lastmod', item, None),
                'changefreq': self.__get('changefreq', item, None),
                'priority': str(priority is not None and priority or ''),
            }
            urls.append(url_info)
        return urls
    Sitemap.__get = __get
    Sitemap.get_urls = get_urls


class CategorySitemap(Sitemap):
    """Google's XML sitemap for categories.
    """
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Category.objects.all()

    def lastmod(self, obj):
        return datetime.now()


class PageSitemap(Sitemap):
    """Google's XML sitemap for pages.
    """
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Page.objects.filter(active=True)

    def lastmod(self, obj):
        return datetime.now()


class ShopSitemap(Sitemap):
    """Google's XML sitemap for the shop.
    """
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Shop.objects.all()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return "/"


def index(
        request,
        template_name='sitemap_index.xml', mimetype='application/xml'):
    """ Generation index sitemap
    """
    protocol = 'https' if request.is_secure() else 'http'
    req_site = get_current_site(request)

    sites = []

    sitemap_url = '/sitemap/pages.xml'
    absolute_url = '%s://%s%s' % (protocol, req_site.domain, sitemap_url)
    sites.append(absolute_url)

    for i in Category.objects.filter(exclude_from_navigation=False):
        sitemap_url = '/sitemap/category/' + i.slug + '.xml'
        absolute_url = '%s://%s%s' % (protocol, req_site.domain, sitemap_url)
        sites.append(absolute_url)

    # Append extra sitemaps
    for s in settings.EXTRA_SITEMAPS:
        sitemap_url = s['url']
        absolute_url = '%s://%s%s' % (protocol, req_site.domain, sitemap_url)
        sites.append(absolute_url)

    return TemplateResponse(request, template_name, {'sitemaps': sites},
                            content_type=mimetype)


def products_sitemap(request, category):
    try:
        category = Category.objects.get(slug=category)
    except Category.DoesNotExist:
        raise Http404

    sitemaps = {'Products': ProductSitemap(category=category)}

    return sitemap(request, sitemaps)


def pages_sitemap(request):
    sitemaps = {'Pages': PageSitemap}

    return sitemap(request, sitemaps)
