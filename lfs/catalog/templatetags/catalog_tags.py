# coding: utf-8
from django import template

from ..models import Category

register = template.Library()


@register.inclusion_tag(
    'admin/catalog_includes/products_block.html', takes_context=True)
def admin_products_info_block(context):
    object_id = context['object_id']
    category = Category.objects.get(pk=object_id)
    return {
        'category': category,
    }
