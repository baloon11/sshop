# coding: utf-8
import logging

from django import template
from django.conf import settings
from django.utils.translation import ugettext as _

from ..models import (
    ProductPropertyValue,
    PropertyValueIcon,
)


logger = logging.getLogger('sshop')
register = template.Library()


def _get_property_value(product, property_identifier):
    value = ''
    try:
        p_value = ProductPropertyValue.objects.get(
            product=product, property__identificator=property_identifier)
        value = p_value.value
    except ProductPropertyValue.DoesNotExist:
        value = ''
    except ProductPropertyValue.MultipleObjectsReturned:
        logger.error(_(
            u'Found more than one value for property "%(property)s" for '
            u'product "%(product)s" (pk=%(product_pk)s).') % {
                'property': property_identifier,
                'product': product.get_name(),
                'product_pk': product.pk,
            })
    return value


@register.inclusion_tag(
    'lfs/catalog/products/value_icons.html', takes_context=True)
def property_value_icons(context, product):
    p_icons = PropertyValueIcon.objects.filter(
        products=product).order_by('position')
    return {
        'icons': p_icons,
    }


@register.simple_tag(takes_context=True)
def property_for_product(context, product, property_identifier):
    return _get_property_value(product, property_identifier)


@register.simple_tag(takes_context=True)
def property_for_product_with_translator(
        context, product, property_identifier, translator_name):
    translators = getattr(settings, 'PRODUCT_PROPERTY_TRANSLATORS', {})
    translator = {}
    if translator_name in translators:
        translator = translators[translator_name]

    value = _get_property_value(product, property_identifier)
    value = translator.get(value, value)
    return value


@register.filter
def translate_property(value, translator_name):
    translators = getattr(settings, 'PRODUCT_PROPERTY_TRANSLATORS', {})
    translator = {}
    if translator_name in translators:
        translator = translators[translator_name]
    value = translator.get(value, value)
    return value


@register.inclusion_tag(
    'admin/catalog_includes/sort_inline.html', takes_context=True)
def admin_sort_inline(context):
    from ...core.utils import import_symbol
    if 'object_id' in context:
        object_id = context['object_id']
        sorters = []
        for s in settings.PROPERTY_SORTERS:
            sorter_class = import_symbol(s[1])
            sorters.append({
                'code': s[0],
                'class_name': s[1],
                'name': sorter_class.name,
            })
        return {
            'sorters': sorters,
            'property_type_id': object_id
        }
    return {}
