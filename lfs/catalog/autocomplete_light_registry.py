# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django import http
import autocomplete_light
from .models import (
    Category,
    Product,
    Property,
    PropertyUnit,
    PropertyOption,
)


class CategoryAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    search_fields = ['name']
    autocomplete_js_attributes = {
        'placeholder': _(u'Type the category name...'),
    }

autocomplete_light.register(Category, CategoryAutocomplete)


class ProductAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    search_fields = ['name']
    autocomplete_js_attributes = {
        'placeholder': _(u'Type the product name...'),
    }
    choice_html_format = u'''
        <span class="div" data-value="%s">%s - %s</span>
    '''

    def choice_html(self, choice):
        return self.choice_html_format % (
            self.choice_value(choice),
            self.choice_label(choice),
            choice.status.name if choice.status else '',
        )

autocomplete_light.register(Product, ProductAutocomplete)


class PropertyAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    search_fields = ['name']
    add_another_url_name = 'admin:catalog_property_add'
    autocomplete_js_attributes = {
        'placeholder': _(u'Type the property name...'),
    }

    def post(self, request, *args, **kwargs):
        p = Property.objects.create(name=request.POST['createChoice'])
        return http.HttpResponse(self.choice_html(p))

autocomplete_light.register(Property, PropertyAutocomplete)


class PropertyUnitAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    search_fields = ['name']
    autocomplete_js_attributes = {
        'minimum_characters': 0,
        'placeholder': _(u'Type the unit name...'),
    }

autocomplete_light.register(PropertyUnit, PropertyUnitAutocomplete)


class PropertyOptionAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    search_fields = ['name']
    autocomplete_js_attributes = {
        'minimum_characters': 0,
        'placeholder': _(u'Type the proerty value...'),
    }
    choice_html_format = u'''
        <span class="div" data-value="%s">%s</span>
    '''

    def choice_html(self, choice):
        return self.choice_html_format % (
            self.choice_value(choice),
            self.choice_label(choice),
        )
autocomplete_light.register(PropertyOption, PropertyOptionAutocomplete)
