# coding: utf-8
import logging

from django.http import Http404
from django.conf import settings
from django.core.cache import cache
from django.db.models import Max, Min, Q
from .settings import PRODUCT_WITH_VARIANTS
from .exceptions import InvalidFilters
from lfs.catalog.settings import STANDARD_PRODUCT, VARIANT

from urllib import urlencode


logger = logging.getLogger("sshop")


# TODO: Add unit test
def get_current_top_category(request, obj):
    """
    Returns the current top category of a product.
    """
    if obj.__class__.__name__.lower() == "product":
        category = obj.get_current_category(request)
    else:
        category = obj

    if category is None:
        return category

    while category.parent is not None:
        category = category.parent

    return category


def filter_product(categories, products, product_filter):
    #TODO: are categories in really necessary for this function
    # it looks that using only products attribute is more than enough
    from ..filters.models import Filter, FilterOption
    try:
        for key, value in product_filter.items():
            if '..' not in value[0]:
                ids = [fo['id'] for fo in FilterOption.objects.filter(
                    Q(filter__identificator=key) &
                    Q(identificator__in=value[0].split(',')) &
                    Q(filter__category__in=categories)
                ).values('id')]

                if len(ids) != len(value[0].split(',')):
                    raise InvalidFilters

                products = products.filter(filteroption__id__in=ids)
            elif Filter.objects.get(
                    Q(identificator=key) &
                    Q(category__in=categories)).special_filter == 10:
                value = value[0].split('..')

                if value[0] != value[0].strip()\
                        or value[1] != value[1].strip():
                    raise InvalidFilters

                if not value[0] and not value[1]:
                    raise InvalidFilters
                elif not value[0] and value[1]:
                    products = products.filter(
                        effective_price__lte=int(value[1]),
                    )
                elif not value[1] and value[0]:
                    products = products.filter(
                        effective_price__gte=int(value[0]),
                    )
                else:
                    products = products.filter(
                        Q(effective_price__gte=int(value[0])) &
                        Q(effective_price__lte=int(value[1]))
                    )
            else:
                # TODO: need correct for multicategory filtering
                f = Filter.objects.get(
                    Q(identificator=key) &
                    Q(category__in=categories))
                value = value[0].split('..')
                if not value[0] and not value[1]:
                    raise InvalidFilters
                elif not value[0] and value[1]:
                    p_values = f.property_values.filter(
                        value_as_float__lte=int(value[1]))
                elif not value[1] and value[0]:
                    p_values = f.property_values.filter(
                        value_as_float__gte=int(value[0]))
                else:
                    p_values = f.property_values.filter(
                        Q(value_as_float__gte=int(value[0])) &
                        Q(value_as_float__lte=int(value[1])))
                if p_values.count() == 0:
                    raise InvalidFilters
                products = products.filter(property_values__in=p_values)
    except (Filter.DoesNotExist, ValueError):
        raise InvalidFilters
    products = products.distinct()

    return products


def get_product_filters(category, product_filter, sorting):
    """Returns the next product filters based on products which
    are in the given category and within the result set of
    the current filters.
    """
    from ..filters.models import Filter

    product_filter.pop('start', None)
    if product_filter:
        ck_product_filter = ""
        for key, value in product_filter.items():
            ck_product_filter += key + "|"
            ck_product_filter += "|".join(value)
    else:
        ck_product_filter = ""

    cache_key = "%s-productfilters-%s-%s-%s" % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX,
        category.slug, ck_product_filter, sorting)

    result = cache.get(cache_key)
    if result is not None:
        return result

    cache_keys = cache.get('%s-productfilters-keys-%s' % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX, category.slug), [])
    cache_keys.append(cache_key)
    cache.set('%s-productfilters-keys-%s' % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX, category.slug), cache_keys)

    filters = Filter.objects.filter(category=category, parent=None)

    categories = [category]
    if getattr(settings, 'FILTER_SHOW_ALL_PRODUCTS', True) and category.show_all_products:
        categories.extend(category.get_descendants())

    result = get_filters_as_dict(filters, categories, product_filter)

    cache.set(cache_key, result)
    return result


def get_filters_as_dict(filters, categories, product_filter):
    from .models import Product
    from ..filters.models import FilterOption
    SHOW_FILTER_COUNTERS = getattr(settings, 'SHOW_FILTER_COUNTERS', True)
    USE_HR_URLS = getattr(settings, 'USE_HR_URLS', False)
    GENERATE_HR_URLS_FOR_FO = getattr(settings, 'GENERATE_HR_URLS_FOR_FO', False)

    params = {}
    for key, value in product_filter.items():
        params[key] = ','.join(value)

    products = Product.objects.filter(
        active=True,
        status__is_visible=True,
        categories__in=categories,
    ).distinct().select_related()

    products_before_initial = products
    # filtered products
    products = filter_product(categories, products, product_filter)
    ids = [p['id'] for p in products.values('id')]

    result = []
    for f in filters:
        if USE_HR_URLS and GENERATE_HR_URLS_FOR_FO:
            from hr_urls.utils import get_best_template
            base_param = dict(params)
            for key, value in base_param.items():
                if value.count(',') > 0 and key != f.identificator:
                    del base_param[key]
            if f.identificator not in base_param:
                base_param.update({f.identificator: ''})
                template1 = template2 = get_best_template(
                    categories[0], base_param)
            elif base_param[f.identificator].count(',') < 2:
                template1 = get_best_template(
                    categories[0], base_param)
                del base_param[f.identificator]
                template2 = get_best_template(
                    categories[0], base_param)
            elif base_param[f.identificator].count(',') >= 2:
                del base_param[f.identificator]
                template1 = template2 = get_best_template(
                    categories[0], base_param)

        options = f.filteroption_set.all().order_by('-is_popular', 'position')
        # mark checked options
        for o in options:
            if f.identificator in product_filter:
                o.checked = o.identificator in \
                    product_filter[f.identificator][0].split(',')

        show_all_options = False
        showing_options = []
        if f.take_popularity and (f.type == 0 or f.type == 10):
            for o in options:
                if ((hasattr(o, 'checked') and o.checked)) \
                        and not o.is_popular:
                    showing_options = list(options)
                    show_all_options = False
                    break
                elif o.is_popular:
                    showing_options.append(o)
                    show_all_options = True

        if not showing_options:
            showing_options = list(options)

        checked = 0
        if f.type != 20:
            popular_items = False
            has_items = False
            _filters = dict(product_filter)
            _filters.pop(f.identificator, None)

            products_with_out_f = filter_product(
                categories, products_before_initial, _filters)

            idents = []
            if SHOW_FILTER_COUNTERS is False:
                idents = [
                    x['identificator'] for x in FilterOption.objects.filter(
                        filter__identificator=f.identificator,
                        products__in=products_with_out_f
                    ).distinct().values('identificator')
                ]

            for o in showing_options:
                if ((hasattr(o, 'checked') and o.checked)) \
                        or not f.is_addition:
                    checked += 1

                    if SHOW_FILTER_COUNTERS:
                        o.quantity = Product.objects.filter(
                            pk__in=ids, filteroption__id=o.id).count()
                    else:
                        o.quantity = 1 if o.identificator in idents else 0

                elif f.is_addition:
                    if SHOW_FILTER_COUNTERS:
                        o.quantity = products_with_out_f.filter(
                            filteroption__id=o.id).count()
                    else:
                        o.quantity = 1 if o.identificator in idents else 0
                if getattr(
                    settings,
                        'FILTER_SHOW_CHECKED_OPTIONS_WITHOUT_PRODUCTS', False):
                    if f.identificator in product_filter.keys() and\
                            o.quantity == 0 and\
                            o.identificator in product_filter[f.identificator]:
                            o.quantity = 1

                # if has items with quantity > 0 and popular_items
                # and one option not popular
                if has_items and popular_items and not o.is_popular:
                    show_all_options = True

                if o.quantity > 0:
                    has_items = True
                    if o.is_popular:  # if popular option and his quantity > 0
                        popular_items = True

                # if in popular option no quantity > 0 add
                # not popular options to list
                if (showing_options.index(o) + 1) == \
                        len(showing_options) and not has_items:
                    has_items = True
                    checked = 0
                    show_all_options = False
                    for x in options:
                        if x not in showing_options:
                            showing_options.append(x)

                # generate links for fitler options
                if USE_HR_URLS and GENERATE_HR_URLS_FOR_FO:
                    template = template1
                    get_params = dict(params)
                    if f.identificator not in get_params:
                        get_params.update(
                            {f.identificator: o.identificator})
                    elif o.identificator in get_params[f.identificator].split(','):
                        if get_params[f.identificator] == o.identificator:
                            del get_params[f.identificator]
                            template = template2
                        else:
                            ps = get_params[f.identificator].split(',')
                            ps.remove(o.identificator)
                            get_params[f.identificator] = ','.join(ps)
                    else:
                        ps = get_params[f.identificator].split(',')
                        ps += [o.identificator]
                        get_params[f.identificator] = ','.join(ps)
                        if len(ps) == 1:
                            template = template1
                        else:
                            template = template2
                    get_params = urlencode(get_params).replace('%2C', ',')
                    # from hr_urls.utils import get_hr_urls
                    # o.link = '/%s' % categories[0].slug + get_hr_urls(
                    #     categories[0],
                    #     get_params,
                    # )

                    from hr_urls.utils import get_formatted_url
                    link = get_formatted_url(template, get_params)
                    o.link = '/%s' % categories[0].slug
                    if link != '/':
                        o.link += link
                    o.link = o.link.replace('%2C', ',')

        min_value, max_value = None, None
        if f.special_filter == 10:  # Price
            if ids:
                agg_result = products.filter(
                    sub_type__in=[STANDARD_PRODUCT, VARIANT]
                ).aggregate(
                    Min('effective_price'),
                    Max('effective_price'),
                )
                try:
                    min_value, max_value = \
                        int(agg_result['effective_price__min']), \
                        int(agg_result['effective_price__max'])
                except TypeError:
                    min_value, max_value = 0, 0
            elif f.identificator in product_filter:
                values = product_filter[f.identificator][0].split('..')
                if values[0] and values[1]:
                    min_value, max_value = int(values[0]), int(values[1])
                else:
                    min_value = max_value = int(values[0] or values[1])

        elif f.special_filter == 0:  # Property
            if f.type == 20:  # Slider
                if ids:
                    agg_result = f.property_values.filter(
                        product__id__in=ids) \
                        .aggregate(
                            Min('value_as_float'),
                            Max('value_as_float'),
                        )
                    try:
                        min_value, max_value = \
                            int(agg_result['value_as_float__min']), \
                            int(agg_result['value_as_float__max'])
                    except TypeError:
                        min_value, max_value = 0, 0
                elif f.identificator in product_filter:
                    values = product_filter[f.identificator][0].split('..')
                    if values[0] and values[1]:
                        min_value, max_value = int(values[0]), int(values[1])
                    else:
                        min_value = max_value = int(values[0] or values[1])
        result.append({
            'id': f.id,
            'show_as_tree': f.show_as_tree,
            'identificator': f.identificator,
            'title': f.get_title(),
            'special_filter': f.special_filter,
            'template_name': f.get_template_name(),
            'type': f.type,
            'options': options,
            'is_addition': f.is_addition,
            'checked': checked,
            'min_value_0': int(f.min_value),
            'max_value_0': int(f.max_value),
            'min_value': min_value,
            'max_value': max_value,
            'use_after_choose': f.use_after_choose,
            'show_all_options': show_all_options,
            'show_filter_counters': SHOW_FILTER_COUNTERS,
        })
    return result


def get_filtered_products_for_category(
        category, filters, sorting, variant_distinct=False):
    """Returns products for given categories and current filters sorted by
    current sorting.
    """
    from .models import Product

    categories = [category]
    if getattr(settings, 'FILTER_SHOW_ALL_PRODUCTS', True) and category.show_all_products:
        categories.extend(category.get_descendants())

    # products = Product.objects.none()
    # for category in categories:
    #     products |= category.products.filter(
    #         active=True,
    #         status__is_visible=True,
    #         sub_type__in=[STANDARD_PRODUCT, VARIANT])
    if getattr(settings, 'CATEGORY_HIDE_VARIANTS', False):
        products = Product.objects.filter(
            # Q(sub_type=STANDARD_PRODUCT) | Q(sub_type=VARIANT),
            active=True,
            status__is_visible=True,
            categories__in=categories).distinct()
    else:
        products = Product.objects.filter(
            # Q(sub_type=STANDARD_PRODUCT) | Q(sub_type=VARIANT),
            active=True,
            status__is_visible=True,
            categories__in=categories,
        ).exclude(sub_type=PRODUCT_WITH_VARIANTS)
    if len(categories) > 1:
        products = products.distinct()

    if filters:
        filters.pop('start', None)
        try:
            products = filter_product(categories, products, filters)
        except:
            raise Http404

    if not sorting:
        from .models import SortType
        try:
            sorting = \
                SortType.objects.all().order_by('order')[0].sortable_fields
        except IndexError:
            sorting = \
                getattr(settings, 'DEFAULT_SORTING', 'effective_price')

    if sorting:
        try:
            sort_args = sorting.replace(' ', '').split(',')
            if variant_distinct:
                _sort_args = ['sub_type', 'parent__id']  # + sort_args

                variants_and_parents = [
                    (p['id'], p['parent_id']) for p in products.order_by(
                        *_sort_args).values('id', 'parent_id')
                ]

                ids = set()
                good_id = None
                prev_parent = None
                # prev_count = 0
                ids_add = ids.add
                for i in variants_and_parents:
                    if not i[1]:
                        # becouse this product is standart
                        # or varian without parent
                        ids_add(i[0])
                        prev_parent = None
                        good_id = None
                    elif i[1] != prev_parent:
                        good_id = i[0]
                        # prev_count = 0
                        prev_parent = i[1]
                        ids_add(good_id)
                    else:
                        # prev_count += 1
                        if good_id and good_id in ids:
                            ids.remove(good_id)
                            ids_add(prev_parent)
                products = Product.objects.filter(id__in=tuple(ids))\
                    .order_by(*sort_args).select_related()

                # print ':::', 10285 in tuple(ids)
            elif getattr(settings, 'CATEGORY_HIDE_VARIANTS', False):
                ids = set()
                for p in products:
                    if p.sub_type == VARIANT and p.parent:
                        ids.add(p.parent.id)
                    else:
                        ids.add(p.id)
                products = Product.objects.filter(id__in=tuple(ids))\
                    .order_by(*sort_args).select_related()
            else:
                products = products.order_by(*sort_args).select_related()
        except Exception, e:
            logger.error(u'Error when try to sort products: %s' % unicode(e))
    return products


def rebuild_main_menu():
    """Rebuild main menu
    """
    from ..core.utils import get_default_shop
    shop = get_default_shop()
    shop.prerender_menu()


def standard_validation(property_value, property_type, unpack_data=False):
    """This function was written for testing purposes only.
    Use it as example for own custom validators.
    """
    value = property_value.value
    t = value.split()
    if len(t) == 2:
        if unpack_data:
            return {'value': t[0], 'unit': t[1]}
        else:
            return (True, None)
    else:
        if unpack_data:
            return {'value': '0'}
        else:
            return (False, u'Validation error.')
