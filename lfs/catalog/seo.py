# coding: utf-8
from ..core.seo import BaseSEOGenerator
from django.conf import settings


class ProductSEOGenerator(BaseSEOGenerator):
    def __init__(self, request, context):
        self.request = request
        self.context = context
        self.product = None
        if 'product' in self.context:
            self.product = self.context['product']

    def get_h1(self):
        if self.product:
            return self.product.get_meta_h1(
                request=self.request, context=self.context)
        return None

    def get_title(self):
        if self.product:
            return self.product.get_meta_title(
                request=self.request, context=self.context)
        return None

    def get_description(self):
        if self.product:
            return self.product.get_meta_description(
                request=self.request, context=self.context)
        return None

    def get_keywords(self):
        if self.product:
            return self.product.get_meta_keywords(
                request=self.request, context=self.context)
        return None

    def get_seo_text(self):
        if self.product and self.product.slug == self.request.path[1:]:
            return self.product.get_meta_seo_text(
                request=self.request, context=self.context)


class CategorySEOGenerator(BaseSEOGenerator):
    def __init__(self, request, context):
        self.request = request
        self.context = context
        self.category = None
        if 'category' in self.context:
            self.category = self.context['category']

    def get_h1(self):
        if self.category:
            return self.category.get_meta_h1(
                request=self.request, context=self.context)
        return None

    def get_title(self):
        if self.category:
            return self.category.get_meta_title(
                request=self.request, context=self.context)
        return None

    def get_description(self):
        if self.category:
            return self.category.get_meta_description(
                request=self.request, context=self.context)
        return None

    def get_keywords(self):
        if self.category:
            return self.category.get_meta_keywords(
                request=self.request, context=self.context)
        return None

    def get_seo_text(self):
        if settings.SHOW_SEO_TEXT_ON_FILTERED_PAGES:
            if self.request.method == 'GET':
                request_path = self.request.path.split('/')[1]
                if self.category and self.context['category_slug'] ==\
                        request_path:
                    return self.category.get_meta_seo_text(
                        request=self.request, context=self.context)
        else:
            if self.request.method == 'GET' and \
                not self.request.GET.keys() or \
                    self.request.GET.keys() == ['start'] and \
                    self.request.GET['start'] == u'1':
                request_path = self.request.path.split('/')[1]
                if self.category and self.context['category_slug'] ==\
                        request_path:
                    return self.category.get_meta_seo_text(
                        request=self.request, context=self.context)
