# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Product.active_base_price'
        db.delete_column('catalog_product', 'active_base_price')

        # Deleting field 'Product.supplier'
        db.delete_column('catalog_product', 'supplier_id')

        # Deleting field 'Product.weight'
        db.delete_column('catalog_product', 'weight')

        # Deleting field 'Product.active_packing_unit'
        db.delete_column('catalog_product', 'active_packing_unit')

        # Deleting field 'Product.tax'
        db.delete_column('catalog_product', 'tax_id')

        # Deleting field 'Product.packing_unit_unit'
        db.delete_column('catalog_product', 'packing_unit_unit')

        # Deleting field 'Product.price_calculation'
        db.delete_column('catalog_product', 'price_calculation')

        # Deleting field 'Product.active_name'
        db.delete_column('catalog_product', 'active_name')

        # Deleting field 'Product.active_for_sale'
        db.delete_column('catalog_product', 'active_for_sale')

        # Deleting field 'Product.active_meta_title'
        db.delete_column('catalog_product', 'active_meta_title')

        # Deleting field 'Product.base_price_amount'
        db.delete_column('catalog_product', 'base_price_amount')

        # Deleting field 'Product.active_price_calculation'
        db.delete_column('catalog_product', 'active_price_calculation')

        # Deleting field 'Product.width'
        db.delete_column('catalog_product', 'width')

        # Deleting field 'Product.category_variant'
        db.delete_column('catalog_product', 'category_variant')

        # Deleting field 'Product.manage_stock_amount'
        db.delete_column('catalog_product', 'manage_stock_amount')

        # Deleting field 'Product.manual_delivery_time'
        db.delete_column('catalog_product', 'manual_delivery_time')

        # Deleting field 'Product.active_meta_keywords'
        db.delete_column('catalog_product', 'active_meta_keywords')

        # Deleting field 'Product.stock_amount'
        db.delete_column('catalog_product', 'stock_amount')

        # Deleting field 'Product.default_variant'
        db.delete_column('catalog_product', 'default_variant_id')

        # Deleting field 'Product.base_price_unit'
        db.delete_column('catalog_product', 'base_price_unit')

        # Deleting field 'Product.ordered_at'
        db.delete_column('catalog_product', 'ordered_at')

        # Deleting field 'Product.active_price'
        db.delete_column('catalog_product', 'active_price')

        # Deleting field 'Product.height'
        db.delete_column('catalog_product', 'height')

        # Deleting field 'Product.order_time'
        db.delete_column('catalog_product', 'order_time_id')

        # Deleting field 'Product.delivery_time'
        db.delete_column('catalog_product', 'delivery_time_id')

        # Deleting field 'Product.active_sku'
        db.delete_column('catalog_product', 'active_sku')

        # Deleting field 'Product.active_images'
        db.delete_column('catalog_product', 'active_images')

        # Deleting field 'Product.active_meta_description'
        db.delete_column('catalog_product', 'active_meta_description')

        # Deleting field 'Product.active_accessories'
        db.delete_column('catalog_product', 'active_accessories')

        # Deleting field 'Product.deliverable'
        db.delete_column('catalog_product', 'deliverable')

        # Deleting field 'Product.length'
        db.delete_column('catalog_product', 'length')

        # Deleting field 'Product.packing_unit'
        db.delete_column('catalog_product', 'packing_unit')

        # Deleting field 'Product.active_for_sale_price'
        db.delete_column('catalog_product', 'active_for_sale_price')

        # Deleting field 'Product.active_related_products'
        db.delete_column('catalog_product', 'active_related_products')

        # Deleting field 'Product.active_dimensions'
        db.delete_column('catalog_product', 'active_dimensions')


    def backwards(self, orm):
        # Adding field 'Product.active_base_price'
        db.add_column('catalog_product', 'active_base_price',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Product.supplier'
        db.add_column('catalog_product', 'supplier',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['supplier.Supplier'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.weight'
        db.add_column('catalog_product', 'weight',
                      self.gf('django.db.models.fields.FloatField')(default=0.0),
                      keep_default=False)

        # Adding field 'Product.active_packing_unit'
        db.add_column('catalog_product', 'active_packing_unit',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Product.tax'
        db.add_column('catalog_product', 'tax',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tax.Tax'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.packing_unit_unit'
        db.add_column('catalog_product', 'packing_unit_unit',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=30, blank=True),
                      keep_default=False)

        # Adding field 'Product.price_calculation'
        db.add_column('catalog_product', 'price_calculation',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Product.active_name'
        db.add_column('catalog_product', 'active_name',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.active_for_sale'
        db.add_column('catalog_product', 'active_for_sale',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Product.active_meta_title'
        db.add_column('catalog_product', 'active_meta_title',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.base_price_amount'
        db.add_column('catalog_product', 'base_price_amount',
                      self.gf('django.db.models.fields.FloatField')(default=0.0, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.active_price_calculation'
        db.add_column('catalog_product', 'active_price_calculation',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.width'
        db.add_column('catalog_product', 'width',
                      self.gf('django.db.models.fields.FloatField')(default=0.0),
                      keep_default=False)

        # Adding field 'Product.category_variant'
        db.add_column('catalog_product', 'category_variant',
                      self.gf('django.db.models.fields.SmallIntegerField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.manage_stock_amount'
        db.add_column('catalog_product', 'manage_stock_amount',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.manual_delivery_time'
        db.add_column('catalog_product', 'manual_delivery_time',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.active_meta_keywords'
        db.add_column('catalog_product', 'active_meta_keywords',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.stock_amount'
        db.add_column('catalog_product', 'stock_amount',
                      self.gf('django.db.models.fields.FloatField')(default=0),
                      keep_default=False)

        # Adding field 'Product.default_variant'
        db.add_column('catalog_product', 'default_variant',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Product'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.base_price_unit'
        db.add_column('catalog_product', 'base_price_unit',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=30, blank=True),
                      keep_default=False)

        # Adding field 'Product.ordered_at'
        db.add_column('catalog_product', 'ordered_at',
                      self.gf('django.db.models.fields.DateField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.active_price'
        db.add_column('catalog_product', 'active_price',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.height'
        db.add_column('catalog_product', 'height',
                      self.gf('django.db.models.fields.FloatField')(default=0.0),
                      keep_default=False)

        # Adding field 'Product.order_time'
        db.add_column('catalog_product', 'order_time',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='products_order_time', null=True, to=orm['catalog.DeliveryTime'], blank=True),
                      keep_default=False)

        # Adding field 'Product.delivery_time'
        db.add_column('catalog_product', 'delivery_time',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='products_delivery_time', null=True, to=orm['catalog.DeliveryTime'], blank=True),
                      keep_default=False)

        # Adding field 'Product.active_sku'
        db.add_column('catalog_product', 'active_sku',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.active_images'
        db.add_column('catalog_product', 'active_images',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.active_meta_description'
        db.add_column('catalog_product', 'active_meta_description',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.active_accessories'
        db.add_column('catalog_product', 'active_accessories',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.deliverable'
        db.add_column('catalog_product', 'deliverable',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Adding field 'Product.length'
        db.add_column('catalog_product', 'length',
                      self.gf('django.db.models.fields.FloatField')(default=0.0),
                      keep_default=False)

        # Adding field 'Product.packing_unit'
        db.add_column('catalog_product', 'packing_unit',
                      self.gf('django.db.models.fields.FloatField')(default=1.0, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.active_for_sale_price'
        db.add_column('catalog_product', 'active_for_sale_price',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.active_related_products'
        db.add_column('catalog_product', 'active_related_products',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Product.active_dimensions'
        db.add_column('catalog_product', 'active_dimensions',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    models = {
        'catalog.category': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Category'},
            'active_formats': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category_cols': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'exclude_from_navigation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'extras': ('jsonfield.fields.JSONField', [], {'default': 'None', 'blank': 'True'}),
            'icon': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'level': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_title': ('django.db.models.fields.CharField', [], {'default': "'<name>'", 'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['catalog.Category']"}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '1000'}),
            'product_cols': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'product_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'product_rows': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'categories'", 'blank': 'True', 'to': "orm['catalog.Product']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'seo': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'show_all_products': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '300'}),
            'static_block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'categories'", 'null': 'True', 'to': "orm['catalog.StaticBlock']"}),
            'template': ('django.db.models.fields.PositiveSmallIntegerField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'default': "'54abc858-26eb-4715-8603-03fff5473133'", 'unique': 'True', 'max_length': '50'})
        },
        'catalog.deliverytime': {
            'Meta': {'ordering': "('min',)", 'object_name': 'DeliveryTime'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.FloatField', [], {}),
            'min': ('django.db.models.fields.FloatField', [], {}),
            'unit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'})
        },
        'catalog.file': {
            'Meta': {'ordering': "('position',)", 'object_name': 'File'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'files'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '999'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'catalog.image': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Image'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'image'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'catalog.product': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Product'},
            'accessories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'reverse_accessories'", 'to': "orm['catalog.Product']", 'through': "orm['catalog.ProductAccessories']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'active_description': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'active_short_description': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'active_static_block': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'default_value': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'effective_price': ('django.db.models.fields.FloatField', [], {'default': '0.0', 'blank': 'True'}),
            'for_sale': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'for_sale_price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'guarantee': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_1c': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'manufacturer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'products'", 'null': 'True', 'to': "orm['manufacturer.Manufacturer']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'meta_seo_text': ('django.db.models.fields.TextField', [], {'default': "u''", 'blank': 'True'}),
            'meta_title': ('django.db.models.fields.CharField', [], {'default': "u'{{ name }} - {{ shop_name }}'", 'max_length': '300', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'variants'", 'null': 'True', 'to': "orm['catalog.Product']"}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'price_calculator': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'price_unit': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'related_products': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'reverse_related_products'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['catalog.Product']"}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sku': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'sku_manufacturer': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '300'}),
            'static_block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'products'", 'null': 'True', 'to': "orm['catalog.StaticBlock']"}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.ProductStatus']", 'null': 'True', 'blank': 'True'}),
            'sub_type': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '10'}),
            'template': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type_of_quantity_field': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'default': "'fcbdf377-1d33-48a7-bd96-cb9662b8e9ee'", 'unique': 'True', 'max_length': '50'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'variant_position': ('django.db.models.fields.IntegerField', [], {'default': '999'}),
            'variants_display_type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'catalog.productaccessories': {
            'Meta': {'ordering': "('position',)", 'object_name': 'ProductAccessories'},
            'accessory': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'productaccessories_accessory'", 'to': "orm['catalog.Product']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '999'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'productaccessories_product'", 'to': "orm['catalog.Product']"}),
            'quantity': ('django.db.models.fields.FloatField', [], {'default': '1'})
        },
        'catalog.productattachment': {
            'Meta': {'ordering': "('position',)", 'object_name': 'ProductAttachment'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'attachments'", 'to': "orm['catalog.Product']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'catalog.productimage': {
            'Meta': {'object_name': 'ProductImage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'links': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Product']"})
        },
        'catalog.productpropertyvalue': {
            'Meta': {'unique_together': "(('product', 'property'),)", 'object_name': 'ProductPropertyValue'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'property_values'", 'to': "orm['catalog.Product']"}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'property_values'", 'to': "orm['catalog.Property']"}),
            'value': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'value_as_float': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        'catalog.productstatus': {
            'Meta': {'object_name': 'ProductStatus'},
            'css_class': ('django.db.models.fields.CharField', [], {'max_length': '70', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_searchable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'show_ask_button': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_buy_button': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'catalog.property': {
            'Meta': {'ordering': "['-is_group', 'name']", 'unique_together': "(('name', 'is_group'),)", 'object_name': 'Property'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificator': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'is_group': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'uid': ('django.db.models.fields.CharField', [], {'default': "'a83a6286-a6c6-4555-919c-42894db775ce'", 'unique': 'True', 'max_length': '50'})
        },
        'catalog.propertyoption': {
            'Meta': {'ordering': "['name']", 'object_name': 'PropertyOption'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'catalog.propertyoptionrelation': {
            'Meta': {'ordering': "['position']", 'object_name': 'PropertyOptionRelation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'property_option': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.PropertyOption']"}),
            'property_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.PropertyType']"})
        },
        'catalog.propertyset': {
            'Meta': {'object_name': 'PropertySet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'catalog.propertysetrelation': {
            'Meta': {'object_name': 'PropertySetRelation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Property']"}),
            'property_set': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.PropertySet']"})
        },
        'catalog.propertytype': {
            'Meta': {'object_name': 'PropertyType'},
            'actions': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'available_units': ('mptt.fields.TreeManyToManyField', [], {'blank': 'True', 'related_name': "'available_property_types'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['catalog.PropertyUnit']"}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['catalog.Category']", 'symmetrical': 'False'}),
            'do_sync': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'false_value': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_searchable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_value': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'min_value': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'property': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['catalog.Property']"}),
            'regex': ('django.db.models.fields.TextField', [], {'default': "u'@strip\\n(?P<value>\\\\d+[.,]?\\\\d*)\\n@replace:value:,:.'", 'blank': 'True'}),
            'true_value': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'unit': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'property_types'", 'null': 'True', 'to': "orm['catalog.PropertyUnit']"}),
            'validator': ('django.db.models.fields.CharField', [], {'max_length': '155', 'blank': 'True'}),
            'visible_in_list': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'catalog.propertyunit': {
            'Meta': {'object_name': 'PropertyUnit'},
            'coefficient': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['catalog.PropertyUnit']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'catalog.propertyvalueicon': {
            'Meta': {'object_name': 'PropertyValueIcon'},
            'category': ('mptt.fields.TreeForeignKey', [], {'to': "orm['catalog.Category']"}),
            'expression': ('django.db.models.fields.TextField', [], {}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '999'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['catalog.Product']", 'symmetrical': 'False', 'blank': 'True'}),
            'properties': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['catalog.Property']", 'symmetrical': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'})
        },
        'catalog.sorttype': {
            'Meta': {'object_name': 'SortType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sortable_fields': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'catalog.staticblock': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StaticBlock'},
            'display_files': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '1000'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'manufacturer.manufacturer': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Manufacturer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_popular': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['catalog']