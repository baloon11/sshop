# coding: utf-8
from .models import Property


class PropertyLookup(object):
    auto_add = True

    def create_from_ajax_string(
            self, ajax_string, request_data, form_field_name):
        p, created = Property.objects.get_or_create(
            name=unicode(ajax_string), is_group=False)
        if created:
            return p
        else:
            return None

    def get_query(self, q, request):
        """ return a query set.  you also have access to
        request.user if needed """
        return Property.objects.filter(name__istartswith=q)

    def format_result(self, t_property):
        """ the search results display in the dropdown menu.
        may contain html and multiple-lines. will remove any |  """
        return u"%s" % (t_property.name,)

    def format_item(self, t_property):
        """ the display of a currently selected object in the area
        below the search box. html is OK """
        return u"<span><a href='#'>%s</a></span>" % (t_property.name,)

    def get_objects(self, ids):
        """ given a list of ids, return the objects ordered as you
        would like them on the admin page.
        this is for displaying the currently selected items (in the
        case of a ManyToMany field)
        """
        return Property.objects.filter(pk__in=ids).order_by('name')
