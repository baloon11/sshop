# coding: utf-8
from django.http import Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _

from .models import Order


@login_required
def order_preview(request, order_id, template_name='admin/order_preview.html'):
    if not request.user.is_staff:
        raise Http404
    try:
        order = Order.objects.get(pk=order_id)
    except Order.DoesNotExist:
        raise Http404

    return render_to_response(
        template_name,
        RequestContext(request, {
            'order': order,
            'title': _(u'Preview order %s') % order.number,
        }))
