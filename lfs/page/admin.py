# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from suit.admin import SortableModelAdmin
from .models import Page
from .forms import PageForm


class PageAdmin(SortableModelAdmin, admin.ModelAdmin):
    """
    """
    sortable = 'position'

    list_display = ('title', 'slug', 'active')
    search_fields = ['title', 'slug']
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('active',)

    add_form = PageForm
    edit_form = PageForm

    add_suit_form_tabs = (
        ('general', _(u'General')),
        ('seo', _(u'SEO')),
        ('products', _(u'Products')),
    )
    edit_suit_form_tabs = (
        ('general', _(u'General')),
        ('seo', _(u'SEO')),
        ('products', _(u'Products')),
        ('portlets', _(u'Portlets')),
    )

    fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['title', 'slug', 'active', 'exclude_from_navigation']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['short_text', 'body', 'file']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-seo',),
            'fields': [
                'meta_h1',
                'meta_title', 'meta_keywords', 'meta_description',
                'meta_seo_text']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-products',),
            'fields': ['associated_products', 'associated_categories']
        }),
    ]

    add_suit_form_includes = None
    edit_suit_form_includes = (
        ('admin/portlets_includes/portlets.html', 'top', 'portlets'),
    )
    actions = (
        'apply_default_meta_h1',
        'apply_default_meta_title',
        'apply_default_meta_keywords',
        'apply_default_meta_description',
    )

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def get_form(self, request, obj=None, **kwargs):
        defaults = {}
        if obj is None:
            defaults.update({
                'form': self.add_form,
            })
            self.suit_form_tabs = self.add_suit_form_tabs
            self.suit_form_includes = self.add_suit_form_includes
        else:
            defaults.update({
                'form': self.edit_form,
            })
            self.suit_form_tabs = self.edit_suit_form_tabs
            self.suit_form_includes = self.edit_suit_form_includes
        defaults.update(kwargs)
        return super(PageAdmin, self).get_form(request, obj, **defaults)

    def apply_default_meta_h1(self, request, queryset):
        queryset.update(
            meta_h1=getattr(
                settings, 'DEFAULT_PAGE_META_H1_TEMPLATE', ''))
    apply_default_meta_h1.short_description = _(u'Apply default H1 header')

    def apply_default_meta_title(self, request, queryset):
        queryset.update(
            meta_title=getattr(
                settings, 'DEFAULT_PAGE_META_TITLE_TEMPLATE', ''))
    apply_default_meta_title.short_description = _(u'Apply default META title')

    def apply_default_meta_keywords(self, request, queryset):
        queryset.update(
            meta_keywords=getattr(
                settings, 'DEFAULT_PAGE_META_KEYWORDS_TEMPLATE', ''))
    apply_default_meta_keywords.short_description = \
        _(u'Apply default META keywords')

    def apply_default_meta_description(self, request, queryset):
        queryset.update(
            meta_description=getattr(
                settings, 'DEFAULT_PAGE_META_DESCRIPTION_TEMPLATE', ''))
    apply_default_meta_description.short_description = \
        _(u'Apply default META description')

    def apply_default_meta_text(self, request, queryset):
        queryset.update(
            seo=getattr(settings, 'DEFAULT_PAGE_META_TEXT_TEMPLATE', ''))
    apply_default_meta_text.short_description = _(u'Apply default SEO text')

admin.site.register(Page, PageAdmin)
