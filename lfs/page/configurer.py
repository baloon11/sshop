# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from adminconfig.utils import BaseConfig
from codemirror.widgets import CodeMirrorTextarea


class PcartPagesConfigForm(forms.Form):
    page_name = forms.CharField(
        label=_(u'Name of root page')
    )
    meta_title = forms.CharField(
        required=False,
        label=_(u'Meta Title'),
        widget=CodeMirrorTextarea(mode='xml', config={
            'fixedGutter': True,
            'lineWrapping': True,
        }),
    )
    meta_keywords = forms.CharField(
        widget=CodeMirrorTextarea(mode='xml', config={
            'fixedGutter': True,
            'lineWrapping': True,
        }),
        required=False,
        label=_(u'Meta Keywords')
    )
    meta_description = forms.CharField(
        widget=CodeMirrorTextarea(mode='xml', config={
            'fixedGutter': True,
            'lineWrapping': True,
        }),
        required=False,
        label=_(u'Meta Description')
    )
    meta_text = forms.CharField(
        widget=CodeMirrorTextarea(mode='xml', config={
            'fixedGutter': True,
            'lineWrapping': True,
        }),
        required=False,
        label=_(u'Meta Text')
    )


class PcartPagesConfig(BaseConfig):
    form_class = PcartPagesConfigForm
    block_name = 'pcart_page'

    def __init__(self):
        super(PcartPagesConfig, self).__init__()

        self.default_data = {
            'PCART_PAGE_META_TITLE': u'',
            'PCART_PAGE_META_KEYWORDS': u'',
            'PCART_PAGE_META_DESCRIPTION': u'',
            'PCART_PAGE_META_TEXT': u'',
            'PCART_PAGE_NAME_PAGE': u'',
        }

        self.option_translation_table = (

            ('PCART_PAGE_META_TITLE', 'meta_title'),
            ('PCART_PAGE_META_KEYWORDS', 'meta_keywords'),
            ('PCART_PAGE_META_DESCRIPTION', 'meta_description'),
            ('PCART_PAGE_META_TEXT', 'meta_text'),
            ('PCART_PAGE_NAME_PAGE', 'page_name'),
        )
