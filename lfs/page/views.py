# coding: utf-8
from django.http import Http404
from django.shortcuts import render_to_response
from django.template import RequestContext

from ..caching.utils import lfs_get_object_or_404
from ..core.decorators import ajax_required
from .models import Page, SeoPage
# from django.conf import settings


def page_view(request, slug, template_name="lfs/page/page.html"):
    """Displays page with passed slug
    """
    page = lfs_get_object_or_404(Page, slug=slug)
    # if page.id == 1:
    #     raise Http404()

    if request.user.is_superuser or page.active:
        return render_to_response(template_name, RequestContext(request, {
            "page": page,
            "page_type": "page",
        }))

    raise Http404('No Page matches the given query.')


def pages_view(request, template_name="lfs/page/pages.html"):
    """Displays an overview of all pages.
    """
    pages = Page.objects.filter(active=True, exclude_from_navigation=False)

    # root_page = Page.objects.get(pk=1)
    root_page = SeoPage()
    root_page.slug = 'pages/'
    request.path = '//pages/'

    return render_to_response(template_name, RequestContext(request, {
        "pages": pages,
        "page": root_page,
        "page_type": "pages",
    }))


@ajax_required
def popup_view(request, slug, template_name="lfs/page/popup.html"):
    """Displays page with passed slug
    """
    page = lfs_get_object_or_404(Page, slug=slug)

    return render_to_response(template_name, RequestContext(request, {
        "page": page,
    }))
