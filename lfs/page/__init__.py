from django.conf import settings

if not hasattr(settings, 'PCART_PAGE_META_TITLE'):
    settings.PCART_PAGE_META_TITLE = u''
if not hasattr(settings, 'PCART_PAGE_META_KEYWORDS'):
    settings.PCART_PAGE_META_KEYWORDS = u''
if not hasattr(settings, 'PCART_PAGE_META_DESCRIPTION'):
    settings.PCART_PAGE_META_DESCRIPTION = u''
if not hasattr(settings, 'PCART_PAGE_META_TEXT'):
    settings.PCART_PAGE_META_TEXT = u''
if not hasattr(settings, 'PCART_PAGE_NAME_PAGE'):
    settings.PCART_PAGE_NAME_PAGE = u''
