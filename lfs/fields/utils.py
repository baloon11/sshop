# coding: utf-8
from ..customer.utils import get_or_create_customer
from .arrays import FieldsDataManager


def get_customer_variable(request, variable, address=None):
    if variable[0] == '$':
        variable = variable[1:]
    if request is None:
        return None
    if request.user.is_authenticated():
        customer = get_or_create_customer(request)
        return customer.get_variable(variable, address=address)
    else:
        return None


def get_field_object_list(container_object, request=None, address=None):
    """Return the list of fields attached to container_object
    usable for forms.

    container_object
        Object with `extra` and `fields_objects` attributes.
    """
    from ..core.utils import get_default_shop
    shop = get_default_shop(request)
    address_fields = shop.address_form_fields.split(',')

    post = {}
    if request is not None:
        post = dict(request.POST)

    field_objs = container_object.fields_objects.all().order_by('position')
    fields = []

    d = FieldsDataManager(data=container_object.extra)
    not_binded_actions = d.get_not_binded_actions()
    for action in not_binded_actions:
        if action['type'] == 'write_value':
            post[action['target']] = action['value']
        elif action['type'] == 'write_variable':
            if address is None and action['target'] in address_fields:
                continue
            value = get_customer_variable(
                request, action['variable'], address=address)
            if value is None and action['target'] in post:
                continue
            post[action['target']] = value

    for obj in field_objs:
        if obj.field.identificator in post:
            field = obj.field.get_field_object(
                container_object=container_object,
                initial=post[obj.field.identificator],
                post=post)
        else:
            field = obj.field.get_field_object(
                container_object=container_object,
                post=post)
        if field is None:
            continue
        if 'actions' in field:
            for action in field['actions']:
                if action['type'] == 'write_array':
                    array = d.get_array(action['array'])
                    post['%s__choices' % action['target']] = action['array']
                    if action['target'] not in post:
                        post[action['target']] = array[0]['value']
                elif action['type'] == 'write_value':
                    post[action['target']] = action['value']
                elif action['type'] == 'write_variable':
                    post[action['target']] = \
                        get_customer_variable(
                            request, action['variable'], address=address)
                elif action['type'] == 'touch':
                    post[action['target']] = '!TOUCH'
        fields.append(field)
    return fields


def get_field_titles(container_object):
    from collections import OrderedDict
    field_objs = container_object.fields_objects.all().order_by('position')
    fields = OrderedDict()
    for obj in field_objs:
        fields[obj.field.identificator] = obj.field.field_name
    return fields
