# coding: utf-8
from django import template
from django.contrib.contenttypes.models import ContentType


register = template.Library()


@register.inclusion_tag('admin/fields_block.html', takes_context=True)
def admin_field_info_block(context):
    content_object = ContentType.objects.get(id=context['content_type_id'])
    object_id = context['object_id']
    object = content_object.get_object_for_this_type(id=object_id)
    fields_models = ContentType.objects.filter(app_label='fields')\
        .exclude(app_label='fields', model='fieldsobjects')
    return {
        'content_type': content_object,
        'object': object,
        'fields_models': fields_models,
    }
