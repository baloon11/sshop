# coding: utf-8
import re
import smtplib
import urllib
import logging
import os

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from django.template.base import TemplateDoesNotExist
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.template import Context, Template
# from django.core.cache import cache


from .models import MailTemplate
logger = logging.getLogger('sshop')


def send_email(subject, text, to, html="", bcc=False, job_meta={},
               reply_email=''):
    from ..core.utils import get_default_shop
    shop = get_default_shop()

    email_type = getattr(settings, 'EMAIL_DEFAULT_FORMAT', 'text')
    # email_type = shop.send_email_as

    from_email = shop.from_email
    if bcc:
        bcc = shop.get_notification_emails()
        mail = EmailMultiAlternatives(
            subject=subject, body=text, from_email=from_email, to=to, bcc=bcc)
    else:
        mail = EmailMultiAlternatives(
            subject=subject, body=text, from_email=from_email, to=to)

    if email_type == 'html':
        mail.attach_alternative(html, "text/html")
    elif email_type == 'html+images':
        msgRoot = MIMEMultipart('related')
        msgRoot['Subject'] = subject
        msgRoot['From'] = from_email
        msgRoot['To'] = ', '.join(to)
        if settings.REPLY_TO_CUSTOMER and len(reply_email) > 0:
            msgRoot['Reply-To'] = reply_email

        msgAlternative = MIMEMultipart('alternative')
        msgRoot.attach(msgAlternative)

        msgText = MIMEText(text.encode('UTF-8'), 'plain', 'UTF-8')
        msgAlternative.attach(msgText)

        links = re.compile("<img[^>]*\ssrc=\"(.*?)\"").findall(html)
        media_root = settings.MEDIA_ROOT
        static_root = settings.STATIC_ROOT
        for i, link in enumerate(links):
            try:
                name = 'image%s' % i
                html = html.replace(link, 'cid:%s' % name)
                link = urllib.unquote(link.encode('ascii')).decode('utf-8')
                if '/media/' in link and not link.startswith('/media/'):
                    link = '/media/' + link.split('/media/')[1]
                elif '/static/' in link and not link.startswith('/static/'):
                    link = '/static/' + link.split('/static/')[1]
                if '/media/' in link:
                    path = os.path.join(media_root, link.split('/media/')[-1])
                elif '/static/' in link:
                    path = os.path.join(
                        static_root, link.split('/static/')[-1])
                fp = open(path.encode('utf8'), 'rb')
                msgImage = MIMEImage(fp.read(), _subtype=path.split('.')[-1])
                fp.close()

                msgImage.add_header('Content-ID', '<%s>' % name)
                msgRoot.attach(msgImage)
            except Exception, e:
                logger.error(str(e))

        msgText = MIMEText(html.encode('UTF-8'), 'html', 'UTF-8')
        msgAlternative.attach(msgText)

        if to:
            try:
                smtp = smtplib.SMTP()
                smtp.connect(settings.EMAIL_HOST)
                smtp.login(
                    str(settings.EMAIL_HOST_USER),
                    str(settings.EMAIL_HOST_PASSWORD)
                )
                smtp.sendmail(msgRoot['From'], to, msgRoot.as_string())
                smtp.quit()
            except Exception, e:
                logger.error(str(e))
                mail.attach_alternative(html, "text/html")
                mail.send(fail_silently=True)

    if email_type != 'html+images':
        mail.send(fail_silently=True)


def render(path, **params):
    """Looking for template in DB firstly, than looking for the
    template on disk.
    """
    from ..core.utils import get_default_shop  # prevent the ciclic import
    name, file_type = path.split('/')[-1].split('.')
    site = "http://%s" % Site.objects.get(id=settings.SITE_ID)
    shop = get_default_shop()
    data = params
    data.update({
        'shop': shop,
        'site': site,
    })
    try:
        mail_template = MailTemplate.objects.get(name=name)
        if file_type == 'txt':
            template = mail_template.text_template
        else:
            template = mail_template.html_template

        t = Template(template)
        data.update({
            'topic': mail_template.comment,
        })
        c = Context(data)
        return t.render(c)
    except MailTemplate.DoesNotExist:
        return render_to_string(path, data)


def send_validate_email_mail(email_address):
    """Sends an order has been sent mail to the shop customer
    """
    data = {
        'email_address': email_address.email,
        'email_id': email_address.id,
        'email_verification_code': email_address.verification_code,
        'customer': email_address.customer,
    }
    try:
        subject = render('lfs/mail/validate_email_subject.txt', **data)
    except TemplateDoesNotExist:
        subject = _(u"Email validation")

    if email_address:
        to = [email_address.email]
    else:
        to = []
    # text
    text = render("lfs/mail/validate_email.txt", **data)

    # html
    html = render("lfs/mail/validate_email.html", **data)
    send_email(subject, text, to, html, bcc=False)


def send_order_sent_mail(order):
    """Sends an order has been sent mail to the shop customer
    """
    site = "http://%s" % Site.objects.get(id=settings.SITE_ID)
    data = {
        'order': order,
        'site': site,
    }
    try:
        subject = render('lfs/mail/order_sent_subject.txt', **data)
    except TemplateDoesNotExist:
        subject = _(u"Your order has been sent")

    if order.customer_email:
        to = [order.customer_email]
    elif order.user.email:
        to = [order.user.email]  # TODO: check this
    else:
        to = []
    # text
    text = render("lfs/mail/order_sent_mail.txt", **data)

    # html
    html = render("lfs/mail/order_sent_mail.html", **data)
    send_email(subject, text, to, html, bcc=True)


def send_order_paid_mail(order):
    """Sends an order has been paid mail to the shop customer.
    """
    site = "http://%s" % Site.objects.get(id=settings.SITE_ID)
    data = {
        'order': order,
        'site': site,
    }
    try:
        subject = render("lfs/mail/order_paid_subject.txt", **data)
    except TemplateDoesNotExist:
        subject = _(u"Your order has been paid")

    if order.customer_email:
        to = [order.customer_email]
    elif order.user.email:
        to = [order.user.email]  # TODO: check this
    else:
        to = []

    # text
    text = render("lfs/mail/order_paid_mail.txt", **data)

    # html
    html = render("lfs/mail/order_paid_mail.html", **data)
    send_email(subject, text, to, html, bcc=True)


def send_order_received_mail(order):
    """Sends an order received mail to the shop customer.

    Customer information is taken from the provided order.
    """
    site = "http://%s" % Site.objects.get(id=settings.SITE_ID)
    data = {
        'order': order,
        'site': site,
    }
    try:
        subject = render(
            "lfs/mail/order_received_subject.txt", **data)
    except TemplateDoesNotExist:
        subject = _(u"Your order has been received")

    if order.customer_email:
        to = [order.customer_email]
    elif order.user is not None and order.user.email:
        to = [order.user.email]  # TODO: check this
    else:
        to = []

    # text
    text = render("lfs/mail/order_received_mail.txt", **data)

    # html
    html = render("lfs/mail/order_received_mail.html", **data)
    send_email(subject, text, to, html, bcc=False)
    send_order_to_shop_manager(order)


def send_customer_added(user, password=None):
    """Sends a mail to a newly registered user.
    """
    from ..core.utils import get_default_shop
    shop = get_default_shop()
    data = {
        'shop': shop,
        'user': user,
        'password': password,
    }
    if not user.email:
        return
    to = [user.email]

    try:
        subject = render('lfs/mail/new_user_mail_subject.txt', **data)
    except TemplateDoesNotExist:
        subject = _(u"Your order has been sent")

    # text
    text = render("lfs/mail/new_user_mail.txt", **data)

    # html
    html = render("lfs/mail/new_user_mail.html", **data)

    # Add or update job to send only mail with password
    from tasks.models import Job
    from tasks.api import TaskQueueManager
    task_manager = TaskQueueManager()
    pending_jobs = Job.objects.filter(
        name='lfs.mail.jobs.send_email_job',
        status='pending',
    )
    kwargs = {
        'subject': subject,
        'text': text,
        'to': to,
        'html': html,
    }
    find = False
    for job in pending_jobs:
        if job.args == to and job.kwargs['subject'] == subject:
            job.kwargs = kwargs
            job.save()
            find = True
    if not find:
        task_manager.schedule(
            'lfs.mail.jobs.send_email_job',
            args=to,
            kwargs={
                'subject': subject,
                'text': text,
                'to': to,
                'html': html,
            },
        )


def send_customer_change_password(user):
    """Sends a mail to a newly registered user.
    """
    from ..core.utils import get_default_shop
    shop = get_default_shop()

    to = [user.email]

    # text
    text = render("lfs/mail/change_password_mail.txt", **{
        "user": user, "shop": shop})

    # subject
    subject = render("lfs/mail/change_password_mail_subject.txt", **{
        "user": user, "shop": shop})

    # html
    html = render("lfs/mail/change_password_mail.html", **{
        "user": user, "shop": shop,
    })

    send_email(subject, text, to, html)


def send_customer_change_email(user):
    """Sends a mail to a newly registered user.
    """
    from ..core.utils import get_default_shop
    shop = get_default_shop()

    to = [user.email]

    # text
    text = render("lfs/mail/change_email_mail.txt", **{
        "user": user, "shop": shop})

    # subject
    subject = render("lfs/mail/change_email_mail_subject.txt", **{
        "user": user, "shop": shop})

    # html
    html = render("lfs/mail/change_email_mail.html", **{
        "user": user, "shop": shop,
    })

    send_email(subject, text, to, html)


def send_review_added(review):
    """Sends a mail to shop admins that a new review has been added
    """
    from ..core.utils import get_default_shop
    shop = get_default_shop()

    subject = _(u"New review has been added")
    to = shop.get_notification_emails()

    ctype = ContentType.objects.get_for_id(review.content_type_id)
    product = ctype.get_object_for_this_type(pk=review.content_id)

    # text
    text = render("lfs/mail/review_added_mail.txt", **{
        "review": review,
        "product": product,
    })

    # html
    html = render("lfs/mail/review_added_mail.html", **{
        "site": "http://%s" % Site.objects.get(id=settings.SITE_ID),
        "review": review,
        "product": product,
    })

    send_email(subject, text, to, html)


def send_answer(question, answer):
    """Sends a mail to customer who previously asked the question
    about product
    """
    subject = _(u"You asked about ") + question.product.name
    to = [question.email]

    # text
    text = render("lfs/mail/answer_mail.txt", **{
        "answer": answer,
    })

    # html
    html = render("lfs/mail/answer_mail.html", **{
        "answer": answer,
        "question": question,
    })

    send_email(subject, text, to, html, bcc=True)

    return True


def send_order_to_shop_manager(order):
    """
    """
    from django.contrib.sites.models import Site
    from ..core.utils import get_default_shop

    site = "http://%s" % Site.objects.get(id=settings.SITE_ID)
    shop = get_default_shop()

    try:
        subject = render(
            "lfs/mail/order_received_subject.txt", **{"order": order})
    except TemplateDoesNotExist:
        subject = _(u"Your order has been received")

    to = shop.get_notification_emails()

    # text
    text = render("lfs/mail/order_received_mail.txt", **{"order": order})

    # html
    html = render("lfs/mail/order_received_mail.html", **{
        "order": order,
        'site': site,
    })
    reply_to = order.customer_email
    send_email(subject, text, to, html, reply_email=reply_to)
