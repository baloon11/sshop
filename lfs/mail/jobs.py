# coding: utf-8


def send_email_job(*args, **kwargs):
    from .utils import send_email
    send_email(**kwargs)
