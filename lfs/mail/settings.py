# coding: utf-8

DEMO_DATA = {
    'user': {
        'username': u'testuser@mail.com',
    },
    'password': 'sample_password',
    'email_address': 'test@mail.com',
    'email_id': 1,
    'email_verification_code': 'ca9ec16827ee34b30f572f0d5e449b9678103f7a',
}
