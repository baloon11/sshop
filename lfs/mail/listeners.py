# coding: utf-8
from django.conf import settings

from ..core.signals import customer_added
from ..core.signals import customer_change_password
from ..core.signals import customer_change_email
from ..core.signals import order_submitted
from ..core.signals import order_sent
from ..core.signals import order_paid
from ..core.signals import email_added
from .utils import (
    send_order_paid_mail,
    send_order_sent_mail,
    send_order_received_mail,
    send_customer_added,
    send_customer_change_password,
    send_customer_change_email,
    send_review_added,
    send_validate_email_mail,
)

from reviews.signals import review_added


def email_added_listener(sender, **kwargs):
    """Listen to add the email.
    """
    send_validate_email_mail(sender)
email_added.connect(email_added_listener)


def order_paid_listener(sender, **kwargs):
    """Listen to order payed signal.
    """
    order = sender.get("order")
    send_order_paid_mail(order)
order_paid.connect(order_paid_listener)


def order_sent_listener(sender, **kwargs):
    """Listen to order payed signal.
    """
    order = sender.get("order")
    send_order_sent_mail(order)
order_sent.connect(order_sent_listener)


def order_submitted_listener(sender, **kwargs):
    """Listen to order submitted signal.
    """
    order = sender.get("order")
    if getattr(settings, 'LFS_SEND_ORDER_MAIL_ON_CHECKOUT', True):
        send_order_received_mail(order)
order_submitted.connect(order_submitted_listener)


def customer_added_listener(sender, **kwargs):
    """Listens to customer added signal.
    """
    send_customer_added(sender)
customer_added.connect(customer_added_listener)


def customer_change_password_listener(sender, **kwargs):
    """Listens to customer change password signal.
    """
    send_customer_change_password(sender)
customer_change_password.connect(customer_change_password_listener)


def customer_change_email_listener(sender, **kwargs):
    """Listens to customer change password signal.
    """
    send_customer_change_email(sender)
customer_change_email.connect(customer_change_email_listener)


def review_added_listener(sender, **kwargs):
    """Listens to review added signal
    """
    send_review_added(sender)
review_added.connect(review_added_listener)
