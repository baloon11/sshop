# coding: utf-8
from django.http import Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.template import Context, Template
from django.contrib.sites.models import Site
from django.conf import settings

from ..core.utils import get_default_shop
from ..customer.utils import get_customer
from .models import MailTemplate
from .settings import DEMO_DATA


@login_required
def preview_txt(request, template_id, template_name='mail/mail_preview.txt'):
    if not request.user.is_staff:
        raise Http404
    try:
        mt = MailTemplate.objects.get(pk=template_id)
    except MailTemplate.DoesNotExist:
        raise Http404
    template = Template(mt.text_template)
    customer = get_customer(request)
    data = DEMO_DATA
    data.update({
        'shop': get_default_shop(request),
        'topic': mt.comment,
        'customer': customer,
        'site': "http://%s" % Site.objects.get(pk=settings.SITE_ID)
    })
    context = Context(data)
    mail_body = template.render(context)
    return render_to_response(
        template_name,
        RequestContext(request, {'mail_body': mail_body}),
        mimetype='text/plain; charset=utf-8')


@login_required
def preview_html(request, template_id, template_name='mail/mail_preview.html'):
    if not request.user.is_staff:
        raise Http404
    try:
        mt = MailTemplate.objects.get(pk=template_id)
    except MailTemplate.DoesNotExist:
        raise Http404
    template = Template(mt.html_template)
    customer = get_customer(request)
    data = DEMO_DATA
    data.update({
        'shop': get_default_shop(request),
        'topic': mt.comment,
        'customer': customer,
        'site': "http://%s" % Site.objects.get(pk=settings.SITE_ID)
    })
    context = Context(data)
    mail_body = template.render(context)
    return render_to_response(
        template_name,
        RequestContext(request, {'mail_body': mail_body}))
