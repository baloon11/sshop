# coding: utf-8
from .models import Discount


def get_valid_discounts(request, product=None):
    """Returns all valid discounts as a list.
    """
    discounts = []
    for discount in Discount.objects.all():
        if discount.is_valid(request, product):
            discounts.append({
                "id": discount.id,
                "name": discount.name,
                "sku": discount.sku,
                "price": discount.get_price(request, product),
                "is_absolute": discount.is_absolute(request),
                "value": discount.get_value(request),
            })

    return discounts
