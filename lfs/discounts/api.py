# coding: utf-8
from ..criteria.utils import is_valid as _is_valid
from lfs.cart import utils as cart_utils
from .settings import DISCOUNT_TYPE_ABSOLUTE


class BaseDiscountCalculator(object):
    def is_valid(self, request, criterion, product=None):
        raise NotImplemented

    def get_price(self, request, criterion, product=None):
        raise NotImplemented

    def get_value(self, request, criterion):
        return criterion.value

    def is_absolute(self, request, criterion):
        return criterion.type == DISCOUNT_TYPE_ABSOLUTE


class ProductDiscountCalculator(BaseDiscountCalculator):
    """A discount calculator for product

    """

    def is_valid(self, request, criterion, product=None):
        cart = cart_utils.get_cart(request)
        for item in cart.get_items():
            if _is_valid(request, criterion, item.product) is True:
                return True
        return False

    def get_price(self, request, criterion, product=None):
        """Returns the price of the discount.
        """
        discount = 0.0

        cart = cart_utils.get_cart(request)
        for item in cart.get_items():
            if _is_valid(request, criterion, item.product) is True:
                if criterion.type == DISCOUNT_TYPE_ABSOLUTE:
                    discount += criterion.value * item.amount
                else:
                    discount +=\
                        item.product.get_price(request)\
                        * (criterion.value / 100) * item.amount
        return discount


class CategoryDiscountCalculator(BaseDiscountCalculator):
    """A discount calculator for category
    """

    def is_valid(self, request, criterion, product=None):
        cart = cart_utils.get_cart(request)
        categories = {}
        for cart_item in cart.get_items():
            for category in cart_item.product.categories.all():
                if category in categories:
                    categories[category] += cart_item.amount
                else:
                    categories[category] = cart_item.amount

        for category in categories.keys():
            if _is_valid(
                request,
                criterion,
                category=category,
                amount=categories[category]
            ) is True:
                return True
        return False

    def get_price(self, request, criterion, product=None):
        """Returns the price of the discount.
        """
        discount = 0.0

        cart = cart_utils.get_cart(request)
        categories = {}
        for cart_item in cart.get_items():
            for category in cart_item.product.categories.all():
                if category in categories:
                    categories[category] += [cart_item, ]
                else:
                    categories[category] = [cart_item, ]

        for category in categories.keys():
            if _is_valid(
                request,
                criterion,
                category=category,
                amount=sum([ci.amount for ci in categories[category]])
            ) is True:
                if criterion.type == DISCOUNT_TYPE_ABSOLUTE:
                    discount += criterion.value
                else:
                    products_price = 0.0
                    for cart_item in categories[category]:
                        products_price += cart_item.product.get_price(request)\
                            * cart_item.amount
                    discount += products_price * (criterion.value / 100)
        return discount
