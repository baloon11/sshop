# coding: utf-8
from itertools import *
from django.db import connection


def raw_query(query_string, *query_args):
    cursor = connection.cursor()
    cursor.execute(query_string, query_args)
    col_names = [desc[0] for desc in cursor.description]
    ids = []
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        ids.append(dict(izip(col_names, row))['id'])
    return ids
