# coding: utf-8
from django import forms
import autocomplete_light


class CartItemInlineForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }
