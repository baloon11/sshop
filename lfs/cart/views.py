# coding: utf-8
import locale

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt

from ..caching.utils import lfs_get_object_or_404
from ..core.signals import cart_changed
from ..catalog.models import Product
from .models import CartItem
from ..core.utils import LazyEncoder
from ..voucher.models import Voucher
from ..voucher.settings import MESSAGES
from .utils import (
    get_cart,
    get_go_on_shopping_url,
    get_or_create_cart,
)
from ..voucher.utils import (
    set_current_voucher_number,
    get_current_voucher_number,
)
from ..core.utils import (
    get_default_shop,
)
from ..shipping.utils import (
    get_selected_shipping_method,
    get_shipping_costs,
    get_valid_shipping_methods,
    update_to_valid_shipping_method,
    get_default_shipping_method,
)
from ..payment.utils import (
    get_selected_payment_method,
    get_payment_costs,
    get_valid_payment_methods,
    update_to_valid_payment_method,
)
from ..discounts.utils import (
    get_valid_discounts,
)
from ..customer.utils import (
    get_or_create_customer,
)
from ..core.decorators import ajax_required


@ajax_required
def cart_block(request, template_name="lfs/cart/cart_block.html"):
    """
    The cart block.
    """
    cart = get_cart(request)
    if cart is None:
        total_price = None
    else:
        total_price = cart.get_price(request)
        if getattr(settings, 'CART_BLOCK_DISCOUNT', False):
            discounts = get_valid_discounts(request)
            for discount in discounts:
                total_price -= discount["price"]

    return render_to_response(template_name, RequestContext(request, {
        'cart': cart,
        'total_price': total_price,
    }))


def cart(request, template_name="lfs/cart/cart.html"):
    """
    The main view of the cart.
    """
    return render_to_response(template_name, RequestContext(request, {
        "voucher_number": get_current_voucher_number(request),
        "cart_inline": cart_inline(request),
    }))


def cart_inline(request, template_name="lfs/cart/cart_inline.html"):
    """
    The actual content of the cart.

    This is factored out to be reused within 'normal' and ajax requests.
    """
    cart = get_cart(request)
    shopping_url = get_go_on_shopping_url(request)
    if cart is None:
        return render_to_string(template_name, RequestContext(request, {
            "shopping_url": shopping_url,
        }))

    # shop = get_default_shop(request)
    # countries = shop.shipping_countries.all()

    # Get default shipping method, so that we have a one in any case.
    selected_shipping_method = get_selected_shipping_method(request)
    selected_payment_method = get_selected_payment_method(request)

    shipping_costs = get_shipping_costs(request, selected_shipping_method)

    # Payment
    payment_costs = get_payment_costs(request, selected_payment_method)

    # Cart costs
    cart_price = cart.get_price(request) + shipping_costs["price"] + \
        payment_costs["price"] / payment_costs["ratio"]

    # Discounts
    discounts = get_valid_discounts(request)
    for discount in discounts:
        cart_price = cart_price - discount["price"]

    # Voucher
    voucher_number = get_current_voucher_number(request)
    try:
        voucher = Voucher.objects.get(number=voucher_number)
    except Voucher.DoesNotExist:
        display_voucher = False
        voucher_value = 0
        voucher_message = MESSAGES[6]
    else:
        set_current_voucher_number(request, voucher_number)
        is_voucher_effective, voucher_message = \
            voucher.is_effective(request, cart)
        if is_voucher_effective:
            display_voucher = True
            voucher_value = voucher.get_price(request, cart)
            cart_price = cart_price - voucher_value
        else:
            display_voucher = False
            voucher_value = 0

    cart_items = []
    for cart_item in cart.get_items():
        product = cart_item.product
        quantity = product.get_clean_quantity(cart_item.amount)
        cart_items.append({
            "obj": cart_item,
            "quantity": quantity,
            "product": product,
            "product_price": cart_item.get_price(request),
            "product_price_total": cart_item.get_price_total(request),
        })

    return render_to_string(template_name, RequestContext(request, {
        "cart_items": cart_items,
        "cart_price": cart_price,
        "shipping_methods": get_valid_shipping_methods(request),
        "selected_shipping_method": selected_shipping_method,
        "shipping_price": shipping_costs["price"],
        "payment_methods": get_valid_payment_methods(request),
        "selected_payment_method": selected_payment_method,
        "payment_price": payment_costs["price"] / payment_costs["ratio"],
        # "countries": countries,
        "shopping_url": shopping_url,
        "discounts": discounts,
        "display_voucher": display_voucher,
        "voucher_number": voucher_number,
        "voucher_value": voucher_value,
        "voucher_number": get_current_voucher_number(request),
        "voucher_message": voucher_message,
    }))


def added_to_cart(request, template_name="lfs/cart/added_to_cart.html"):
    """
    Displays the product that has been added to the cart along with the
    selected accessories.
    """
    cart_items = request.session.get("cart_items", [])
    try:
        accessories = cart_items[0].product.get_accessories()
    except IndexError:
        accessories = []

    return render_to_response(template_name, RequestContext(request, {
        "plural": len(cart_items) > 1,
        "shopping_url": request.META.get("HTTP_REFERER", "/"),
        "product_accessories": accessories,
        "cart_items": added_to_cart_items(request),
    }))


def added_to_cart_items(
        request, template_name="lfs/cart/added_to_cart_items.html"):
    """
    Displays the added items for the added-to-cart view.
    """
    total = 0
    cart_items = []
    for cart_item in request.session.get("cart_items", []):
        # total += cart_item.get_price_gross(request)
        total += cart_item.get_price_total(request)
        product = cart_item.product
        quantity = product.get_clean_quantity(cart_item.amount)
        cart_items.append({
            "product": product,
            "obj": cart_item,
            "quantity": quantity,
            "product_price": cart_item.get_price(request),
            "product_price_total": cart_item.get_price_total(request),
        })

    return render_to_string(template_name, RequestContext(request, {
        "total": total,
        "cart_items": cart_items,
    }))


# Actions
def add_accessory_to_cart(request, product_id, quantity=1):
    """
    Adds the product with passed product_id as an accessory to the cart and
    updates the added-to-cart view.
    """
    try:
        quantity = float(quantity)
    except TypeError:
        quantity = 1

    product = lfs_get_object_or_404(Product, pk=product_id)

    session_cart_items = request.session.get("cart_items", [])
    cart = get_cart(request)
    cart_item = cart.add(product=product, amount=quantity)

    # Update session
    if cart_item not in session_cart_items:
        session_cart_items.append(cart_item)
    else:
        for session_cart_item in session_cart_items:
            if cart_item.product == session_cart_item.product:
                session_cart_item.amount += quantity

    request.session["cart_items"] = session_cart_items

    cart_changed.send(cart, request=request)
    return HttpResponse(added_to_cart_items(request))


def add_to_cart_item(request, product_id=None, quantity=None):
    """
    Adds the passed product with passed product_id to the cart after
    some validations have been taken place. The amount is taken from the query
    string.
    """
    product = lfs_get_object_or_404(Product, pk=product_id)
    # Only active and deliverable products can be added to the cart.
    if (product.is_active() and product.status.show_buy_button) is False:
        raise Http404()

    try:
        if isinstance(quantity, unicode):
            # atof() on unicode string fails in some environments, like Czech
            quantity = quantity.encode("utf-8")
        quantity = int(abs(locale.atof(quantity)))
    except (TypeError, ValueError):
        quantity = 1

    cart = get_or_create_cart(request)
    cart_item = cart.add(product, quantity)
    cart_items = [cart_item]
    request.session["cart_items"] = cart_items
    cart_changed.send(cart, request=request)
    cart.save()

    value = product.price * quantity
    shop = get_default_shop(request)
    currency = shop.default_currency

    try:
        result = currency.get_formatted_value(value)
    except ValueError:
        result = value

    return HttpResponse(simplejson.dumps({
        "msg": 'Product ' + product.name + ' added to cart',
        'quantity': quantity,
        'total_price': result
    }))


@csrf_exempt  # TODO: need to use ajax setup
def add_to_cart(request, product_id=None, ajax_mode=False):
    """
    Adds the passed product with passed product_id to the cart after
    some validations have been taken place. The amount is taken from the query
    string.
    """
    if product_id is None:
        product_id = request.REQUEST.get("product_id")
    product = lfs_get_object_or_404(Product, pk=product_id)

    # Only active and deliverable products can be added to the cart.
    if (product.is_active() and product.status.show_buy_button) is False:
        raise Http404()

    try:
        value = request.POST.get("quantity", "1")
        if isinstance(value, unicode):
            # atof() on unicode string fails in some environments, like Czech
            value = value.encode("utf-8")
        quantity = abs(locale.atof(value))
    except (TypeError, ValueError):
        quantity = 1.0

    cart = get_or_create_cart(request)

    cart_item = cart.add(product, amount=quantity)
    cart_items = [cart_item]

    # Add selected accessories to cart
    for key, value in request.POST.items():
        if key.startswith("accessory"):
            accessory_id = key.split("-")[1]
            try:
                accessory = Product.objects.get(pk=accessory_id)
            except ObjectDoesNotExist:
                continue

            # Get quantity
            quantity = request.POST.get("quantity-%s" % accessory_id, 0)
            try:
                quantity = float(quantity)
            except TypeError:
                quantity = 1

            cart_item = cart.add(product=accessory, amount=quantity)
            cart_items.append(cart_item)

    # Store cart items for retrieval within added_to_cart.
    request.session["cart_items"] = cart_items
    cart_changed.send(cart, request=request)

    # Update the customer's shipping method (if appropriate)
    customer = get_or_create_customer(request)
    update_to_valid_shipping_method(request, customer, save=True)

    # Update the customer's payment method (if appropriate)
    update_to_valid_payment_method(request, customer, save=True)

    # Save the cart to update modification date
    cart.save()
    if ajax_mode:
        return added_to_cart(
            request, template_name='lfs/cart/added_to_cart_modal.html')

    try:
        url_name = settings.LFS_AFTER_ADD_TO_CART
    except AttributeError:
        url_name = "lfs.cart.views.added_to_cart"

    return HttpResponseRedirect(reverse(url_name))


def delete_cart_item(request, cart_item_id):
    """
    Deletes the cart item with the given id.
    """
    lfs_get_object_or_404(CartItem, pk=cart_item_id).delete()

    cart = get_cart(request)
    cart_changed.send(cart, request=request)

    return HttpResponse(cart_inline(request))


def refresh_cart(request):
    """
    Refreshes the cart after some changes has been taken place, e.g.: the
    amount of a product or shipping/payment method.
    """
    cart = get_cart(request)
    customer = get_or_create_customer(request)

    # Update Amounts
    message = ""
    for item in cart.get_items():
        try:
            value = request.POST.get("amount-cart-item_%s" % item.id, "0.0")
            if isinstance(value, unicode):
                value = value.encode("utf-8")
            amount = locale.atof(value)
        except (TypeError, ValueError):
            amount = 1.0

        item.amount = amount
        if amount == 0:
            item.delete()
        else:
            item.save()

    # IMPORTANT: We have to send the signal already here, because the valid
    # shipping methods might be dependent on the price.
    cart_changed.send(cart, request=request)

    # Update shipping method
    customer.selected_shipping_method_id = request.POST.get("shipping_method")

    valid_shipping_methods = get_valid_shipping_methods(request)
    if customer.selected_shipping_method not in valid_shipping_methods:
        customer.selected_shipping_method = \
            get_default_shipping_method(request)

    # Update payment method
    customer.selected_payment_method_id = request.POST.get("payment_method")

    # Last but not least we save the customer ...
    customer.save()

    result = simplejson.dumps({
        "html": cart_inline(request),
        "message": message,
    }, cls=LazyEncoder)
    return HttpResponse(result)


def check_voucher(request):
    """
    Updates the cart after the voucher number has been changed.
    """
    voucher_number = get_current_voucher_number(request)
    set_current_voucher_number(request, voucher_number)

    result = simplejson.dumps({
        "html": (("#cart-inline", cart_inline(request)),)
    })
    return HttpResponse(result)


def clear_cart(request):
    '''
    AJAX view, that cleaned current cart
    '''
    cart = get_cart(request)

    for item in cart.get_items():
        item.delete()

    cart_changed.send(cart, request=request)
    return cart_block(request)
