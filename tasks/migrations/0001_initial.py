# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Task'
        db.create_table('tasks_task', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('function_name', self.gf('django.db.models.fields.CharField')(default='', max_length=200)),
            ('arguments', self.gf('django.db.models.fields.TextField')(default='{}')),
            ('added_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, auto_now_add=True, blank=True)),
            ('finished_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('error_msg', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
        ))
        db.send_create_signal('tasks', ['Task'])


    def backwards(self, orm):
        # Deleting model 'Task'
        db.delete_table('tasks_task')


    models = {
        'tasks.task': {
            'Meta': {'object_name': 'Task'},
            'added_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'arguments': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'error_msg': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'finished_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'function_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['tasks']