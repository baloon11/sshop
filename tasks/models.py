# coding: utf-8
import datetime
import logging

from django.utils import timezone
from django.db import models
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField
from .utils import non_unicode_kwarg_keys


logger = logging.getLogger('sshop')


#DEPRECATED form 0.3
class Task(models.Model):
    TASK_STATUSES = (
        (0, _(u'Pending')),
        (3, _(u'In processing')),
        (5, _(u'Success')),
        (10, _(u'Failed')),
    )

    function_name = models.CharField(max_length=200, default='')
    arguments = models.TextField(default='{}')
    added_at = models.DateTimeField(
        default=datetime.datetime.now, auto_now_add=True)
    finished_at = models.DateTimeField(default=datetime.datetime.now)
    status = models.IntegerField(default=0, choices=TASK_STATUSES)
    error_msg = models.TextField(default='', blank=True)
    run_after = models.DateTimeField(
        default=datetime.datetime.now, auto_now_add=True)

    def __unicode__(self):
        return self.function_name


class Job(models.Model):
    '''Abstract class for tasks
    '''
    JOB_STATUSES = (
        ('pending', _(u'Pending')),
        ('running', _(u'Running')),
        ('success', _(u'Success')),
        ('failed', _(u'Failed')),
        ('cancelled', _(u'Canceled')),
    )

    JOB_PRIORITIES = (
        (0, _(u'Paused')),
        (1, _(u'Lowest')),
        (2, _(u'Low-')),
        (3, _(u'Low')),
        (4, _(u'Normal-')),
        (5, _(u'Normal')),
        (6, _(u'Normal+')),
        (7, _(u'High')),
        (8, _(u'High+')),
        (9, _(u'Highest')),
    )

    name = models.CharField(_(u'Name'), max_length=256, blank=False)
    sender = models.CharField(
        _(u'Sender'), max_length=256,
        null=True, blank=True)

    args = JSONField(_(u'Anonymous arguments'), default=[], blank=True)
    kwargs = JSONField(_(u'Named arguments'), default={}, blank=True)
    storage = JSONField(_(u'Storage'), null=True, blank=True)
    result = JSONField(_(u'Result'), null=True, blank=True)

    duty = models.PositiveIntegerField(
        _(u'Priority'), default=5, choices=JOB_PRIORITIES,
        help_text=_(
            u'A positive number. Bigger number means higher priority.'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)
    scheduled = models.DateTimeField(
        _(u'Sheduled'), null=True, blank=True,
        help_text=_(u"If not set, will be executed ASAP"))
    started = models.DateTimeField(_(u'Started'), null=True, blank=True)
    executed = models.DateTimeField(_(u'Executed'), null=True, blank=True)
    repeat_timeout = models.PositiveIntegerField(
        _(u'Repeat timeout'), default=0,
        help_text=_(u'Time in minutes for repeatable tasks.'))

    status = models.CharField(
        _(u'Status'),
        max_length=30, default='pending', choices=JOB_STATUSES)

    class Meta:
        verbose_name = _(u'Job')
        verbose_name_plural = _(u'Jobs')

    def __unicode__(self):
        return '%s priority=%s status=%s id=%s' % (
            self.name, self.duty, self.status, self.id)

    def execute(self):
        from lfs.core.utils import import_symbol
        func = import_symbol(self.name)
        _args = self.args or []
        _kwargs = non_unicode_kwarg_keys(self.kwargs or {})
        result = None
        self.status = 'running'
        self.started = timezone.now()
        self.save()
        _kwargs.update({
            'job_meta': {
                'id': self.id,
                'storage': self.storage,
                'status': self.status,
                'sender': self.sender,
                'priority': self.duty,
            }
        })
        try:
            result = transaction.commit_on_success(func)(*_args, **_kwargs)
            self.result = result
            self.status = 'success'
            self.executed = timezone.now()
            if type(result) is dict:
                if 'job_meta' in result:
                    if 'status' in result['job_meta']:
                        self.status = result['job_meta']['status']
                    if 'storage' in result['job_meta']:
                        self.storage = result['job_meta']['storage']
                    if 'priority' in result['job_meta']:
                        self.duty = result['job_meta']['priority']
            self.save()
        except Exception, e:
            error_str = _(
                u'Background job (pk=%(job)s) is failed: %(exception)s') % {
                    'job': self.id,
                    'exception': unicode(e),
                }
            self.result = {'error': error_str}
            self.executed = timezone.now()
            self.status = 'failed'
            self.save()
            logger.error(error_str)
        if self.repeat_timeout > 0:
            new_job = Job(
                name=self.name,
                sender=self.sender,
                args=self.args,
                kwargs=self.kwargs,
                duty=self.duty,
                scheduled=
                timezone.now()+datetime.timedelta(minutes=self.repeat_timeout),
                repeat_timeout=self.repeat_timeout,
            )
            new_job.save()
        return self.id
