# coding: utf-8
from django.conf.urls.defaults import *

urlpatterns = patterns(
    'regions.views',
    url(r'set-region/$', 'set_region_view', name='set_region'),
)
